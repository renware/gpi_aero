<div id="row">
    <div class="col-md-12">
          <div class="block-web">
            <div class="header">
                <h3 class="content-header">Manage Permissions </h3>
            </div>
            <div class="porlets-content">
                <div class="adv-table editable-table ">
                    <div class="clearfix">
                        <div class="btn-group">
				<a class="btn btn-primary" href="<?php echo $base_url;?>auth_admin/insert_privilege">
                                    Add new <i class="fa fa-plus"></i>
                                </a>
                        </div>
                    </div>
                    <?php if (! empty($message)) { ?>
                            <div id="message">
                                    <?php echo $message; ?>
                            </div>
                    <?php } ?>
			
                    <div class="margin-top-10"></div>
                    <?php echo form_open(current_url());	?>  	
                        <table class="table table-striped table-hover table-bordered">
                            <thead>
                                <tr>
                                    <th title="El nombre del privilegio.">
                                            Privilege name
                                    </th>
                                    <th title="Una breve descripción de la finalidad del privilegio.">
                                            Description
                                    </th>
                                    <th title="Si se marca, la fila se borrará de la forma en proceso de actualización.">
                                            Delete
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php if($privileges) foreach ($privileges as $privilege) { ?>
                                <tr>
                                    <td>
                                        <a href="<?php echo $base_url;?>auth_admin/update_privilege/<?php echo $privilege[$this->flexi_auth->db_column('user_privileges', 'id')];?>">
                                                <?php echo $privilege[$this->flexi_auth->db_column('user_privileges', 'name')];?>
                                        </a>
                                    </td>
                                    <td><?php echo $privilege[$this->flexi_auth->db_column('user_privileges', 'description')];?></td>
                                    <td class="align_ctr">
                                    <?php if ($this->flexi_auth->is_privileged('Delete Users')) { ?>
                                            <input type="checkbox" name="delete_privilege[<?php echo $privilege[$this->flexi_auth->db_column('user_privileges', 'id')];?>]" value="1"/>
                                    <?php } else { ?>
                                            <input type="checkbox" disabled="disabled"/>
                                            <small>no privilegiada</small>
                                            <input type="hidden" name="delete_privilege[<?php echo $privilege[$this->flexi_auth->db_column('user_privileges', 'id')];?>]" value="0"/>
                                    <?php } ?>
                                    </td>
                                </tr>
                            <?php } ?>
                            </tbody>
                            <tfoot>
                                    <td colspan="3">
                                            <?php $disable = (! $this->flexi_auth->is_privileged('Update Privileges') && ! $this->flexi_auth->is_privileged('Delete Privileges')) ? 'disabled="disabled"' : NULL;?>
                                        <button type="submit" name="submit"  class="btn btn-danger btn-icon" <?php echo $disable; ?>>Delete selected privileges <i class="fa fa-trash-o"></i>
                                        </button>
                                    </td>
                            </tfoot>
                        </table>

                    <?php echo form_close();?>
                </div>
            </div>
	</div>	
    </div>
</div>
