<div id="row">
    <div class="col-md-12">
          <div class="block-web">
            <div class="header">
                <h3 class="content-header">User accounts</h3>
            </div>
            <div class="porlets-content">
                <?php if (! empty($message)) { ?>
                        <div id="message">
                                <?php echo $message; ?>
                        </div>
                <?php } ?>
				
                          <?php echo form_open(current_url());	?>
                                    <div class="form-horizontal group-border-dashed">
                                    	<div class="form-group">
                                    		<div class="col-sm-6">
                                    			<a class="btn btn-primary" href="register_account">
                                    				Register new Account
                                    				<i class="fa fa-pencil"></i>
                                    				</a>
                                    		</div>
                                    	</div>
                                    </div>
                                    
                                    <div class="form-horizontal group-border-dashed">
                                   
                                    <fieldset>
                                            <legend>Search Filter</legend>
										
											<div class="form-group">
                                            <label class="col-sm-3 control-label" >Search Users:</label>
                                            <div class="col-sm-6">
                                            <input class="form-control parsley-validated" type="text" id="search" name="search_query" value="<?php echo set_value('search_users',$search_query);?>" title="Search by email, name and lastname."/>
                                            </div>
                                            <input type="submit" name="search_users" value="Search" class="btn btn-info btn-icon"/>   
                                            
                                           
                                            
                                            <a href="<?php echo $base_url; ?>auth_admin/manage_user_accounts" class="btn btn-default">Clean</a>
											 </div>
                                    </fieldset>
                                    </div>
                            <?php echo form_close();?>

                            <?php echo form_open(current_url());	?>
                                    <table class="table table-striped table-hover table-bordered">
                                            <thead>
                                                    <tr>
                                                            <th >Email address</th>
                                                            <th>Name</th>
                                                            <th>Lastname</th>
                                                            <th 
                                                                    title="Indicates Usergroup which the user belongs to.">
                                                                    User Group
                                                            </th>
                                                            <th 
                                                                    title="Manage access privileges for the user.">
                                                                    User privileges
                                                            </th>
                                                            <th 
                                                                    title="If checked, the user won't be able to log in using the account.">
                                                                    Suspend account
                                                            </th>
                                                            <th  
                                                                    title="If checked, the account will be deleted in the update process.">
                                                                    Delete
                                                            </th>
                                                    </tr>
                                            </thead>
                                            <?php if (!empty($users)) { ?>
                                            <tbody>
                                                    <?php foreach ($users as $user) { ?>
                                                    <tr>
                                                            <td>
                                                                    <a href="<?php echo $base_url.'auth_admin/update_user_account/'.$user[$this->flexi_auth->db_column('user_acc', 'id')];?>">
                                                                            <?php echo $user[$this->flexi_auth->db_column('user_acc', 'email')];?>
                                                                    </a>
                                                            </td>
                                                            <td>
                                                                    <?php echo $user['upro_first_name'];?>
                                                            </td>
                                                            <td>
                                                                    <?php echo $user['upro_last_name'];?>
                                                            </td>
                                                            <td class="align_ctr">
                                                                    <?php echo $user[$this->flexi_auth->db_column('user_group', 'name')];?>
                                                            </td>
                                                            <td class="align_ctr">
                                                                    <a href="<?php echo $base_url.'auth_admin/update_user_privileges/'.$user[$this->flexi_auth->db_column('user_acc', 'id')];?>">Manage</a>
                                                            </td>
                                                            <td class="align_ctr">
                                                                    <input type="hidden" name="current_status[<?php echo $user[$this->flexi_auth->db_column('user_acc', 'id')];?>]" value="<?php echo $user[$this->flexi_auth->db_column('user_acc', 'suspend')];?>"/>
                                                                    <!-- A hidden 'suspend_status[]' input is included to detect unchecked checkboxes on submit -->
                                                                    <input type="hidden" name="suspend_status[<?php echo $user[$this->flexi_auth->db_column('user_acc', 'id')];?>]" value="0"/>

                                                            <?php if ($this->flexi_auth->is_privileged('Update Users')) { ?>
                                                                    <input type="checkbox" name="suspend_status[<?php echo $user[$this->flexi_auth->db_column('user_acc', 'id')];?>]" value="1" <?php echo ($user[$this->flexi_auth->db_column('user_acc', 'suspend')] == 1) ? 'checked="checked"' : "";?>/>
                                                            <?php } else { ?>
                                                                    <input type="checkbox" disabled="disabled"/>
                                                                    <small>Not Privileged</small>
                                                                    <input type="hidden" name="suspend_status[<?php echo $user[$this->flexi_auth->db_column('user_acc', 'id')];?>]" value="0"/>
                                                            <?php } ?>
                                                            </td>
                                                            <td class="align_ctr">
                                                            <?php if ($this->flexi_auth->is_privileged('Delete Users')) { ?>
                                                                    <input type="checkbox" name="delete_user[<?php echo $user[$this->flexi_auth->db_column('user_acc', 'id')];?>]" value="1"/>
                                                            <?php } else { ?>
                                                                    <input type="checkbox" disabled="disabled"/>
                                                                    <small>Not Privileged</small>
                                                                    <input type="hidden" name="delete_user[<?php echo $user[$this->flexi_auth->db_column('user_acc', 'id')];?>]" value="0"/>
                                                            <?php } ?>
                                                            </td>
                                                    </tr> 
                                            <?php } ?>
                                            </tbody>
                                            <tfoot>
                                                    <tr>
                                                            <td colspan="7">
                                                                    <?php $disable = (! $this->flexi_auth->is_privileged('Update Users') && ! $this->flexi_auth->is_privileged('Delete Users')) ? 'disabled="disabled"' : NULL;?>
                                                                <button value="true" type="submit" name="update_users"  <?php echo $disable; ?> class="btn btn-primary" >
                                                                    Update / Delete selected users <i class="fa fa-pencil"></i>
                                                                </button>
                                                            </td>
                                                    </tr>
                                            </tfoot>
                                    <?php } else { ?>
                                            <tbody>
                                                    <tr>
                                                            <td colspan="7" class="highlight_red">
                                                                    There is no available users.
                                                            </td>
                                                    </tr>
                                            </tbody>
                                    <?php } ?>
                                    </table>

                            <?php if (! empty($pagination['links'])) { ?>
                                    <div id="pagination" class="w100 frame">
                                            <p>paginación: <?php echo $pagination['total_users'];?> Users matching the search</p>
                                            <p>enlaces: <?php echo $pagination['links'];?></p>
                                    </div>
                            <?php } ?>

                <?php echo form_close();?>
            </div>
        </div>
    </div>		
</div>