<div class="row">
<form id="edit_event" class="form-horizontal" method="POST" action ="<?=site_url("admin/edit_event/".$event["id_event"])?>">
<!-- Form Controls -->
<div class="col-lg-12">
    <div class="portlet portlet-default">
        <div class="portlet-heading">
            <div class="portlet-title">
                <h4>Event Details</h4>
            </div>
            <div class="portlet-widgets">
                
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="panel-collapse collapse in" id="formControls">
            <div class="portlet-body">
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="textInput">Event Name</label>
                        <div class="col-sm-10">
                            <input id="event_name" name="event_name" type="text" placeholder="Event Name"  class="form-control" vaue="<?=$event["name_event"]?>">
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="textArea">Event Description</label>
                        <div class="col-sm-10">
                            <textarea name ="event_description" placeholder="Placeholder Text" id="textArea" class="form-control"><?=$event["description"]?></textarea>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Locations participants</label>
                        <div class="col-sm-10">
                            <?php if($locations)
                                    foreach ($locations as $loc)
                                    {?>
                                
                                        <label class="checkbox-inline">
                                                        <input name="locations[]" type="checkbox"
                                     <?php                       
                                        if($eventLocations)
                                        {
                                            foreach($eventLocations as $evL)
                                            {
                                                if($evl==$loc["id_location"])                                       
                                                {?>
                                                   checked 
                                            <?php }
                                            }
                                        }?>
                                        value="<?=$loc["id_location"]?>"><?=$loc["name"]?>
                                                    </label>
                                <?php}
                                   else{?>
                                        <label class="checkbox-inline">
                                            <input type="checkbox" disabled="" value="">No Locations availables
                                        </label>
                                   <?php }?>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-sm-2 control-label">All Day Event</label>
                        <div class="col-sm-10">
                            <label class="radio-inline">
                                <input type="radio"  value="1" name="alldayEvent" <?=($event["all_day"]==1 ? "checked":"")?>>Yes
                            </label>
                            <label class="radio-inline">
                                <input type="radio" value="2" name="alldayEvent" <?=($event["all_day"]==2 ? "checked":"")?>>No
                            </label>
                            
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Event Category</label>
                        <div class="col-sm-10">
                            <select vaue="<?=$event["eventCategory"]?>" class="form-control" name ="eventCategory" <?=($categories? "":"disabled")?>>
                                <?php if($categories)
                                        foreach($categories as $cat)
                                        {
                                            echo '<option value="'.$cat["id_category"].'">'.$cat["name"].'</option>';
                                        } 
                                    else
                                        echo '<option>No categories available</option>';
                                ?>
                            </select>
                        </div>
                    </div>
            </div>
        </div>
    </div>
    <!-- /.portlet -->
</div>
<!-- /.col-lg-12 (nested) -->
<!-- End Form Controls -->

<!-- Input Sizing -->
<div class="col-lg-12">

    <div class="portlet portlet-default">
        <div class="portlet-heading">
            <div class="portlet-title">
                <h4>Event Date/Time</h4>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="portlet-body">

            <div class="row">
                <div class="col-lg-8">
                    <h4>Date Start</h4>
                    <div id="sandbox-container">
                        <input name="date_begin" id="date_begin" type="text" class="form-control" value="<?=$event["date_start"]?>">
                    </div>
                </div>
                <div class="col-lg-4">
                    <h4>Hour Start</h4>
                    <div class="input-append bootstrap-timepicker input-group">
                        <input name="hour_begin" type="text" class="form-control" id="hour_begin" value="<?=$event["date_start"]?>">
                        <span class="input-group-btn">
                            <button type="button" class="btn btn-default add-on"><i class="fa fa-clock-o"></i>
                            </button>
                        </span>
                    </div>
                </div>
                <div class="col-lg-8">
                    <h4>Date End</h4>
                    <div id="sandbox-container">
                        <input name="date_end" id="date_end" type="text" class="form-control" value="<?=$event["date_end"]?>">
                    </div>
                </div>
                <div class="col-lg-4">
                    <h4>Hour End</h4>
                    <div class="input-append bootstrap-timepicker input-group">
                        <input name="hour_end" type="text" class="form-control" id="hour_end" value="<?=$event["date_end"]?>">
                        <span class="input-group-btn">
                            <button type="button" class="btn btn-default add-on"><i class="fa fa-clock-o"></i>
                            </button>
                        </span>
                    </div>
                </div>
                <!-- /.col-lg-4 (portlet body nested) -->
            </div>
            <!-- /.row (portlet body nested) -->

        </div>
        <!-- /.portlet-body -->
    </div>
    <!-- /.portlet -->

</div>
<div class="col-lg-12">

    <div class="portlet portlet-default">
        <div class="portlet-heading">
            <div class="portlet-title">
                <h4>Qr-options</h4>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="portlet-body">
            
            <div class="form-group">
                <button type="radio" class="form-control"  > Create QR codes for locations</button>
            </div>     
            <div class="form-group">
                        <label class="col-sm-4 control-label" for="textInput">Points (without category's points)</label>
                        <div class="col-sm-6">
                            <input id="points" type="text" placeholder="without category's points" class="form-control">
                        </div>
            </div>
        </div>
            <!-- /.row (portlet body nested) -->
    </div>
        <!-- /.portlet-body -->
</div>
<!-- /.col-lg-12 (nested) -->
<!-- End Form Controls -->
</form>
</div>
