<div class="row">

    <div class="col-lg-8">
        <div class="portlet portlet-default">
            <div class="portlet-heading">
                <div class="portlet-title">
                    <h4>Calendar</h4>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="portlet-body">
                <div class="table-responsive">
                    <div id="calendar"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.col-lg-8 -->

    <div class="col-lg-4">
        <div class="portlet portlet-default">
            <div class="portlet-heading">
                <div id="new_event_list" class="portlet-title">
                    <h4>Draggable Events</h4>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="portlet-body">
                <div id='external-events'>
                    <p>
                        <input type='checkbox' id='drop-remove' />
                        <label for='drop-remove'>Remove After Drop</label>
                    </p>
                    <p>
                        <button id="new_event" type ="button" class="btn btn-green btn-lg" data-toggle="modal" data-target="#flexModal">
                         New event
                        </button>
                    </p>
                </div>
            </div>
        </div>
    </div>
    <!-- /.col-lg-4 -->

</div>
<!-- /.row -->

<!-- Advanced Modal Trigger -->
                                
<!-- Flex Modal -->
<div class="modal modal-flex fade" id="flexModal" tabindex="-1" role="dialog" aria-labelledby="flexModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="flexModalLabel">Flex Admin Styled Modal</h4>
            </div>
            <div class="modal-body" id="flexModalContent">
                <div id="insert"></div>
            </div>
            <div class="modal-footer">
                <button id="close_modal" type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button id="save_data" type="button" class="btn btn-green">Save changes</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<script> 
    var date = new Date();
    var d = date.getDate();
    var m = date.getMonth();
    var y = date.getFullYear()
    var events=[<?php $coma =false; if($events)
        foreach($events as $event)
    {
        if($event["date_start"])
        {
            if($coma)
                echo ",";
            echo "
            {
                id: ".$event["id_event"].",
                title: '".$event["name_event"]."',
                start: '".$event["date_start"]."',
                ".($event["all_day"]==2 ? "end: '".$event["date_end"]."',":"")."
                    ".($event["all_day"]==1 ? "allDay: true,":"")."
                className: '".$event["color"]."'
            }";
        }
            $coma=true;
    }?> 
        ];
    var base_url="<?=site_url()?>";</script>