<div id="row">
    <div class="col-md-12">
        <div class="block-web">
            <div class="header">
                <div class="actions"> <a class="minimize" href="#"><i class="fa fa-chevron-down"></i></a> <a class="refresh" href="#"><i class="fa fa-repeat"></i></a> <a class="close-down" href="#"><i class="fa fa-times"></i></a> </div>
                <h3 class="content-header">Update user's privileges for '<?php echo $user['upro_first_name'].' '.$user['upro_last_name']; ?>', Group Member '<?php echo $user['ugrp_name']; ?>'</h3>
            </div>
            <div class="porlets-content">
                <div class="adv-table editable-table ">
                    <div class="clearfix">
                        <div class="btn-group">
				<a class="btn btn-primary" href="<?php echo $base_url;?>auth_admin/manage_user_accounts">
                                   Manage User Accounts <i class="fa fa-pencil"></i>
                                </a> 
				<a class="btn btn-primary" href="<?php echo $base_url;?>auth_admin/update_user_account/<?php echo $user['upro_uacc_fk']; ?>">
                                    Update User Accounts  <i class="fa fa-pencil"></i>
                                </a>
                        </div>
                    </div>
                    <?php if (! empty($message)) { ?>
                            <div id="message">
                                    <?php echo $message; ?>
                            </div>
                    <?php } ?>
		    <div class="margin-top-10"></div>
                        <?php echo form_open(current_url());	?>  	
                        <table class="table table-striped table-hover table-bordered">
                            <thead>
                                <tr>
                                    <th 
                                            title="The name of the privilege."/>
                                            Privilege Name
                                    </th>
                                    <th 
                                            title="A short description of the purpose of the privilege."/>
                                            Description
                                    </th>
                                    <th 
                                            title="If checked, the user will be granted the privilege, regardless of whether their user group has the privilege."/>
                                            The user has individual privilege
                                    </th>
                                    <th 
                                            title="Indicates whether the privilege has been assigned to the user via the privileges defined for their user group."/>
                                            Has privileges from the group
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($privileges as $privilege) { ?>
                                <tr>
                                    <td>
                                        <input type="hidden" name="update[<?php echo $privilege[$this->flexi_auth->db_column('user_privileges', 'id')]; ?>][id]" value="<?php echo $privilege[$this->flexi_auth->db_column('user_privileges', 'id')]; ?>"/>
                                        <?php echo $privilege[$this->flexi_auth->db_column('user_privileges', 'name')];?>
                                    </td>
                                    <td><?php echo $privilege[$this->flexi_auth->db_column('user_privileges', 'description')];?></td>
                                    <td class="align_ctr">
                                            <?php 
                                                    // Define form input values.
                                                    $current_status = (in_array($privilege[$this->flexi_auth->db_column('user_privileges', 'id')], $user_privileges)) ? 1 : 0; 
                                                    $new_status = (in_array($privilege[$this->flexi_auth->db_column('user_privileges', 'id')], $user_privileges)) ? 'checked="checked"' : NULL;
                                            ?>
                                            <input type="hidden" name="update[<?php echo $privilege[$this->flexi_auth->db_column('user_privileges', 'id')];?>][current_status]" value="<?php echo $current_status ?>"/>
                                            <input type="hidden" name="update[<?php echo $privilege[$this->flexi_auth->db_column('user_privileges', 'id')];?>][new_status]" value="0"/>
                                            <input type="checkbox" name="update[<?php echo $privilege[$this->flexi_auth->db_column('user_privileges', 'id')];?>][new_status]" value="1" <?php echo $new_status ?>/>
                                    </td>
                                    <td class="align_ctr">
                                        <?php echo (in_array($privilege[$this->flexi_auth->db_column('user_privileges', 'id')], $group_privileges) ? 'Si' : 'No'); ?>
                                    </td>
                                </tr>
                            <?php } ?>
                            </tbody>
                            <tfoot>
                                    <tr>
                                            <td colspan="4">
                                                    <input type="submit" name="update_user_privilege" value="Actualizar permisos del usuario" class="link_button large"/>
                                            </td>
                                    </tr>
                            </tfoot>
                        </table>					
                    <?php echo form_close();?>
                </div>
            </div>
	</div>		
    </div>
</div>
