<div id="body_wrap">

	<!-- Demo Navigation -->
	<?php $this->load->view('admin/header'); ?> 
	
	<!-- Intro Content 
	<div class="content_wrap intro_bg">
		<div class="content clearfix">
			<div class="col100">
				<h2>Administrador: Ingresar nuevo grupo de usuarios</h2>
				<p>La biblioteca de autenticación flexible permite a los grupos de usuarios personalizados ilimitados para definirse. Cada usuario puede entonces ser asignado a un grupo específico de usuarios.</p>
				<p>Una vez que los grupos de usuarios se han definido, el acceso a páginas específicas e incluso secciones específicas de las páginas se puede controlar mediante la comprobación de si un usuario tiene permiso para acceder a una página solicitada.</p>
				<p>La configuración por defecto de este programa utiliza grupos de usuarios y privilegios para restringir el ejemplo público usuario el acceso al área de administración, y el usuario ejemplo moderador de inserción, actualización y eliminación de datos específicos en el área de administración.</p>
			</div>		
		</div>
	</div>
	
	 Main Content -->
	<div class="content_wrap main_content_bg">
		<div class="content clearfix">
			<div class="col100">
				<h2>Enter a new user group</h2>
				<a href="<?php echo $base_url;?>auth_admin/manage_user_groups"> Manage User Groups</a>

			<?php if (! empty($message)) { ?>
				<div id="message">
					<?php echo $message; ?>
				</div>
			<?php } ?>
				
				<?php echo form_open(current_url());	?>  	
					<fieldset>
						<legend>Detalles del grupo</legend>
						<ul>
							<li class="info_req">
								<label for="group">Group names:</label>
								<input type="text" id="group" name="insert_group_name" value="<?php echo set_value('insert_group_name');?>" class="tooltip_trigger"
									title="Usergroup Name."/>
							</li>
							<li>
								<label for="description">Group description:</label>
								<textarea id="description" name="insert_group_description" class="width_400 tooltip_trigger"
									title="A brief description of the Usergroup goal."><?php echo set_value('insert_group_description');?></textarea>
							</li>
							<li>
								<label for="admin">Is administration group?:</label>
								<input type="checkbox" id="admin" name="insert_group_admin" value="1" <?php echo set_checkbox('insert_group_admin',1);?> class="tooltip_trigger"
									title="If its checked, the Usergroup is established as 'Admin'."/>
							</li>
						</ul>
					</fieldset>

					<fieldset>
						<legend>Enter new Group</legend>
						<ul>
							<li>
								<label for="submit">Enter Group:</label>
								<input type="submit" name="insert_user_group" id="submit" value="Submit" class="link_button large"/>
							</li>
						</ul>
					</fieldset>
				<?php echo form_close();?>
			</div>
		</div>
	</div>	
</div>

<!-- Scripts -->  
<?php $this->load->view('includes/scripts'); ?> 
