
<div id="body_wrap">
    <div class="content_wrap main_content_bg">
            <div class="content clearfix">
                    <div class="col100">
                            <h2>Insert new Privilege</h2>
                            <a href="<?php echo site_url()?>auth_admin/manage_privileges">Manage Privileges</a>

                    <?php if (! empty($message)) { ?>
                            <div id="message">
                                    <?php echo $message; ?>
                            </div>
                    <?php } ?>

                            <?php echo form_open(current_url());	?>  	
                                    <fieldset>
                                            <legend>Privilege details</legend>
                                            <ul>
                                                    <li class="info_req">
                                                            <label for="privilege">Privilege Name:</label>
                                                            <input type="text" id="privilege" name="insert_privilege_name" value="<?php echo set_value('insert_privilege_name');?>" class="tooltip_trigger"
                                                                    title="El nombre del privilegio."/>
                                                    </li>
                                                    <li>
                                                            <label for="description">Privilege description:</label>
                                                            <textarea id="description" name="insert_privilege_description" class="width_400 tooltip_trigger"
                                                                    title="Una breve descripción de la finalidad del privilegio."><?php echo set_value('insert_privilege_description');?></textarea>
                                                    </li>
                                            </ul>
                                    </fieldset>

                                    <fieldset>
                                    <legend>Add new privilege</legend>
                                            <ul>
                                                    <li>
                                                            <label for="submit">Add privilege:</label>
                                                            <input type="submit" name="insert_privilege" id="submit" value="Send" class="link_button large"/>
                                                    </li>
                                            </ul>
                                    </fieldset>
                            <?php echo form_close();?>
                    </div>
            </div>
    </div>	
</div>
