
<div id="row">	
    <?php if (! empty($message)) { ?>
				<div class="alert alert-success" id="message">
					<?php echo $message; ?>
				</div>
			<?php } ?>
    <section class="panel default blue_title h2">
	
                <div class="panel-heading">Admin: Dashboard</div>
	<!--Main Content -->
	<div class="panel-body">
		<div class="list-group">
                    <h3>User Accounts</h3>
                    <p>Manage the account details of all site users.</p>
                    
                        <a href="<?php echo site_url("auth_admin/manage_user_accounts")?>">Manage User Accounts</a>			
                   
                    <hr/>

                    <h3>User Groups</h3>
                    <p>Manage the user groups that users can be assigned to.</p>
                    <p>User groups are intended to be used to categorise the primary access rights of a user, if required, more specific privileges can then be assigned to a user using the 'User Privileges' below. User groups are completely customised.</p>
                   
                        <a href="<?php echo site_url("auth_admin/manage_user_groups")?>">Manage User Groups</a>			
                  
                    <hr/>

                    <h3>User Privileges</h3>
                    <p>Manage the specific user privileges that can be assigned to users.</p>
                    <p>User privileges are intended to verify whether a user has privileges to perfrom specific actions within the site. The specific action of each privilege is completely customised.</p>
                    <a href="<?php echo site_url("auth_admin/manage_privileges")?>">Manage User Privileges</a>			
                 
                    <hr/>

                    <h3>User Activity</h3>
                    <p>View lists of users that are currently active, inactive or who have a high number of failed logins attempts.</p>
                    <p>
                            When a user registers for an account, it is a good practice to have the user confirm their registration via email, as this helps prevent spam accounts being repeatedly setup.<br/>
                            Active (activated) account users can login, inactive accounts cannot. It is also possible in suspend an active account to prevent the user from logging in again.
                    </p>
                    <ul>
						<li>
                    <a href="<?php echo site_url("auth_admin/list_user_status/active")?>">List all active users</a>
                    </li>	
						<li>
                          <a href="<?php echo site_url("auth_admin/list_user_status/inactive")?>">List all inactive users</a>
                          </li>	
						<li>
                           <a href="<?php echo site_url("auth_admin/delete_unactivated_users")?>">List all unactivated (Never been activated) users over 31 days old</a>
                           </li>	
						<li>
                             <a href="<?php echo site_url("auth_admin/failed_login_users")?>">List users with a high number of failed login attempts</a> - Helps identify possible brute force hack attempts.			
                             </li>	
                    </ul>
                            
		</div>
	</div>
    </section>
</div>
<script>
    $("a").hover(function(){$(this).addClass("active");},function(){$(this).removeClass("active");});  
</script>
    
