<div id="row">
    <div class="col-md-12">
          <div class="block-web">
            <div class="header">
                <div class="actions"> <a class="minimize" href="#"><i class="fa fa-chevron-down"></i></a> <a class="refresh" href="#"><i class="fa fa-repeat"></i></a> <a class="close-down" href="#"><i class="fa fa-times"></i></a> </div>
                <h3 class="content-header">Manage Usergroups</h3>
            </div>
            <div class="porlets-content">
                <div class="adv-table editable-table ">
                            <div class="clearfix">
                                <div class="btn-group">
                                    <a href="<?php echo $base_url;?>auth_admin/insert_user_group" id="editable-sample_new" class="btn btn-primary">
                                      Enter new Usergroup <i class="fa fa-plus"></i>
                                    </a>
                                </div>
                            </div>
                  <div class="margin-top-10"></div>                
                      <?php if (! empty($message)) { ?>
                              <div id="message">
                                      <?php echo $message; ?>
                              </div>
                      <?php } ?>

                  <?php echo form_open(current_url());	?>  	
                          <table class="table table-striped table-hover table-bordered">
                                  <thead>
                                          <tr>
                                                  <th 
                                                          title="The user group name.">
                                                         Group Name
                                                  </th>
                                                  <th  
                                                          title="A short description of the purpose of the user group.">
                                                          Description
                                                  </th>
                                                  <th 
                                                          title="Indicates whether the group is considered an 'Admin' group.<br/> Note: Privileges can still be set seperately.">
                                                          Is Administrative group?
                                                  </th>
                                                  <th class="spacer_100 align_ctr tooltip_trigger"
                                                          title="Manage the access privileges of user groups.">
                                                          Group Privileges
                                                  </th>
                                                  <th class="spacer_100 align_ctr tooltip_trigger" 
                                                          title="If checked, the row will be deleted upon the form being updated.">
                                                          Delete
                                                  </th>
                                          </tr>
                                  </thead>
                                  <tbody>
                                  <?php foreach ($user_groups as $group) { ?>
                                          <tr>
                                                  <td>
                                                          <a href="<?php echo $base_url;?>auth_admin/update_user_group/<?php echo $group[$this->flexi_auth->db_column('user_group', 'id')];?>">
                                                                  <?php echo $group[$this->flexi_auth->db_column('user_group', 'name')];?>
                                                          </a>
                                                  </td>
                                                  <td><?php echo $group[$this->flexi_auth->db_column('user_group', 'description')];?></td>
                                                  <td><?php echo ($group[$this->flexi_auth->db_column('user_group', 'admin')] == 1) ? "Si" : "No";?></td>
                                                  <td>
                                                          <a class="btn btn-primary" href="<?php echo $base_url.'auth_admin/update_group_privileges/'.$group[$this->flexi_auth->db_column('user_group', 'id')];?>">Administrar <i class="fa fa-pencil"></i></a>
                                                  </td>
                                                  <td>
                                                  <?php if ($this->flexi_auth->is_privileged('Delete User Groups')) { ?>
                                                          <input type="checkbox" name="delete_group[<?php echo $group[$this->flexi_auth->db_column('user_group', 'id')];?>]" value="1"/>
                                                  <?php } else { ?>
                                                          <input type="checkbox" disabled="disabled"/>
                                                          <small>Not Privileged</small>
                                                          <input type="hidden" name="delete_group[<?php echo $group[$this->flexi_auth->db_column('user_group', 'id')];?>]" value="0"/>
                                                  <?php } ?>
                                                  </td>
                                          </tr>
                                  <?php } ?>
                                  </tbody>
                                  <tfoot>
                                          <td colspan="5">
                                                  <?php $disable = (! $this->flexi_auth->is_privileged('Update User Groups') && ! $this->flexi_auth->is_privileged('Delete User Groups')) ? 'disabled="disabled"' : NULL;?>
                                                  <button type="submit" name="submit" class="btn btn-danger btn-icon" <?php echo $disable; ?>>
                                                      Eliminar Grupos Seleccionados <i class="fa fa-trash-o"></i>
                                                  </button>
                                          </td>
                                  </tfoot>
                          </table>

                      <?php echo form_close();?>
                </div>
            </div>
        </div>	
    </div>
</div>

<!-- Scripts -->  
<?php $this->load->view('includes/scripts'); ?> 
