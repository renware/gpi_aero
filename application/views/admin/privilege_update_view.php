
<div id="body_wrap">

	<div class="content_wrap main_content_bg">
		<div class="content clearfix">
			<div class="col-md-6">
				<h2>Update privilege</h2>
				<a href="<?php echo $base_url;?>auth_admin/manage_privileges">Manage Privileges</a>

			<?php if (! empty($message)) { ?>
				<div id="message">
					<?php echo $message; ?>
				</div>
			<?php } ?>
				
				<?php echo form_open(current_url());	?>  	
					<fieldset>
						<legend>Privilege details</legend>
						<ul>
							<li class="info_req">
								<label for="privilege">Privilege name:</label>
								<input type="text" id="privilege" name="update_privilege_name" 
								value="<?php echo set_value('update_privilege_name', $privilege[$this->flexi_auth->db_column('user_privileges', 'name')]);?>" class="form-control parsley-validated" title="El nombre del privilegio."/>
							</li>
							<li>
								<label for="description">Privilegio description:</label>
								<textarea id="description" name="update_privilege_description" class="form-control parsley-validated" title="Una breve descripción de la finalidad del privilegio."><?php echo set_value('update_privilege_description',$privilege[$this->flexi_auth->db_column('user_privileges', 'description')]);?></textarea>
							</li>
						</ul>
					</fieldset>
									
					<fieldset>
						<legend>Privilege update details</legend>
						<ul>
							<li>
								<label for="submit">Update Privileges:</label>
								<input type="submit" name="update_privilege" id="submit" value="Update privilege" class="btn btn-primary"/>
							</li>
						</ul>
					</fieldset>
				<?php echo form_close();?>
			</div>
		</div>
	</div>	
</div>