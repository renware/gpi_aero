<div id="row">
    <div class="col-md-12">
          <div class="block-web">
            <div class="header">
                <div class="actions"> <a class="minimize" href="#"><i class="fa fa-chevron-down"></i></a> <a class="refresh" href="#"><i class="fa fa-repeat"></i></a> <a class="close-down" href="#"><i class="fa fa-times"></i></a> </div>
                <h3 class="content-header">Update group privileges '<?php echo $group['ugrp_name']; ?>'</h3>
            </div>
            <div class="porlets-content">
                <div class="adv-table editable-table ">
                    <div class="clearfix">
                        <div class="btn-group">
                            <a class="btn btn-primary" href="<?php echo $base_url;?>auth_admin/manage_user_groups">Manage Usergroups</a> 
                            <a class="btn btn-primary" href="<?php echo $base_url;?>auth_admin/update_user_group/<?php echo $group['ugrp_id']; ?>">Usergroup Update</a>
                        </div>
                    </div>
                    <?php if (! empty($message)) { ?>
                            <div id="message">
                                    <?php echo $message; ?>
                            </div>
                    <?php } ?>
                    <div class="margin-top-10"></div>
			
                    <?php echo form_open(current_url());	?>  
                    
                    <table class="table table-striped table-hover table-bordered" >
                        <thead>
                            <tr>
                                <th title="The name of the Privilege."/>
                                        Privilege Name
                                </th>
                                <th  title="A brief description of the privilege's purpose."/>
                                        Description
                                </th>
                                <th title="If checked, the user will have the privilege."/>
                                        User has privileges
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($privileges as $privilege) { ?>
                            <tr>
                                <td>
                                        <input type="hidden" name="update[<?php echo $privilege[$this->flexi_auth->db_column('user_privileges', 'id')]; ?>][id]" value="<?php echo $privilege[$this->flexi_auth->db_column('user_privileges', 'id')]; ?>"/>
                                        <?php echo $privilege[$this->flexi_auth->db_column('user_privileges', 'name')];?>
                                </td>
                                <td><?php echo $privilege[$this->flexi_auth->db_column('user_privileges', 'description')];?></td>
                                <td class="align_ctr">
                                        <?php 
                                                // Define form input values.
                                                $current_status = (in_array($privilege[$this->flexi_auth->db_column('user_privileges', 'id')], $group_privileges)) ? 1 : 0; 
                                                $new_status = (in_array($privilege[$this->flexi_auth->db_column('user_privileges', 'id')], $group_privileges)) ? 'checked="checked"' : NULL;
                                        ?>
                                        <input type="hidden" name="update[<?php echo $privilege[$this->flexi_auth->db_column('user_privileges', 'id')];?>][current_status]" value="<?php echo $current_status ?>"/>
                                        <input type="hidden" name="update[<?php echo $privilege[$this->flexi_auth->db_column('user_privileges', 'id')];?>][new_status]" value="0"/>
                                        <input type="checkbox" name="update[<?php echo $privilege[$this->flexi_auth->db_column('user_privileges', 'id')];?>][new_status]" value="1" <?php echo $new_status ?>/>
                                </td>
                            </tr>
                        <?php } ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="3">
                                    <input type="submit" name="update_group_privilege" value="Update Group Privileges" class="btn btn-success btn-icon"/>
                                </td>
                            </tr>
                        </tfoot>
                    </table>					
                    <?php echo form_close();?>
                </div>
            </div>
	</div>	
    </div>
</div>

<!-- Scripts -->  
<?php $this->load->view('includes/scripts'); ?> 
