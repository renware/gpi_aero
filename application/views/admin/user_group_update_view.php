<div id="body_wrap">

	<!-- Demo Navigation -->
	<?php $this->load->view('admin/header'); ?> 
	
	<!-- Intro Content 
	<div class="content_wrap intro_bg">
		<div class="content clearfix">
			<div class="col100">
				<h2>Administrador: Actualización de Grupo de Usuarios</h2>
				<p>La biblioteca de autenticación flexible permite a los grupos de usuarios personalizados ilimitados para definirse. Cada usuario puede entonces ser asignado a un grupo específico de usuarios.</p>
				<p>Una vez que los grupos de usuarios se han definido, el acceso a páginas específicas e incluso secciones específicas de las páginas se puede controlar mediante la comprobación de si un usuario tiene permiso para acceder a una página solicitada.</p>
				<p>La configuración por defecto de este programa utiliza grupos de usuarios y privilegios para restringir el ejemplo público usuario el acceso al área de administración, y el usuario ejemplo moderador de inserción, actualización y eliminación de datos específicos en el área de administración.</p>
			</div>		
		</div>
	</div>
	
	 Main Content -->
	<div class="content_wrap main_content_bg">
		<div class="content clearfix">
			<div class="col100">
				<h2>Actualización de Grupo de Usuarios</h2>
				<a href="<?php echo $base_url;?>auth_admin/manage_user_groups">Administrar grupos de usuarios</a>

			<?php if (! empty($message)) { ?>
				<div id="message">
					<?php echo $message; ?>
				</div>
			<?php } ?>
				
				<?php echo form_open(current_url());	?>  	
					<fieldset>
						<legend>Detalles del grupo</legend>
						<ul>
							<li class="info_req">
								<label for="group">Nombre del grupo:</label>
								<input type="text" id="group" name="update_group_name" value="<?php echo set_value('update_group_name', $group[$this->flexi_auth->db_column('user_group', 'name')]);?>" class="tooltip_trigger"
									title="El nombre del grupo de usuarios."/>
							</li>
							<li>
								<label for="description">Descripcion del grupo:</label>
								<textarea id="description" name="update_group_description" class="width_400 tooltip_trigger"
									title="Una breve descripción de la finalidad del grupo de usuarios."><?php echo set_value('update_group_description', $group[$this->flexi_auth->db_column('user_group', 'description')]);?></textarea>
							</li>
							<li>
								<?php $ugrp_admin = ($group[$this->flexi_auth->db_column('user_group', 'admin')] == 1) ;?>
								<label for="admin">Es grupo de administración:</label>
								<input type="checkbox" id="admin" name="update_group_admin" value="1" <?php echo set_checkbox('update_group_admin', 1, $ugrp_admin);?> class="tooltip_trigger"
									title="If checked, the user group is set as an 'Admin' group."/>
							</li>
							<li>
								<label for="admin">Los privilegios de grupo de usuarios:</label>
								<a href="<?php echo $base_url;?>auth_admin/update_group_privileges/<?php echo $group['ugrp_id']; ?>">Manage Privileges for this User Group</a>
							</li>
						</ul>
					</fieldset>
									
					<fieldset>
						<legend>Detalles de la actualización de grupo</legend>
						<ul>
							<li>
								<label for="submit">actualización de Grupo:</label>
								<input type="submit" name="update_user_group" id="submit" value="Enviar" class="link_button large"/>
							</li>
						</ul>
					</fieldset>
				<?php echo form_close();?>
			</div>
		</div>
	</div>	
	
	<!-- Footer -->  
	<?php $this->load->view('includes/footer'); ?> 
</div>

<!-- Scripts -->  
<?php $this->load->view('includes/scripts'); ?> 
