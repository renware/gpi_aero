<div id="row">
    <div class="col-md-12">
        <div class="block-web">
            <div class="header">
                <div class="actions"> <a class="minimize" href="#"><i class="fa fa-chevron-down"></i></a> <a class="refresh" href="#"><i class="fa fa-repeat"></i></a> <a class="close-down" href="#"><i class="fa fa-times"></i></a> </div>
                <h3 class="content-header"><?php echo $page_title;?></h3>
            </div>
            <div class="porlets-content">
                <div class="adv-table editable-table ">

                    <?php if (! empty($message)) { ?>
                            <div id="message">
                                    <?php echo $message; ?>
                            </div>
                    <?php } ?>
                    <div class="margin-top-10"></div>
                        <?php echo form_open(current_url()); ?>
                            <table class="table table-striped table-hover table-bordered">
                                <thead>
                                    <tr>
                                        <th >Email</th>
                                        <th>Nombre</th>
                                        <th>Apellido</th>
                                        <th 
                                                title="Indicates the user group the user belongs to.">
                                                Group
                                        </th>
                                <?php if (isset($status) && $status == 'failed_login_users') { ?>
                                        <th 
                                                title="The number of consecutive failed login attempts made since the user last successfully logged in.">
                                                Failed Attempts</th>
                                <?php } ?>
                                        <th 
                                                title="Indicates whether the users account is currently set as 'active'.">
                                                State
                                        </th>
                                    </tr>
                                </thead>
                                    <?php if (! empty($users)) { ?>
                                            <tbody>
                                            <?php foreach ($users as $user) { ?>
                                                    <tr>
                                                            <td>
                                                                    <a href="<?php echo $base_url;?>auth_admin/update_user_account/<?php echo $user[$this->flexi_auth->db_column('user_acc', 'id')];?>">
                                                                            <?php echo $user[$this->flexi_auth->db_column('user_acc', 'email')];?>
                                                                    </a>
                                                            </td>
                                                            <td>
                                                                    <?php echo $user['upro_first_name'];?>
                                                            </td>
                                                            <td>
                                                                    <?php echo $user['upro_last_name'];?>
                                                            </td>
                                                            <td class="align_ctr">
                                                                    <?php echo $user[$this->flexi_auth->db_column('user_group', 'name')];?>
                                                            </td>
                                                    <?php if (isset($status) && $status == 'failed_login_users') { ?>
                                                            <td class="align_ctr">
                                                                    <?php echo $user[$this->flexi_auth->db_column('user_acc', 'failed_logins')];?>
                                                            </td>
                                                    <?php } ?>
                                                            <td class="align_ctr">
                                                                    <?php echo ($user[$this->flexi_auth->db_column('user_acc', 'active')] == 1) ? 'Activo' : 'Inactivo';?>
                                                            </td>
                                                    </tr>
                                            <?php } ?>
                                            </tbody>
                                <?php } else { ?>
                                <tbody>
                                        <tr>
                                                <td colspan="<?php echo (isset($status) && $status == 'failed_login_users') ? '6' : '5'; ?>" class="highlight_red">
                                                        There is no available users.
                                                </td>
                                        </tr>
                                </tbody>
                            <?php } ?>
                        </table>
                    <?php echo form_close(); ?>
                </div>
            </div>
	</div>
    </div>
</div>