<div id="row">
    <div class="col-md-12">
          <div class="block-web">
            <div class="header">
                <div class="actions"> <a class="minimize" href="#"><i class="fa fa-chevron-down"></i></a> <a class="refresh" href="#"><i class="fa fa-repeat"></i></a> <a class="close-down" href="#"><i class="fa fa-times"></i></a> </div>
                <h3 class="content-header">Account Update <?php echo $user['upro_first_name'].' '.$user['upro_last_name']; ?></h3>
            </div>
            <div class="porlets-content">
                <div class="adv-table editable-table ">
                    <div class="clearfix">
                        <div class="btn-group">
				<a class="btn btn-primary" href="<?php echo $base_url;?>auth_admin/manage_user_accounts">
                                    Manage User Accounts <i class="fa fa-pencil"></i>
                                </a>
                        </div>
                    </div>
                   <?php if (! empty($message)) { ?>
                           <div id="message">
                                   <?php echo $message; ?>
                           </div>
                   <?php } ?>
                    <div class="margin-top-10"></div>
				<?php echo form_open(current_url());?>  	
				
				<div class="form-horizontal group-border-dashed">	
					
					<fieldset>
						<legend>Profile Data</legend>
						
						<div class="form-group">
								<label class="col-sm-3 control-label" for="first_name">Name:</label>
								<div class="col-sm-6">
								<input class="form-control parsley-validated" type="text" id="first_name" name="update_first_name" value="<?php echo set_value('update_first_name',$user['upro_first_name']);?>"/>
								</div>
						</div>
						
						<div class="form-group">
								<label class="col-sm-3 control-label" for="last_name">Last Name:</label>
								<div class="col-sm-6">
								<input class="form-control parsley-validated" type="text" id="last_name" name="update_last_name" value="<?php echo set_value('update_last_name',$user['upro_last_name']);?>"/>
								</div>
						</div>
					</fieldset>
				</div>	
				<div class="form-horizontal group-border-dashed">	
					<fieldset>
						<legend>Contact Details</legend>
						
							<div class="form-group">
								<label class="col-sm-3 control-label" for="phone_number">Phone Number:</label>
								<div class="col-sm-6">
								<input class="form-control parsley-validated" type="text" id="phone_number" name="update_phone_number" value="<?php echo set_value('update_phone_number',$user['upro_phone']);?>"/>
								</div>
							</div>
							
								<?php $newsletter = ($user['upro_newsletter'] == 1) ;?>
							<div class="form-group">
								<label class="col-sm-3 control-label" for="newsletter">Subscribe to the Newspaper:</label>
								<div class="col-sm-6">
								<div class="icheckbox_minimal-blue">
									<input class="form-control parsley-validated" type="checkbox" id="newsletter" name="update_newsletter" value="1" <?php echo set_checkbox('update_newsletter','1',$newsletter); ?>/>
								</div>
							</div>
					</fieldset>
				</div>	
				<div class="form-horizontal group-border-dashed">	
					<fieldset>
						<legend>Enter Details</legend>
							
							<div class="form-group">
								<label class="col-sm-3 control-label" for="email_address">E-mail address:</label>
								<div class="col-sm-6">
								<input class="form-control parsley-validated" type="text" id="email_address" name="update_email_address" value="<?php echo set_value('update_email_address',$user[$this->flexi_auth->db_column('user_acc', 'email')]);?>"
									title="Set the e-mail address for users to log in."
								/>
								</div>
							</div>
								
							<div class="form-group">
								<label class="col-sm-3 control-label" for="username">Username:</label>
								<div class="col-sm-6">
								<input class="form-control parsley-validated" type="text" id="username" name="update_username" value="<?php echo set_value('update_username',$user[$this->flexi_auth->db_column('user_acc', 'username')]);?>" class="tooltip_trigger"
									title="Set the username for users to log in."
								/>
								</div>
							</div>
							
							<div class="form-group">
								<label class="col-sm-3 control-label" for="group">Group:</label>
								<div class="col-sm-6">
								<select id="group" name="update_group" class="form-control"
									title="Set the user group, can be defined as administrator, public, etc ."
								>
							
							
							
								<?php foreach($groups as $group) { ?>
									<?php $user_group = ($group[$this->flexi_auth->db_column('user_group', 'id')] == $user[$this->flexi_auth->db_column('user_acc', 'group_id')]) ? TRUE : FALSE;?>
									<option value="<?php echo $group[$this->flexi_auth->db_column('user_group', 'id')];?>" <?php echo set_select('update_group', $group[$this->flexi_auth->db_column('user_group', 'id')], $user_group);?>>
										<?php echo $group[$this->flexi_auth->db_column('user_group', 'name')];?>
									</option>
								<?php } ?>
								</select>
								</div>
							</div>
							
							<div class="form-group">
								<label class="col-sm-3 control-label">Privileges:</label>
								<div class="col-sm-6">
								<a href="<?php echo $base_url.'auth_admin/update_user_privileges/'.$user[$this->flexi_auth->db_column('user_acc', 'id')];?>" class="btn btn-primary"
									title="Manage access privileges to users."> Manage User Privileges</a>
								</div>
							</div>
							
					</fieldset>
				</div>	
					<fieldset>
						<legend>Update Account</legend>
						<div class="form-group">
								<label class="col-sm-3 control-label" for="submit">Update account:</label>
								<div class="col-sm-6">
								
									<div class="form-button-box">
									<button id="form-button-save" class="btn btn-success" type="submit" name="update_users_account" value = "1">
									<i class="fa fa-check">
									Update
									</i>
									</button>
									</div>
								</div>
								
						</div>
					</fieldset>
				<?php echo form_close();?>
			</div>
		</div>
	</div>	
    </div>
</div>
