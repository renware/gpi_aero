 <?php $user= $this->flexi_auth->get_user_by_identity_row_array();?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">
    <title><?=$title?></title>
    <link type="text/css" rel="stylesheet" href="http://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700,300italic,400italic,500italic,700italic">
<link type="text/css" rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800">
    <meta name="robots" content="index, follow"/>
    <meta name="designer" content="aero "/> 
    <meta name="copyright" content="Copyright <?php echo date('Y');?> Lxmove, All rights reserved"/>
    <meta http-equiv="imagetoolbar" content="no"/>
    <meta name="resource-type" content="document" />
    <meta name="googlebot" content="all, index, follow" />
	<?php
	/** -- Copy from here -- */
	if(!empty($meta))
	foreach($meta as $name=>$content){
		echo "\n\t\t";
		?><meta name="<?php echo $name; ?>" content="<?php echo $content; ?>" /><?php
			 }
	echo "\n";

	if(!empty($canonical))
	{
		echo "\n\t\t";
		?><link rel="canonical" href="<?php echo $canonical?>" /><?php

	}
	echo "\n\t";
if(isset($css_files))//grocery_crud
{
	foreach($css_files as $file)
		echo '<link type="text/css" rel="stylesheet" href="'.$file.'" >';
 
}
if(isset($css))//simple template
{
	foreach($css as $file){
	 	echo "\n\t\t";
		?><link rel="stylesheet" href="<?php echo $file; ?>" type="text/css" ><?php
	} echo "\n\t";
}  
if(isset($js_files)) //grocery crud
{
	foreach($js_files as $file)
		echo '<script src="'.$file.'"></script>';
}
if(isset($js)) //simple template
{
	foreach($js as $file){
			echo "\n\t\t";
			?><script src="<?php echo $file; ?>"></script><?php
	} echo "\n\t";
}?>


<!--[if lt IE 9]>
      <script src="<?=site_url("assets/themes/aero/js/html5shiv.js")?>"></script>
      <script src="<?=site_url("assets/themes/aero/js/respond.min.js")?>"></script>
    <![endif]-->

<style>
    @media (max-width:760px)
    {
        .img-responsive{
            margin-left: auto;
            margin-right: auto;
            margin-top: -7%;
            max-width: 22%;
        }
    }
    @media (min-width:761px)
    {
        .img-responsive{
            margin-left: auto;
            margin-right: auto;
            margin-top: -7%;
            max-width: 100%;
        }
    }
    .navbar-brand{
      height: 80px;   
    }
</style>

</head>
<body>
<div id="wrapper">
  <nav class="navbar-top" role="navigation">

            <!-- begin BRAND HEADING -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle pull-right" data-toggle="collapse" data-target=".sidebar-collapse">
                    <i class="fa fa-bars"></i> Menu
                </button>
                <div class="navbar-brand">
                    <a href="<?=site_url()?>">
                        <img width="40%" height="100%" src="<?=site_url("assets/themes/aero/img/Logo.png")?>" data-1x="<?=site_url("assets/themes/aero/img/Logo.png")?>" data-2x="<?=site_url("assets/themes/aero/img/Logo.png")?>" class="hisrc img-responsive" alt="" />
                    </a>
                </div>
            </div>
            <!-- end BRAND HEADING -->

            <div class="nav-top">

                <!-- begin LEFT SIDE WIDGETS -->
                <ul class="nav navbar-left">
                    <li class="tooltip-sidebar-toggle">
                        <a href="#" id="sidebar-toggle" data-toggle="tooltip" data-placement="right" title="Ocultar Menú">
                            <i class="fa fa-bars"></i>
                        </a>
                    </li>
                    <!-- You may add more widgets here using <li> -->
                </ul>
                <!-- end LEFT SIDE WIDGETS -->

                <!-- begin MESSAGES/ALERTS/TASKS/USER ACTIONS DROPDOWNS -->
                <ul class="nav navbar-right">
                    <!-- begin USER ACTIONS DROPDOWN -->
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-user"></i>  <i class="fa fa-caret-down"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li>
                                <a href="profile.html">
                                    <i class="fa fa-user"></i> My Profile
                                </a>
                            </li>
                            <li>
                                <a href="mailbox.html">
                                    <i class="fa fa-envelope"></i> My Messages
                                    <span class="badge green pull-right">4</span>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="fa fa-bell"></i> My Alerts
                                    <span class="badge orange pull-right">9</span>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="fa fa-tasks"></i> My Tasks
                                    <span class="badge blue pull-right">10</span>
                                </a>
                            </li>
                            <!--<li>
                                <a href="calendar.html">
                                    <i class="fa fa-calendar"></i> My Calendar
                                </a>
                            </li>-->
                            <li class="divider"></li>
                            <li>
                                <a href="#">
                                    <i class="fa fa-gear"></i> Settings
                                </a>
                            </li>
                            <li>
                                <a class="logout_open" href="#logout">
                                    <i class="fa fa-sign-out"></i> Logout
                                    <strong><?php echo (isset($user['upro_first_name']) ? $user['upro_first_name'] : "")?> <?php echo (isset($user['upro_last_name']) ? $user['upro_last_name']:"")?></strong>
                                </a>
                            </li>
                        </ul>
                        <!-- /.dropdown-menu -->
                    </li>
                    <!-- /.dropdown -->
                    <!-- end USER ACTIONS DROPDOWN -->

                </ul>
                <!-- /.nav -->
                <!-- end MESSAGES/ALERTS/TASKS/USER ACTIONS DROPDOWNS -->

            </div>
            <!-- /.nav-top -->
        </nav>
        <!-- /.navbar-top -->
        <!-- end TOP NAVIGATION -->

        <!-- begin SIDE NAVIGATION -->
        <nav class="navbar-side" role="navigation">
            <div class="navbar-collapse sidebar-collapse collapse">
                <ul id="side" class="nav navbar-nav side-nav">
                    <!-- begin SIDE NAV USER PANEL -->
                    <li class="side-user hidden-xs">
                        <img class="img-circle" src="<?=site_url("assets/themes/aero/img/profile-pic.jpg")?>" alt="">
                        <p class="welcome">
                            <i class="fa fa-key"></i> Identificado como 
                        </p>
                        <p class="name tooltip-sidebar-logout">
                            <?php echo (isset($user['upro_first_name']) ? $user['upro_first_name'] : "")?> 
                            <span class="last-name"><?php echo (isset($user['upro_last_name']) ? $user['upro_last_name']:"")?></span> <a style="color: inherit" class="logout_open" href="#logout" data-toggle="tooltip" data-placement="top" title="Logout"><i class="fa fa-sign-out"></i></a>
                        </p>
                        <div class="clearfix"></div>
                    </li>
                    <!-- end SIDE NAV USER PANEL -->
                    <!-- begin SIDE NAV SEARCH -->
                    <li class="nav-search">
                        <form role="form">
                            <input type="search" class="form-control" placeholder="Search...">
                            <button class="btn">
                                <i class="fa fa-search"></i>
                            </button>
                        </form>
                    </li>
                    <!-- end SIDE NAV SEARCH -->
                    <!-- begin DASHBOARD LINK -->
                    <li class="panel">
                        <a href="javascript:;" data-parent="#side" data-toggle="collapse" class="accordion-toggle" data-target="#dashboard">
                            <i class="fa fa-dashboard"></i> Panel de Control <i class="fa fa-caret-down"></i>
                        </a>
                        <ul class="collapse nav" id="dashboard">
                            
			<?php if (! $this->flexi_auth->is_logged_in_via_password()) { ?>
				<li>
					<a href="<?=site_url()?>auth"><span>&nbsp;</span> <i class="fa fa-circle"></i> <b> <?php echo ($this->flexi_auth->is_logged_in()) ? 'Login via Password' : 'Login';?></b></a>
				</li>
			<?php } ?>
			<?php if (! $this->flexi_auth->is_logged_in()) { ?>
				<li>
					<a href="<?=site_url()?>auth/register_account"><span>&nbsp;</span> <i class="fa fa-circle"></i> <b> Register account</b></a>
				</li>
			<?php } else { ?>
				<li>
					<a href="<?=site_url()?>auth/logout"> <span>&nbsp;</span> <i class="fa fa-circle"></i> <b> Log out</b></a>
				</li>
			<?php } ?>
			
                                <li>
                                    <a><span>&nbsp;</span><b class="theme_color">Panel de Control Personal</b></a>
                                </li>
                                <li>
                                    <a href="<?=site_url()?>start/"><span>&nbsp;</span> <i class="fa fa-circle"></i> <b>Perfil Público</b></a>
                                </li>
                                <li >
                                    <a><span>&nbsp;</span> <b class="theme_color"> Seleccione característica a gestionar</b></a></li>
                                <li> 
                                    <a href="<?=site_url()?>start/update_account"><span>&nbsp;</span> <i class="fa fa-circle"></i> <b> Actualizar detalles de cuenta</b></a>                                
                                </li>
                                <li>
                                    <a href="<?=site_url()?>start/update_email"><span>&nbsp;</span> <i class="fa fa-circle"></i> <b> Actualizar Email</b></a>
                                </li>
                                <li>
                                    <a href="<?=site_url()?>start/change_password"><span>&nbsp;</span> <i class="fa fa-circle"></i> <b> Cambiar Contraseña</b></a>
                                </li>
                                <li>
                                    <a href="<?=site_url()?>start/manage_address_book"><span>&nbsp;</span> <i class="fa fa-circle"></i> <b> Gestionar Libro de Direcciones</b></a>
                                </li>
                            


                                <li>
                                    <a><span>&nbsp;</span><b class="theme_color"> Panel de Administrador</b></a>
                                </li>

                                <li>
                                        <a href="<?=site_url()?>auth_admin/"><span>&nbsp;</span> <i class="fa fa-circle"></i> <b> Panel de Control de Administrador</b> </a>
                                </li>
                                <li>
                                    <a><span>&nbsp;</span><b class="theme_color">Seleccione característica a gestionar</b></a></li>
                                <li>
                                        <a href="<?=site_url()?>auth_admin/manage_user_accounts"><span>&nbsp;</span> <i class="fa fa-circle"></i> <b> Gestionar Cuentas de Usuarios</b></a>			
                                </li>
                                <li>
                                        <a href="<?=site_url()?>auth_admin/manage_user_groups"><span>&nbsp;</span> <i class="fa fa-circle"></i> <b> Gestionar Grupos</b></a>			
                                </li>
                                <li>
                                        <a href="<?=site_url()?>auth_admin/manage_privileges"><span>&nbsp;</span> <i class="fa fa-circle"></i> <b> Gestionar Privilegios</b></a>			
                                </li>
                                <li>
                                        <a href="<?=site_url()?>auth_admin/list_user_status/active"><span>&nbsp;</span> <i class="fa fa-circle"></i> <b> Lista de Usuarios Activos</b></a>
                                </li>	
                                <li>
                                        <a href="<?=site_url()?>auth_admin/list_user_status/inactive"><span>&nbsp;</span> <i class="fa fa-circle"></i> <b> Lista de Usuarios Inactivos</b></a>
                                </li>	
                                <li>
                                        <a href="<?=site_url()?>auth_admin/delete_unactivated_users"><span>&nbsp;</span> <i class="fa fa-circle"></i> <b> Lista de usuarios desactivados</b></a>
                                </li>	
                                <li>
                                        <a href="<?=site_url()?>auth_admin/failed_login_users"><span>&nbsp;</span> <i class="fa fa-circle"></i> <b> Lista de accesos fallidos</b></a>			
                                </li>
			</ul>
                        
                    </li>
                    <!-- end DASHBOARD LINK -->

                    
                    <!-- begin Client DROPDOWN -->
                    <li class="panel">
                        <a href="javascript:;" data-parent="#side" data-toggle="collapse" class="accordion-toggle" data-target="#clientes">
                            <i class="fa fa-male"></i> Clientes <i class="fa fa-caret-down"></i>
                        </a>
                        <ul class="collapse nav" id="clientes">
                            <li>
                                <a href="<?=site_url("clientes")?>">
                                    <i class="fa fa-angle-double-right"></i> Inicio
                                </a>
                                
                            </li>
                          
                        </ul>
                    </li>
                    
       
                    
                    <!-- begin Embarque DROPDOWN -->
                    <li class="panel">
                        <a href="javascript:;" data-parent="#side" data-toggle="collapse" class="accordion-toggle" data-target="#embarque">
                            <i class="fa fa-anchor"></i> Embarque <i class="fa fa-caret-down"></i>
                        </a>
                        <ul class="collapse nav" id="embarque">
                            <li>
                                <a href="<?=site_url("embarque")?>">
                                    <i class="fa fa-angle-double-right"></i> Inicio
                                </a>
                                
                            </li>
                          
                        </ul>
                    </li>
                    
                    
                     <!-- begin Mostrador DROPDOWN -->
                    <li class="panel">
                        <a href="javascript:;" data-parent="#side" data-toggle="collapse" class="accordion-toggle" data-target="#mostrador">
                            <i class="fa fa-desktop"></i> Mostrador <i class="fa fa-caret-down"></i>
                        </a>
                        <ul class="collapse nav" id="mostrador">
                            <li>
                                <a href="<?=site_url("mostrador")?>">
                                    <i class="fa fa-angle-double-right"></i> Inicio
                                </a>
                                
                            </li>
                          
                        </ul>
                    </li>
                    
                    <!-- begin Reparto DROPDOWN -->
                    <li class="panel">
                        <a href="javascript:;" data-parent="#side" data-toggle="collapse" class="accordion-toggle" data-target="#repartos">
                            <i class="fa fa-share-square-o"></i> Repartos <i class="fa fa-caret-down"></i>
                        </a>
                        <ul class="collapse nav" id="repartos">
                            <li>
                                <a href="<?=site_url("repartos")?>">
                                    <i class="fa fa-angle-double-right"></i> Inicio
                                </a>
                                
                            </li>
                          
                        </ul>
                    </li>
                    
                    <!-- begin Ventas DROPDOWN -->
                    <li class="panel">
                        <a href="javascript:;" data-parent="#side" data-toggle="collapse" class="accordion-toggle" data-target="#ventas">
                            <i class="fa fa-shopping-cart"></i> Ventas <i class="fa fa-caret-down"></i>
                        </a>
                        <ul class="collapse nav" id="ventas">
                            <li>
                                <a href="<?=site_url("ventas/pasajes")?>">
                                    <i class="fa fa-angle-double-right"></i> Venta de Pasajes
                                </a>
                                
                            </li>
                            <li>
                                <a href="<?=site_url("ventas/encomiendas")?>">
                                    <i class="fa fa-angle-double-right"></i> Solicitud de Encomiendas
                                </a>
                                
                            </li>
                        </ul>
                    </li>
                    
                    <!-- begin Vuelos DROPDOWN -->
                    <li class="panel">
                        <a href="javascript:;" data-parent="#side" data-toggle="collapse" class="accordion-toggle" data-target="#vuelos">
                            <i class="fa fa-fighter-jet"></i> Vuelos <i class="fa fa-caret-down"></i>
                        </a>
                        <ul class="collapse nav" id="vuelos">
                            <li>
                                <a href="<?=site_url("vuelos")?>">
                                    <i class="fa fa-angle-double-right"></i> Inicio
                                </a>
                                
                            </li>
                          
                        </ul>
                    </li>
                    
   
                        
                </ul>
                <!-- /.side-nav -->
            </div>
            <!-- /.navbar-collapse -->
        </nav>
        <!-- /.navbar-side -->
        <!-- end SIDE NAVIGATION -->

        <!-- begin MAIN PAGE CONTENT -->
        <div id="page-wrapper">

            <div class="page-content">


                <!-- begin PAGE TITLE AREA -->
                <!-- Use this section for each page's title and breadcrumb layout. In this example a date range picker is included within the breadcrumb. -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="page-title">
                            <h1><?php echo $title?> 
                                <small><?php echo $meta['description']?></small>
                            </h1>
                            <ol class="breadcrumb">
                                <li class="active"><i class="fa fa-dashboard"></i> <?php echo $title?></li>
                                <!--<li class="pull-right">
                                    <div id="reportrange" class="btn btn-green btn-square date-picker">
                                        <i class="fa fa-calendar"></i>
                                        <span class="date-range"></span> <i class="fa fa-caret-down"></i>
                                    </div>
                                </li>-->
                            </ol>
                        </div>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
                <!-- end PAGE TITLE AREA -->

                <!-- begin DASHBOARD CIRCLE TILES -->
                

                <?php echo $output;?>
                    <?php echo $this->load->get_section('sidebar'); ?>
        </div>
    </div>
</div>
    <div id="logout">
        <div class="logout-message">
            <img class="img-circle img-logout" src="<?=site_url("assets/themes/aero/img/profile-pic.jpg")?>" alt="">
            <h3>
                <i class="fa fa-sign-out text-green"></i> Desconectar?
            </h3>
            <p>Selecciona "Salir" si está listo(a)<br> para terminar la sesión.</p>
            <ul class="list-inline">
                <li>
                    <a href="<?=site_url("auth/logout")?>" class="btn btn-green">
                        <strong>Salir</strong>
                    </a>
                </li>
                <li>
                    <button class="logout_close btn btn-green">Cancelar</button>
                </li>
            </ul>
        </div>
    </div>
      

    <script src="<?=site_url("assets/themes/aero/js/plugins/popupoverlay/logout.js")?>"></script>
    <?php
if(isset($js_inside)) //grocery crud
{
	foreach($js_inside as $file)
		echo '<script src="'.$file.'"></script>/n';
}
?>
  
</body>
</html>
