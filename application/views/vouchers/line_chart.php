<div class="row">
    <div class="col-lg-12">
        <div class="portlet portlet-green">
            <div class="portlet-heading">
                <div class="portlet-title">
                    <h4>points vs date per location</h4>
                </div>
                <div class="portlet-widgets">
                    <a href="javascript:;"><i class="fa fa-refresh"></i></a>
                    <span class="divider"></span>
                    <a data-toggle="collapse" data-parent="#accordion" href="#lineChart"><i class="fa fa-chevron-down"></i></a>
                </div>
                <div class="clearfix"></div>
            </div>
            <div id="lineChart" class="panel-collapse collapse in">
                <div class="portlet-body">
                    <div id="morris-chart-line"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    //Morris Line Chart
    var data=[
        <?php $coma=false;$ant=false;
        if($locations)
            foreach($locations as $location)
            {
                if($coma && $ant!=$location["date"])
                        echo ',';
                if($ant!=$location["date"])
                    {
                        echo "{ d: '".$location["date"]."', ".$location["id_location"].": ".$location["points"];
                        $label_locations[$location["id_location"]]=$location["name"];
                    }
                else 
                {
                    ", ".$location["id_location"].": ".$location["points"]; 
                    $label_locations[$location["id_location"]]=$location["name"];
                }
               if($ant!=$location["date"])
                    echo "}";
               $ant=$location["date"];
               $coma=true;
            }
        ?>
  
];
<?php
$loc=false;$ids=false;$coma=false;$colors=false;
foreach($label_locations as $key=>$la)
{
    if($coma)
    {
        $loc.=",";
        $ids.=",";
        $colors.=",";
    }
    $loc.="'".$la."'";
    $ids.="'".$key."'";
    $colors.="'#".dechex(hexdec("f39c12")+hexdec((int)$key+rand(100,10000)))."'";
    $coma=true;
}?>
var locations =[<?=$loc?>];
var id_locations=[<?=$ids?>];
var colors=[<?=$colors?>];

</script>