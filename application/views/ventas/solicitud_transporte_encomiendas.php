<!DOCTYPE html>
<html lang="en">
    <head>
        <title></title>
        <meta charset="utf-8"> 
        <link rel="stylesheet" href="<?= site_url("assets/themes/aero/css/plugins/bootstrap/css/bootstrap.min.css") ?>" type="text/css" media="all">
        <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.5/css/bootstrap-select.min.css" type="text/css"/>



        <!--[if lt IE 9]>
                <script type="text/javascript" src="http://info.template-help.com/files/ie6_warning/ie6_script_other.js"></script>
                <script type="text/javascript" src="<?= site_url("application//views/ventas/js/html5.js") ?>"></script>
        <![endif]-->


        <script src="http://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.5/js/bootstrap-select.min.js" type="text/javascript"></script>
        <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.min.css" />
        <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker3.min.css" />

        <script src="http://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.min.js"></script>
        <link rel="stylesheet" href="http://weareoutman.github.io/clockpicker/dist/jquery-clockpicker.min.css"/>
        <script src="http://weareoutman.github.io/clockpicker/dist/jquery-clockpicker.min.js"></script>
    </head>
    <body id="page2">

        <div class="container">

            <div class="page-header">
                <h1>Solicitar Transporte de Encomiendas </small></h1>
            </div>

            <!-- Registration form - START -->
            <div class="container">
                <div class="row">
                    <?php echo form_open('ventas/buscar_vuelos_disponibles'); ?>
                    <div class="col-lg-6">
                        <div class="well well-sm"><strong><span class="glyphicon glyphicon-asterisk"></span>Campos Obligatorios</strong></div>
                        <div class="form-group">
                            <label for="InputName">Origen</label>
                            <div class="input-group">
                                <select class="selectpicker form-control" data-size="5" required name="origen" data-live-search="true" data-style="btn-primary">>
                                    <option value=''>Seleccione una ciudad de origen</option>
                                    <?php
                                    foreach ($output as $fila) {
                                        echo "<option value=" . $fila['idLocalizacion'] . ">" . $fila['ciudad'] . "</option>";
                                    }
                                    ?>
                                </select>
                                <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="InputEmail">Destino</label>
                            <div class="input-group">
                                <select class="selectpicker form-control" data-size="5" required name="destino" data-live-search="true" data-style="btn-success">>
                                    <option value=''>Seleccione una ciudad de destino</option>
                                    <?php
                                    foreach ($output as $fila) {
                                        echo "<option value=" . $fila['idLocalizacion'] . ">" . $fila['ciudad'] . "</option>";
                                    }
                                    ?>
                                </select>
                                <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="InputEmail">Fecha Recepcion</label>
                            <div class="input-group input-append date" id="dateRangePicker">
                                <input type="text" class="form-control" name="fecha" placeholder="Fecha de Recepcion" required/>
                                <span class="input-group-addon add-on"><span class="glyphicon glyphicon-asterisk"></span></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="InputEmail">Hora Recepcion</label>
                            <div class="input-group">
                                <input type="time" value="" class="form-control" id="timePicker" name="hora" data-default="20:48">
                                <span class="input-group-addon add-on"><span class="glyphicon glyphicon-asterisk"></span></span>

                            </div>
                        </div>
                        <div class="form-group" id="oficina">
                            <label for="InputEmail">Volumen Total App</label>
                            <div class="input-group">
                                <input type="number" class="form-control bfh-number"  name="volumen" placeholder="Volumen Aproximado en metros cubicos" required>
                                <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
                            </div>
                        </div>

                        <input type="submit" name="submit" id="submit" value="Buscar Encomienda" class="btn btn-info pull-right">
                    </div>
                    <?php echo form_close(); ?>
                </div>
                <?php
                if (isset($_POST['submit'])) {
                    ?>
                <br/>
                    <div class="col-lg-5 col-md-push-1">
                        <div class="col-md-12">
                            <div class="alert alert-danger">
                                <span class="glyphicon glyphicon-remove"></span><strong><?php echo $error;?></strong>
                            </div>
                        </div>
                    </div>
                    <?php
                }
                ?>

            </div>
            <!-- Registration form - END -->

        </div>
        <br/> <br/> <br/>
    <center>
        <footer>
            <p>&copy; D & E AIRLINES S.A. - Todos los derechos reservados.<br/>

                El Bosque 500 Las Condes - Santiago de Chile

                Tel&eacute;fono: 123 456 7890</p>
        </footer>
    </center>

    <script type="text/javascript">
        $(document).ready(function () {
            $('.selectpicker').selectpicker();

            $('#dateRangePicker')
                    .datepicker({
                        format: 'dd/mm/yyyy',
                        startDate: '01/01/2010',
                        endDate: '12/30/2020'
                    })
                    .on('changeDate', function (e) {
                        // Revalidate the date field
                        $('#dateRangeForm').formValidation('revalidateField', 'date');
                    });

            $('#timePicker').clockpicker({
                autoclose: true
            });
        });


    </script>
</body>
</html>