<!DOCTYPE html>
<html lang="en">
    <head>
        <title></title>
        <meta charset="utf-8"> 
        <link rel="stylesheet" href="<?= site_url("assets/themes/aero/css/plugins/bootstrap/css/bootstrap.min.css") ?>" type="text/css" media="all">



        <!--[if lt IE 9]>
                <script type="text/javascript" src="http://info.template-help.com/files/ie6_warning/ie6_script_other.js"></script>
                <script type="text/javascript" src="<?= site_url("application//views/ventas/js/html5.js") ?>"></script>
        <![endif]-->

    </head>
    <body id="page2">

        <div class="container">

            <div class="page-header">
                <h1>Reserva de Encomienda </small></h1>
            </div>

            <!-- Registration form - START -->
            <div class="container">
                <div class="row">
                    <?php echo form_open('ventas/buscar_vuelos_disponibles'); ?>
                    <div class="col-lg-6">
                        <div class="well well-sm"><strong><span class="glyphicon glyphicon-asterisk"></span>Campos Obligatorios</strong></div>
                        <div class="form-group">
                            <label for="InputName">Dimensiones</label>
                            <div class="input-group">
                                <input type="number" class="form-control bfh-number"  name="largo" placeholder="largo" required>
                                <input type="number" class="form-control bfh-number"  name="ancho" placeholder="ancho" required>
                                <input type="number" class="form-control bfh-number"  name="alto" placeholder="alto" required>
                                <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="InputEmail">Peso</label>
                           <div class="input-group">
                                <input type="number" class="form-control bfh-number"  name="peso" placeholder="peso" required>
                                <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
                            </div>
                        </div>
                        <div class="container">
                            <h3>Remitente de Destino</h3>
                        </div>
                        <div class="form-group">
                            <label for="InputEmail">Nombre Destino</label>
                            <div class="input-group">
                                
                                <input type="text" class="form-control bfh-number"  name="nombreDestino" placeholder="nombre destino" required>
                                <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="InputEmail">Direccion Destino</label>
                            <div class="input-group">
                                
                                <input type="text" class="form-control bfh-number"  name="direccionDestino" placeholder="direccion destino" required>
                                <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
                            </div>
                        </div>
                        <div class="form-group" id="oficina">
                            <label for="InputEmail">Telefono Destino</label>
                            <div class="input-group">
                                
                                <input type="number" class="form-control bfh-number"  name="telefonoDestino" placeholder="telefono destino" required>
                                <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
                            </div>
                        </div>
                        <div class="form-group" id="oficina">
                            <label for="InputEmail">Correo  Destino</label>
                            <div class="input-group">
                                
                                <input type="email" class="form-control bfh-number"  name="correoDestino" placeholder="correo destino" required>
                                <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
                            </div>
                        </div>
                        
                        <div class="container">
                            <h3>Remitente de Emisor</h3>
                        </div>
                        <div class="form-group">
                            <label for="InputEmail">Nombre Emisor</label>
                            <div class="input-group">
                                
                                <input type="text" class="form-control bfh-number"  name="nombreEmisor" placeholder="nombre Emisor" required>
                                <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="InputEmail">Direccion Emisor</label>
                            <div class="input-group">
                                
                                <input type="text" class="form-control bfh-number"  name="direccionEmisor" placeholder="direccion Emisor" required>
                                <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
                            </div>
                        </div>
                        <div class="form-group" id="oficina">
                            <label for="InputEmail">Telefono Emisor</label>
                            <div class="input-group">
                                
                                <input type="number" class="form-control bfh-number"  name="telefonoEmisor" placeholder="telefono Emisor" required>
                                <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
                            </div>
                        </div>
                        <div class="form-group" id="oficina">
                            <label for="InputEmail">Correo  Emisor</label>
                            <div class="input-group">
                                
                                <input type="email" class="form-control bfh-number"  name="correoEmisor" placeholder="correo Emisor" required>
                                <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
                            </div>
                        </div>
                        
                        <div class="form-group" id="oficina">
                            <label for="InputEmail">Id Cliente</label>
                            <div class="input-group">
                                
                                <input type="text" class="form-control bfh-number"  name="idCliente" placeholder="id cliente" required>
                                <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
                            </div>
                        </div>
                        
                        <div class="form-group" id="oficina">
                            <label for="InputEmail">Correo Cliente</label>
                            <div class="input-group">
                                
                                <input type="email" class="form-control bfh-number"  name="correoCliente" placeholder="correo cliente" required>
                                <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
                            </div>
                        </div>
                        <input type="submit" name="submit" id="submit" value="Buscar Encomienda" class="btn btn-info pull-right">
                    </div>
                    <?php echo form_close(); ?>
                </div>
                <?php
                if (isset($_POST['submit'])) {
                    ?>
                <br/>
                    <div class="col-lg-5 col-md-push-1">
                        <div class="col-md-12">
                            <div class="alert alert-danger">
                                <span class="glyphicon glyphicon-remove"></span><strong><?php echo $error;?></strong>
                            </div>
                        </div>
                    </div>
                    <?php
                }
                ?>

            </div>
            <!-- Registration form - END -->

        </div>
        <br/> <br/> <br/>
    <center>
        <footer>
            <p>&copy; D & E AIRLINES S.A. - Todos los derechos reservados.<br/>

                El Bosque 500 Las Condes - Santiago de Chile

                Tel&eacute;fono: 123 456 7890</p>
        </footer>
    </center>

   
</body>
</html>
