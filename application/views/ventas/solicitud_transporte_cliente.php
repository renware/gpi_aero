<!DOCTYPE html>
<html lang="en">
    <head>
        <title></title>
        <meta charset="utf-8"> 
        <link rel="stylesheet" href="<?= site_url("assets/themes/aero/css/plugins/bootstrap/css/bootstrap.min.css") ?>" type="text/css" media="all">

        <!--[if lt IE 9]>
                <script type="text/javascript" src="http://info.template-help.com/files/ie6_warning/ie6_script_other.js"></script>
                <script type="text/javascript" src="<?= site_url("application//views/ventas/js/html5.js") ?>"></script>
        <![endif]-->
    </head>
    <body id="page2">

        <div class="container">

            <div class="page-header">
                <h1>Solicitar Transporte de Pasajeros </small></h1>
            </div>

            <!-- Registration form - START -->
            <div class="container">
                <div class="row">
                    <?php echo form_open('ventas/buscar_vuelos_disponibles'); ?>
                        <div class="col-lg-6">
                            <div class="well well-sm"><strong><span class="glyphicon glyphicon-asterisk"></span>Campos Obligatorios</strong></div>
                            <div class="form-group">
                                <label for="InputName">Seleccione una opción</label>
                                <div class="input-group">
                                    <input type="radio" value="1" id="Vuelta" name="opcion" checked="true">Ida y vuelta
                                    <input type="radio" value="2" id="Ida" name="opcion">Ida
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="InputName">Origen</label>
                                <div class="input-group">
                                    <input type="text" class="form-control"  name="CiudadOrigen" id="InputName" placeholder="Ingrese Ciudad de Origen" required>
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="InputEmail">Destino</label>
                                <div class="input-group">
                                    <input type="text" class="form-control" name="CiudadDestino" id="InputEmailFirst"  placeholder="Ingrese Ciudad Destino" required>
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="InputEmail">Fecha Ida</label>
                                <div class="input-group">
                                    <input type="date" class="form-control" id="InputEmailSecond" name="FechaIda" placeholder="Fecha de Ida" required>
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="InputEmail">Hora Ida</label>
                                <div class="input-group">
                                    <input type="time" class="form-control" id="InputEmailSecond" name="HoraIda" placeholder="Hora de Ida" required>
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
                                </div>
                            </div>
                            <div class="form-group" id="fecha-vuelta" >
                                <label for="InputEmail">Fecha Vuelta</label>
                                <div class="input-group">
                                    <input type="date" class="form-control" name="FechaVuelta" placeholder="Fecha de Vuelta" required>
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
                                </div>
                            </div>
                            <div class="form-group" id="oficina">
                                <label for="InputEmail">Oficina</label>
                                <div class="input-group">
                                    <input type="text" class="form-control"  name="Oficina" placeholder="Ingrese  Oficina" required>
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="InputEmail">Numero de Pasajeros</label>
                                <div class="input-group">
                                    <input type="number" class="form-control" id="InputEmailSecond" name="NumeroPasajeros" placeholder="Ingrese  Oficina" required>
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
                                </div>
                            </div>

                            <input type="submit" name="submit" id="submit" value="Buscar Vuelo" class="btn btn-info pull-right">
                        </div>
                    <?php echo form_close(); ?>
                    
                    <?php if(isset($_POST['submit'])){
                        echo "hola";
                    }?>
                    <!--<div class="col-lg-5 col-md-push-1">
                        <div class="col-md-12">
                            <div class="alert alert-success">
                                <strong><span class="glyphicon glyphicon-ok"></span> Success! Message sent.</strong>
                            </div>
                            <div class="alert alert-danger">
                                <span class="glyphicon glyphicon-remove"></span><strong> Error! Please check all page inputs.</strong>
                            </div>
                        </div>
                    </div>-->
                </div>
            </div>
            <!-- Registration form - END -->

        </div>
        <br/> <br/> <br/>
    <center>
        <footer>
            <p>&copy; D & E AIRLINES S.A. - Todos los derechos reservados.<br/>

                El Bosque 500 Las Condes - Santiago de Chile

                Tel&eacute;fono: 123 456 7890</p>
        </footer>
    </center>
    <script type="text/javascript">
        $(document).ready(function () {
             $('input:radio[name="opcion"]').change(
                function () {
                    if (this.checked && this.value == '2') {
                        $('#fecha-vuelta').hide();
                        $('#oficina').hide();
                    } else {
                        $('#fecha-vuelta').show();
                        $('#oficina').show()
                    }
                });
        });   


    </script>
</body>
</html>