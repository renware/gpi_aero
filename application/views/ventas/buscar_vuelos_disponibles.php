<div id="row">
    <div class="col-md-12">
        <div class="block-web">
            <div class="header">
                <h3 class="content-header">Seleccionar un vuelo disponible </h3>
            </div>
            <div class="porlets-content">
                <div class="adv-table editable-table ">



                    <div class="margin-top-10"></div>
                    <?php echo form_open(current_url()); ?>  	
                    <table class="table table-striped table-hover table-bordered">
                        <thead>
                            <tr>
                                <th title="">
                                    Dia
                                </th>
                                <th title="">
                                    Hora salida
                                </th>
                                <th>
                                    Hora Llegada
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach ($output as $fila) {

                                if ($post['hora'] < $fila['hora_llegada']) {
                                    ?>
                                    <tr class="datos danger">
                                    <?php } else { ?>
                                    <tr class="datos success">
                                    <?php }
                                    ?>

                                    <td><?php echo $fila['fecha']; ?></td>

                                    <td><?php echo $fila['hora_salida']; ?></td>

                                    <td><?php echo $fila['hora_llegada']; ?></td>
                                    <td style="display:none;"><?php echo $fila['idVuelo']; ?></td>
                                </tr>
                            <?php } ?>
                        </tbody>
                        <tfoot>

                        </tfoot>
                    </table>

                    <?php echo form_close(); ?>
                </div>
                <div class="col-lg-5 col-md-push-1" id="error" style="visibility:hidden;">
                    <div class="col-md-12">
                        <div class="alert alert-danger">
                            <span class="glyphicon glyphicon-remove"></span><strong><?php echo "No se encuentra volumen disponible en el vuelo seleccionado"; ?></strong>
                        </div>
                    </div>
                </div>
            </div>
        </div>	
    </div>
    <script type="text/javascript">
        $(document).ready(function () {
            $('tr.datos').off('click').on('click', function (event) {
                var vuelo = $(this).find(':nth-child(4)').text();
                $.ajax({
                    url: 'actualizarVuelo',
                    type: 'POST', //the way you want to send data to your URL
                    data: {'vuelo': vuelo, 'volumen': <?php echo $post['volumen']; ?>},
                    success: function (data) { //probably this request will return anything, it'll be put in var "data"
                        var data = $.parseJSON(data);

                        console.log(data);
                        console.log("NOTIFI ES" + data.notify)
                        if (data.notify == "true") {
                            window.location.href=("reservarEncomienda/"+vuelo);
                        } else {
                            $('#error').css("visibility", "visible");
                        }
                    }
                });
            });
        });

    </script>
</div>