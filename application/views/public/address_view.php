<div id="row">
    <div class="col-md-12">
        <div class="block-web">
            <div class="header">
                <div class="actions"> <a class="minimize" href="#"><i class="fa fa-chevron-down"></i></a> <a class="refresh" href="#"><i class="fa fa-repeat"></i></a> <a class="close-down" href="#"><i class="fa fa-times"></i></a> </div>
                <h3 class="content-header">Manage Address Book</h3>
            </div>
            <div class="porlets-content">
                <div class="adv-table editable-table ">
                    <div class="clearfix">
                        <div class="btn-group">
				<a class="btn btn-primary" href="<?php echo $base_url;?>inicio/insert_address">
                                    Enter New Address   <i class="fa fa-plus"></i>
                                </a>
                        </div>
                    </div>
                    <?php if (! empty($message)) { ?>
                            <div id="message">
                                    <?php echo $message; ?>
                            </div>
                    <?php } ?>
		    <div class="margin-top-10"></div>
                    <?php echo form_open(current_url());	?>  	
                            <table>
                                    <thead class="table table-striped table-hover table-bordered">
                                            <tr>
                                                    <th 
                                                            title="An alias to reference the address by.">
                                                            Alias
                                                    </th>
                                                    <th>Recipient</th>
                                                    <th>Company</th>
                                                    <th>Post Code</th>
                                                    <th class="spacer_100 align_ctr tooltip_trigger" 
                                                            title="If checked, the row will be deleted upon the form being updated.">
                                                            Delete
                                                    </th>
                                            </tr>
                                    </thead>
                                    <?php 
                                            if (!empty($addresses)) {
                                                    foreach ($addresses as $address) {
                                    ?>
                                    <tbody>
                                            <tr>
                                                    <td>
                                                            <a href="<?php echo $base_url;?>inicio/update_address/<?php echo $address['uadd_id'];?>/"><?php echo $address['uadd_alias'];?></a>
                                                    </td>
                                                    <td><?php echo $address['uadd_recipient'];?></td>
                                                    <td><?php echo $address['uadd_company'];?></td>
                                                    <td><?php echo $address['uadd_post_code'];?></td>
                                                    <td class="align_ctr">
                                                            <input type="checkbox" name="delete_address[<?php echo $address['uadd_id'];?>]" value="1"/>
                                                    </td>
                                            </tr>
                                    </tbody>
                                    <?php } ?>
                                    <tfoot>
                                            <tr>
                                                    <td colspan="5">
                                                            <input type="submit" name="update_addresses" value="Delete Checked Addresses" class="link_button large"/>
                                                    </td>
                                            </tr>
                                    </tfoot>
                                    <?php } else { ?>
                                    <tbody>
                                            <tr>
                                                    <td colspan="5">
                                                            <p>There are no addresses in your address book</p>
                                                    </td>
                                            </tr>
                                    </tbody>
                                    <?php } ?>
                            </table>
                    <?php echo form_close();?>
                </div>
            </div>
        </div>
    </div>	
</div>
