<div id="row">
    <div class="col-md-12">
          <div class="block-web">
            <div class="header">
                <div class="actions"> <a class="minimize" href="#"><i class="fa fa-chevron-down"></i></a> <a class="refresh" href="#"><i class="fa fa-repeat"></i></a> <a class="close-down" href="#"><i class="fa fa-times"></i></a> </div>
                <h3 class="content-header">Welcome</h3>
            </div>
            <?php if (! empty($message)) { ?>
                    <div id="message">
                            <?php echo $message; ?>
                    </div>
            <?php } ?>
				
            <div class="w100 frame">							
                    <h3>Account Details</h3>
					<p>Update the account details of the currently logged in user.</p>
					<p>This example updates records from the required 'User Accounts' table, and from the custom 'Demo User Profile' table that in this demo is used to store a users name, phone number etc.</p>
							<a class="btn btn-primary" href="<?php echo site_url("start/update_account")?>">Actualizar Detalles de la Cuenta</a>
					
					<hr/>
					
					<h3>Email Address</h3>
					<p>Update the email address of the currently logged in user, via email verification.</p>
					<p>
						Using email verification to update an email address confirms the user has entered the correct new email address.<br/>
						If they were make a typo entering the address, that then was NOT verified via email, they could potentially be prevented from logging back in via their email address as they wouldn't know how they misspelled it. 
					</p>

							<a class="btn btn-primary" href="<?php echo site_url("start/update_email")?>">Update Email Address via Email Verification</a>

					<hr/>
					
					<h3>Password</h3>
					<p>Update the password of the currently logged in user.</p>
					<p>All passwords are securely hashed using the <a href="http://www.openwall.com/phpass/" target="_blank">phpass framework</a>.</p>
					
						
							<a class="btn btn-primary" href="<?php echo site_url("start/change_password")?>">Actualizar Contraseña</a>
					
					<hr/>
					
					<h3>Address Book</h3>
					<p>Manage the custom address details of the currently logged in user.</p>
					<p>This example manages records from the custom 'Demo User Address' table that in this demo is used to store a list of different addresses per user.</p>
					
							<a class="btn btn-primary" href="<?php echo site_url("start/manage_address_book")?>">Manage Address Book</a>
					
            </div>
        </div>
    </div>
</div>	