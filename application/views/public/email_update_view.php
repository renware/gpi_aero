<div id="row">
    <div class="col-md-12">
        <div class="block-web">
            <div class="header">
                <h3 class="content-header">Change email </h3>
            </div>
            <div class="porlets-content">
                <div class="adv-table editable-table ">
                    <div class="clearfix">
                        <div class="btn-group">
				<a class="btn btn-primary" href="<?php echo site_url();?>start/update_account">
                                    Update account info <i class="fa fa-pencil"></i>
                                </a>
                        </div>
                    </div>
                    <?php if (! empty($message)) { ?>
                            <div id="message">
                                    <?php echo $message; ?>
                            </div>
                    <?php } ?>
		    <div class="margin-top-10"></div>
                    <?php echo form_open(current_url());	?>  	
                            <div class="w100 frame">
                                    <div class="form-group">
                                           		 <div class="col-md-4"> 
                                                    <label for="email_address">New email address:</label>
                                                    <input class="form-control" type="text" id="email_address" name="email_address" value="<?php echo set_value('email_address');?>"/>
                                            	
                                            	<hr/>
                                                    <label for="submit">Update Email:</label>
                                                    <input type="submit" name="update_email" id="submit" value="Update" class="btn btn-success btn-icon"/>
                                     			</div>
                                    </div>
                            </div>
                    <?php echo form_close();?>
                </div>
            </div>
	</div>	
    </div>
</div>
