<div id="row">
    <div class="col-md-12">
        <div class="block-web">
            <div class="header">
                <div class="actions"> <a class="minimize" href="#"><i class="fa fa-chevron-down"></i></a> <a class="refresh" href="#"><i class="fa fa-repeat"></i></a> <a class="close-down" href="#"><i class="fa fa-times"></i></a> </div>
                <h3 class="content-header">Actualizar Contraseña</h3>
            </div>
            <div class="porlets-content">
                <div class="adv-table editable-table ">
                    <div class="clearfix">
                        <div class="btn-group">
				<a class="btn btn-primary" href="<?php echo site_url("inicio/update_account");?>">
                                    Actualizar detalles de la cuenta  <i class="fa fa-pencil"></i>
                                </a>
                        </div>
                    </div>
                    <?php if (! empty($message)) { ?>
                            <div id="message">
                                    <?php echo $message; ?>
                            </div>
                    <?php } ?>
		    <div class="margin-top-10"></div>
                    <?php echo form_open(current_url());	?>  	
                        
                            <div class="form-group">
                                <div class="col-md-6">  
                                        <small>
                                                <strong>The following validation settings have been defined:</strong><br/>
                                                Password length must be more than <?php echo $this->flexi_auth->min_password_length(); ?> characters in length.<br/>
                                                Only alpha-numeric, dashes, underscores, periods and comma characters are allowed.
                                        </small>
                               			
                               			<hr/>
                                
                               
                                        <label for="current_password">Contraseña Actual:</label>
                                        <input class="form-control" type="password" id="current_password" name="current_password" value="<?php echo set_value('current_password');?>"/>
                                
                                        <label for="new_password">Nueva Contraseña:</label>
                                        <input class="form-control" type="password" id="new_password" name="new_password" value="<?php echo set_value('new_password');?>"/>
                                
                                        <label for="confirm_new_password">Confirmar Nueva Contraseña:</label>
                                        <input class="form-control" type="password" id="confirm_new_password" name="confirm_new_password" value="<?php echo set_value('confirm_new_password');?>"/>
                                		
                                		<hr/>
                                		
                                        <label for="submit">Actualizar Contraseña:</label>
                                        <input class="btn btn-success btn-icon" type="submit" name="change_password" id="submit" value="Actualizar" class="link_button large"/>
                             
                            </div>
                        </div>
                    <?php echo form_close();?>
                </div>
            </div>
	</div>	
    </div>
</div>
