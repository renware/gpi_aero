<div class="alert alert-danger fade in">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h4>¡Something is wrong!</h4>
    <p><?php if (! empty($message)) { ?>
            <div id="message">
                    <?php echo $message; ?>
            </div>
    <?php } ?></p>
    <p>
      <button type="button" class="btn btn-danger" onclick="window.location='<?=site_url()?>'">To dashboard</button>
      <button type="button" class="btn btn-default" onclick="window.history.back()">Go Back</button>
    </p>
</div>