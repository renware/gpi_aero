<div id="row">
    <div class="col-md-12">
          <div class="block-web">
            <div class="header">
                <h3 class="content-header">Update account details</h3>
            </div>
            <div class="porlets-content">
                <div class="adv-table editable-table ">
                    <div class="clearfix">
                        <div class="btn-group">
				<a class="btn btn-primary" href="<?php echo site_url("start/change_password")?>">
                                    Password change <i class="fa fa-pencil"></i>
                                </a>
                        </div>
                    </div>
                   <?php if (! empty($message)) { ?>
                           <div id="message">
                                   <?php echo $message; ?>
                           </div>
                   <?php } ?>
                    <div class="margin-top-10"></div>

                           <?php echo form_open(current_url());	?>  	
                                   <fieldset>
                                           <legend>Personal details</legend>
                                           <div class="form-group">
                                           		 <div class="col-md-6">
                                                           <label for="first_name">Name:</label>
                                                           <input class="form-control" type="text" id="first_name" name="update_first_name" value="<?php echo set_value('update_first_name',$user['upro_first_name']);?>"/>
                                                
                                                   
                                                   
                                                           <label for="last_name">Last name:</label>
                                                           <input class="form-control" type="text" id="last_name" name="update_last_name" value="<?php echo set_value('update_last_name',$user['upro_last_name']);?>"/>
                                                  </div>
                                           </div>
                                   </fieldset>

                                   <fieldset>
                                           <legend>Contact info</legend>
                                            <div class="form-group">
                                           		 <div class="col-md-6">                                                           <label for="phone_number">Phone number:</label>
                                                           <input class="form-control" type="text" id="phone_number" name="update_phone_number" value="<?php echo set_value('update_phone_number',$user['upro_phone']);?>"/>
                                          
                                                           <?php $newsletter = ($user['upro_newsletter'] == 1) ;?>
                                                           <label for="newsletter">Newsletter suscript:</label>
                                                           <input type="checkbox" id="newsletter" name="update_newsletter" value="1" <?php echo set_checkbox('update_newsletter',1,$newsletter); ?>/>
                                         		</div>
                                         	</div>
                                   </fieldset>

                                   <fieldset>
                                           <legend>Login info</legend>
                                            <div class="form-group">
                                           		 <div class="col-md-6">         
                                                           <label>Email:</label>
                                                           <input class="form-control" type="text" id="email" name="update_email" readonly="" value="<?php echo set_value('update_email',$user[$this->flexi_auth->db_column('user_acc', 'email')]);?>" class="tooltip_trigger"
                                                                   title="Set an email address that can be used to login with."
                                                           />
                                                           <p class="note">
                                                                   Note: This method simply updates the users email address, if you want to verify the user has spelt their new email address correctly, you can send them a verification email to their new email address.<br/> 
                                                                   <a href="<?php echo site_url("start/update_email")?>">Click here to update your user email via email verification</a>.
                                                           </p>
                                                  
                                                    <hr/>
                                                           <label for="username">User name:</label>
                                                           <input class="form-control" type="text" id="username" name="update_username" value="<?php echo set_value('update_username',$user[$this->flexi_auth->db_column('user_acc', 'username')]);?>" class="tooltip_trigger"
                                                                   title="Set a username that can be used to login with."
                                                           />
                                                 	
                                                 	<hr/>
                                                           <label>Password:</label>
                                                           <a class="btn btn-primary" href="<?php echo site_url("start/change_password")?>">Click here to change your password <i class="fa fa-pencil"></i></a>
                                            	</div>
                                         	</div>
                                   </fieldset>
									
									<hr/>
									
                                   <fieldset>
                                           <legend>Update account</legend>
                                          <div class="form-group">
                                           		 <div class="col-md-6"> 
                                           		 	 <hr/>
                                                           <label for="submit">Update account:</label>
                                                           <button type="submit" name="update_account" id="submit" value="Update" class="btn btn-success">Update <i class="fa fa-save"></i></button>
                                                           <button id="salir" onclick="window.location='<?=site_url("start")?>'" class="btn btn-primary" type="button"> Exit <i class="fa fa-arrow-circle-o-right"></i></button>
                                           		</div>
                                         	</div>
                                   </fieldset>
                           <?php echo form_close();?>
                   </div>
            </div>
        </div>	
	
    </div>
</div>
