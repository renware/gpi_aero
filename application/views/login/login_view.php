<div class="container">
  <!--\\\\\\\ wrapper Start \\\\\\-->
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
        <div class="login-banner text-center">
            <h1><img src="<?=site_url("assets/themes/aero/img/Logo.png")?>" width="40%"/></h1>
        </div>
        <div class="login-banner text-center">
            <h2>Código brindado sólo para uso Académico. GPI 2015.</h2>
            <h4>Usuario por defecto: administrador</h4>
            <h4>Password: password123</h4>
        </div>
        <div class="portlet portlet-green">
            <div class="portlet-heading login-heading">
                        <div class="portlet-title">
                            <h4><strong>Login</strong>
                            </h4>
                        </div>
                        <div class="portlet-widgets">
                            <button class="btn btn-white btn-xs" type="button" name="register_accoun" value="1" onclick="window.location='<?=site_url("auth/register_account")?>'"><i class="fa fa-plus-circle"></i> New User</button>
                        </div>
                        <div class="clearfix"></div>
                    </div>	
            <div class="portlet-body">
                <?php if (! empty($message)) { ?>
                    <div class="alert alert-danger" id="message">
                    <?php echo $message; ?>
                    </div>
                <?php } ?>
                <?php echo form_open(current_url(), 'role="form"');?>
                
                <fieldset>
                  <div class="form-group">
                            <input class="form-control" type="text" placeholder="Username or Email" name="login_identity"  />
                  </div>
            
                <div class="form-group">
                        <input type="password" placeholder="Password" id="password" name="login_password" class="form-control">
                        <?php 
                                # Below are 2 examples, the first shows how to implement 'reCaptcha' (By Google - http://www.google.com/recaptcha),
                                # the second shows 'math_captcha' - a simple math question based captcha that is native to the flexi auth library. 
                                # This example is setup to use reCaptcha by default, if using math_captcha, ensure the 'auth' controller and 'demo_auth_model' are updated.

                                # reCAPTCHA Example
                                # To activate reCAPTCHA, ensure the 'if' statement immediately below is uncommented and then comment out the math captcha 'if' statement further below.
                                # You will also need to enable the recaptcha examples in 'controllers/auth.php', and 'models/demo_auth_model.php'.
                                #/*
                                if (isset($captcha)) 
                                { 
                                        #echo "<li>\n";
                                        echo $captcha;
                                       # echo "</li>\n";
                                }
                                #*/

                                /* math_captcha Example
                                # To activate math_captcha, ensure the 'if' statement immediately below is uncommented and then comment out the reCAPTCHA 'if' statement just above.
                                # You will also need to enable the math_captcha examples in 'controllers/auth.php', and 'models/demo_auth_model.php'.
                                if (isset($captcha))
                                {
                                        echo "<li>\n";
                                        echo "<label for=\"captcha\">Captcha Question:</label>\n";
                                        echo $captcha.' = <input type="text" id="captcha" name="login_captcha" class="width_50"/>'."\n";
                                        echo "</li>\n";
                                }
                                #*/
                        ?>
                </div>
                <div class="checkbox">
                    <label>
                            <input type="checkbox" id="remember_me" name="remember_me" value="1" <?php echo set_checkbox('remember_me', 1); ?>><p class="pull-left"> Remember Me</p></label>
                </div>
                <br>
                <button class="btn btn-lg btn-green btn-block" type="submit" name="login_user" id="submit" value="Ingresar">Sign In</button>
                <a class="btn btn-block btn-social btn-facebook" href="javascript:auth_popup('<?php echo site_url('social/facebook') ?>', 500, 200)">
                    <i class="fa fa-facebook"></i> Sign in with Facebook
                  </a>
                  <a class="btn btn-block btn-social btn-twitter" href="javascript:auth_popup('<?php echo site_url('social/twitter'); ?>', 810, 440)">
                    <i class="fa fa-twitter"></i> Sign in with Twitter
                  </a>
            </fieldset>
            <br>
            <p class="small">
                <a href="<?php echo $base_url;?>auth/forgotten_password">Forgotten Password</a>
                <br/>
                <a href="<?php echo $base_url;?>auth/resend_activation_token">Resend activation token</a>
            </p>
        </form>
        </div>
        </div>
            <div>
                <h5>Dashboard Optimizado por <a href="http://globalresponse.cl">GlobalResponse</a></h5>
                <h5>Versión brindada para GPI DuocUC por <a href="mailto:midgardinrage@gmail.com">Camilo 'Ren' Bustos</a>.</h5>
            </div>
    </div>
    </div>
</div>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script src="<?=site_url('assets/themes/aero/js/plugins/bootstrap/bootstrap.min.js')?>"></script>
    <script src="<?=site_url('assets/themes/aero/js/plugins/slimscroll/jquery.slimscroll.min.js')?>"></script>
    <!-- HISRC Retina Images -->
    <script src="<?=site_url('assets/themes/aero/js/plugins/hisrc/hisrc.js')?>"></script>

    <!-- PAGE LEVEL PLUGIN SCRIPTS -->

    <!-- THEME SCRIPTS -->
    <script src="<?=site_url('assets/themes/aero/js/flex.js')?>"></script>
<script type="text/javascript">
    function auth_popup(url, width, height){
	var left = (screen.width/1.5)-(width/1.5);
	var top = (screen.height/1.5)-(height/1.5);
	window.open (url,"auth","toolbar=no,location=no,directories=no,status=no,scrollbars=no,menubar=0,resizable=1,width="+width+",height="+height+',top='+top+',left='+left);
    }
</script>