<div class="login_page">
    <div class="registration">
        <div class="panel-heading border login_heading">Confirm data registration
            <?php if (! empty($message)) { ?>
                    <div id="message">
                            <?php echo $message; ?>
                    </div>
            <?php } ?>
        </div>
        <form role="form" class="form-horizontal" action="<?=current_url()?>" method="POST">
            <div class="portlet portlet-default">
                <div class="portlet-heading">
                    <div class="portlet-title">
                        <h4>Personal Details</h4>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="portlet-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <h4>First Name</h4>
                            <input class="form-control" type="hidden" name="tw_id" value="<?php echo $user->id_str ?>" />
                            <input class="form-control" type="text" name="register_first_name" value="<?php echo $user->name ?>" />
                            <h4>Last Name</h4>
                            <input class="form-control" type="text" name="register_last_name" value="" />
                            <h4>Date Birth</h4>
                            <div id="sandbox-container">
                                <input type="text" class="form-control" name="date_bird" value="" >
                            </div>
                            <h4>Address</h4>
                            <input class="form-control" type="text" placeholder="address" id="address" name="register_address"/>
                            <h4>Gender</h4>
                            <label class="radio-inline">
                                <input type="radio" value="" id="gender_femele" name="gender" value="1" >Female
                            </label>
                            <label class="radio-inline">
                                <input type="radio"  value="" id="gender_male" name="gender" value="2" >Male
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="portlet portlet-default">
                <div class="portlet-heading">
                    <div class="portlet-title">
                        <h4>Contact Details</h4>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="portlet-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <h4>Phone Number</h4>
                            <input class="form-control" placeholder="phone number" type="text" id="phone_number" name="register_phone_number" value=""/>                    
                            <h4>Newsletter subscription</h4>
                            <div class="checkbox">
                                <label>
                                    <input class="form-control" type="checkbox" id="newsletter" name="register_newsletter" value="1" checked /> Suscribe
                                </label>
                            </div>
                            <h4>Privacity</h4>
                            <div class="checkbox">
                                <label>
                                    <input class="form-control" type="checkbox" id="public" name="public" value="1" checked /> Set public Profile
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="portlet portlet-default">
                <div class="portlet-heading">
                    <div class="portlet-title">
                        <h4>Login Details</h4>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="portlet-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <h4>Email address</h4>
                            <input class="form-control" placeholder="email address" type="email" id="email_address" name="register_email_address" value="" />
                            <h4>Username</h4>
                            <input class="form-control" placeholder="username" type="text" id="username" name="register_username" value="<?=$user->screen_name?>" class="tooltip_trigger"
                                        title="Set a username that can be used to login with."
                                />
                            <h4>Image</h4>
                            <input type="text" name="profile_image_url" value="<?= $user->profile_image_url?>" />
                            <img src="<?=$user->profile_image_url ?>" width="50" />
                            	
                        </div>
                    </div>
                </div>
            </div>

            <fieldset class="form-group">
                <legend>Register</legend>
                 <button class="btn btn-default" type="submit"  name="register_user" id="submit" value="Submit">SUBMIT</button>
            </fieldset>
        </form>
        <form action="/file-upload" class="dropzone" id="my-awesome-dropzone"></form>
	
    </div>
</div>