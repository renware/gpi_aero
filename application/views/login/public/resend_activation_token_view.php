<div class="wrapper">
  <!--\\\\\\\ wrapper Start \\\\\\-->
    <div class="login_page">
        <div class="login_content">
            <div class="panel-heading border login_heading">Reenviar token de activación</div>

            <?php if (! empty($message)) { ?>
                    <div id="message">
                            <?php echo $message; ?>
                    </div>
            <?php } ?>
				
            <?php echo form_open(current_url());?>  	
                <div class="form-group">
        
                    <div class="col-sm-10">
                                    <label for="identity">Email o Nombre de Usuario:</label>
                                    <input type="text" id="identity" name="activation_token_identity" value="" class="tooltip_trigger"
                                            title="Please enter either your email address or username defined during registration."
                                    />
                    </div>
                </div>
                <div class="form-group">
        
                    <div class="col-sm-10">
                            <label for="submit">Enviar Email:</label>
                            <input type="submit" name="send_activation_token" id="submit" value="Enviar" class="link_button large"/>
                    </div>
                </div>	
            <?php echo form_close();?>
        </div>
    </div>
</div>	