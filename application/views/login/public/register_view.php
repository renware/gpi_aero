<div class="login_page">
    <div class="registration">
        <div class="panel-heading border login_heading">registration now
            <?php if (! empty($message)) { ?>
                    <div id="message">
                            <?php echo $message; ?>
                    </div>
            <?php } ?>
        </div>
        <form id="details" role="form" class="form-horizontal" action="<?=current_url()?>" method="POST">
            <div class="portlet portlet-default">
                <div class="portlet-heading">
                    <div class="portlet-title">
                        <h4>Personal Details</h4>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="portlet-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <h4>First Name</h4>
                            <input class="form-control" type="text" id="first_name" placeholder="first name" name="register_first_name" value="<?php echo set_value('register_first_name');?>"/>
                            <h4>Last Name</h4>
                            <input class="form-control" type="text" placeholder="last name" id="last_name" name="register_last_name" value="<?php echo set_value('register_last_name');?>"/>
                            <h4>Date Birth</h4>
                            <div id="sandbox-container">
                                <input type="text" class="form-control" name="date_bird">
                            </div>
                            <h4>Address</h4>
                            <input class="form-control" type="text" placeholder="address" id="address" name="register_address" value=""/>
                            <h4>Gender</h4>
                            <label class="radio-inline">
                                <input type="radio" checked="" value="" id="gender_femele" name="gender" value="1" >Female
                            </label>
                            <label class="radio-inline">
                                <input type="radio" checked="" value="" id="gender_male" name="gender" value="2" >Male
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="portlet portlet-default">
                <div class="portlet-heading">
                    <div class="portlet-title">
                        <h4>Contact Details</h4>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="portlet-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <h4>Phone Number</h4>
                            <input class="form-control" placeholder="phone number" type="text" id="phone_number" name="register_phone_number" value="<?php echo set_value('register_phone_number');?>"/>                    
                            <h4>Newsletter subscription</h4>
                            <div class="checkbox">
                                <label>
                                    <input class="form-control" type="checkbox" id="newsletter" name="register_newsletter" value="1" <?php echo set_checkbox('register_newsletter',1);?>/> Suscribe
                                </label>
                            </div>
                            <h4>Privacity</h4>
                            <div class="checkbox">
                                <label>
                                    <input class="form-control" type="checkbox" id="public" name="public" value="1" checked /> Set public Profile
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="portlet portlet-default">
                <div class="portlet-heading">
                    <div class="portlet-title">
                        <h4>Login Details</h4>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="portlet-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <h4>Email address</h4>
                            <input class="form-control" placeholder="email address" type="email" id="email_address" name="register_email_address" value="<?php echo set_value('register_email_address');?>" class="tooltip_trigger"
                                        title="This page requires that upon registration, you will need to activate your account via clicking a link that is sent to your email address."
                                />
                            <h4>Username</h4>
                            <input class="form-control" placeholder="username" type="text" id="username" name="register_username" value="<?php echo set_value('register_username');?>" class="tooltip_trigger"
                                        title="Set a username that can be used to login with."
                                />
                            
                            <h4>Password</h4>
                            <small>
                                Password length must be more than <?php echo $this->flexi_auth->min_password_length(); ?> characters in length.<br/>Only alpha-numeric, dashes, underscores, periods and comma characters are allowed.
                            </small>
                            <input class="form-control" placeholder="password" type="password" id="password" name="register_password" value="<?php echo set_value('register_password');?>"/>
                            <input class="form-control" placeholder="confirm password" type="password" id="confirm_password" name="register_confirm_password" value="<?php echo set_value('register_confirm_password');?>"/>
                        </div>
                    </div>
                </div>
            </div>
            <input name="image" type="hidden" id="image">
            <input name="ref" type="hidden" id="ref" value="<?=$ref?>">
            <input name="register_user" type="hidden" value="Submit" />
    </form>
        <div class="row">
                    <div class="col-lg-12">
                        <div class="portlet portlet-default dropzone-portlet">
                            <div class="portlet-heading">
                                <div class="portlet-title">
                                    <h4>Upload your profile Image</h4>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="portlet-body">
                                <form id="my-awesome-dropzone" class="dropzone dz-clickable" action="<?=site_url("auth/upload_image")?>">
                                    <div class="dz-default dz-message"><span>Click here or Drop your file here to upload</span></div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
            <fieldset class="form-group">
                <legend>Register</legend>
                 <button class="btn btn-default" type="button"  name="register_user" id="submit" value="Submit" >SUBMIT</button>
            </fieldset>
        
    </div>	
	
</div>