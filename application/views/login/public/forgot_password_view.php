<div class="wrapper">
  <!--\\\\\\\ wrapper Start \\\\\\-->
    <div class="login_page">
        <div class="login_content">
            <div class="panel-heading border login_heading">Contraseña Olvidada</div>	

            <?php if (! empty($message)) { ?>
                    <div class="alert alert-danger" id="message">
                            <?php echo $message; ?>
                    </div>
            <?php } ?>
				
            <?php echo form_open(current_url(), 'class="form-horizontal"');?>
                <div class="form-group">
        
                    <div class="col-sm-10">
                        <label for="identity">Email o nombre de usuario:</label>
                    </div>
                </div>
                <div class="form-group">
                  <div class="col-sm-10">        
                        <input type="text" id="identity" name="forgot_password_identity" value="" class="tooltip_trigger"
                                title="Porfavor ingrese su email o nombre de usuario definido durante el registro."
                        />
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-sm-10">
                        <input type="submit" name="send_forgotten_password" id="submit" value="Enviar Email" class="link_button large"/>                        				
                    </div>
                </div>
            <?php echo form_close();?>
        </div>
    </div>
</div>	
