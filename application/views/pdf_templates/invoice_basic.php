<style type="text/css">
body {
	font-family: helvetica;
	font-size: 10pt;
}
.style1 {
	font-size: 26pt;
	font-weight: bold;
	color: #CCCCCC;
}
.style2 {
	font-size: 18pt;
	font-weight: bold;
}
.even {
	background-color: #eee;
	border-bottom: 1pt solid #ccc;
	border-left: 1pt solid #ccc;
}
.odd {
	background-color: #fff;
	border-bottom: 1pt solid #ccc;
	border-left: 1pt solid #ccc;
}
#workdata {
	border-top: 1pt solid #ccc;
	border-right: 1pt solid #ccc;
}
</style>
<body>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="50%" valign="middle" height="10pt"><div class="style2"><?php if ($image) { echo "<img src=\"$image\" />"; } else { echo $company['name']; } ?></div></td>
    <td width="50%" valign="middle"><div align="right" class="style1"><?php if ($invoice['paid']) echo "PAID "; ?>Boleta</div></td>
  </tr>
  <tr>
	<td>
<?=nl2br($company['invoice_address'])?>
	</td>
	<td align="right">
Fecha de impresi&oacute;n: <?=local_date($invoice['created'])?><br />
Fecha de Vencimiento: <?=local_date($invoice['duedate'])?><br />
Boleta #: <?=zero_fill($invoice['id'], 5)?>
	</td>
  </tr>
  <tr>
    <td><br /><br /><strong>A nombre de</strong>: <?=$invoice['client_name']?><br />&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2"><table id="workdata" width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="62%"><strong>Notas</strong></td>
        <td width="10%"><strong>Fecha</strong></td>
        <td width="12%"><strong>Tipo</strong></td>
		<td width="6%"><div align="right"><strong>Horas</strong></div></td>
        <td width="10%"><div align="right"><strong>Precio</strong></div></td>
      </tr>
<?php
$i = 1;
$time = 0;
$total_cost = 0.00;

foreach($expenses AS $expense) {
	$row = $i++ % 2 ? 'even' : 'odd';
	if ($expense['billable']) {
		$total_cost += $expense['amount'];
	}
?>
	  <tr class="<?=$row?>">
        <td><?=$expense['content']?></td>
        <td><?=date('n/j/Y', strtotime($expense['date']))?></td>
        <td><?=$expense['expensetype_name']?></td>
        <td>&nbsp;</td>
        <td><div align="right"><?php if ($expense['billable']) echo '$'.$expense['amount']; else echo "N/A"; ?></div></td>
      </tr>
<?php
}


if ($total_cost != $invoice['amount']) {
	$adjustment = number_format($invoice['amount'] - $total_cost, 2);
?>




	  <tr class="odd">
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td><strong>Etc.</strong>:</td>
        <td>&nbsp;</td>
        <td align="right">$<?=$adjustment?></td>
	  </tr>
<?php
}
?>
      <tr class="odd">
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td><strong>Total</strong>:</td>
        <td><div align="right"><?=hour_float_readable($time)?></div></td>
        <td align="right"><strong>$<?=$invoice['amount']?></strong></td>
      </tr>
      
      
      
      
      
      
      
      
    </table></td>
  </tr>
</table>

<?php

echo"<table><tr><p></p><th width='100'><strong>Producto</strong></th><th width =\"950\"><strong>Detalle</strong></th><th width='80'><strong>Cantidad</strong></th><th width='20' class='monetary' style='padding-right: 8px;'><strong>Monto</strong></th></tr>\n";
if ($productos) {

	foreach($productos AS $producto) {
		
		echo"<tr><td class=\"odd\">{$producto['nombre_producto']}</td>";
		if (!empty($producto['estado_actual'])){
			$cantidad=$producto['estado_actual']-$producto['cantidad'];
			echo"<td> <strong>Estado Anterior: </strong>{$cantidad}  <strong>Estado Actual  </strong>{$producto['estado_actual']}</td>";
		}else{
			echo"<td>{$producto['detalle']}</td>";
		}
		echo"<td align = 'center'>{$producto['cantidad']} {$producto['unidad']}</td><td class='monetary' style='padding-right: 8px;'>\${$producto['monto']}</td>";
		echo"</tr>";
	}
	echo"</table>";
}

?>







<div><strong>Notas:</strong></div>
<div><?=$invoice['content']?></div>






