<div class="login_page">
    <div class="registration">
        <div class="panel-heading border login_heading">Ingreso de Stock
            <?php if (! empty($message)) { ?>
                    <div id="message">
                            <?php echo $message; ?>
                    </div>
            <?php } ?>
        </div>
        <form id="details" role="form" class="form-horizontal" action="<?=current_url()?>" method="POST">
            <div class="portlet portlet-default">
                <?php echo form_open(current_url());?>  	
                <div class="portlet-heading">
                    <div class="portlet-title">
                        <h4>Detalles</h4>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="portlet-body">
                    <div class="row">
                        <div class="col-lg-12">
                            
                            <h4>Nombre</h4>
                            <select class="form-control" name="product_id" id="product_name">
                                <option selected="selected">[Elegir de la lista]</option>
                                <?php
                                    foreach($productos as $producto) { ?>
                                      <option value="<?= $producto['id_producto'] ?>"><?= $producto['nombre'] ?></option>
                                 <?php
                                 } ?>
                            </select>
                              
                            
                            <h4>Disponibilidad a Procesar</h4>
                            <div class="col-lg-6">
                                <input class="form-control" type="number" placeholder="0" id="address" name="product_available" id="product_available" value=""/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
          
            <fieldset class="form-group">
                <legend>Registrar Nuevo Stock</legend>
                 <input class="btn btn-default" type="submit"  name="add_stock" id="submit" value="Registrar"></input>
            </fieldset>
            <?php echo form_close();?>
        </form>
    </div>	
	
</div>
<script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>
<script>
    $(function() {
  
    // Setup form validation on the #register-form element
    $("#details").validate({
    
        // Specify the validation rules
        rules: {
            product_name: "required",
            product_type: "required",
    
        },
        
        // Specify the validation error messages
        messages: {
            product_name: "Por favor seleccione un producto de la lista",
            product_available: "Por favor ingrese una cantidad de stock"
        },
        
        submitHandler: function(form) {
            form.submit();
        }
    });

  });
  
</script>