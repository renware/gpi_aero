<div id="row">
    <div class="col-md-12">
          <div class="block-web">
            <div class="header">
                <div class="actions"> <a class="minimize" href="#"><i class="fa fa-chevron-down"></i></a> <a class="refresh" href="#"><i class="fa fa-repeat"></i></a> <a class="close-down" href="#"><i class="fa fa-times"></i></a> </div>
                <h3 class="content-header">Bienvenido</h3>
            </div>
            <?php if (! empty($message)) { ?>
                    <div id="message">
                            <?php echo $message; ?>
                    </div>
            <?php } ?>
				
            <div class="w100 frame">							
                    <h2>Administrador de Stock</h2>
					<a class="btn btn-primary" href="<?php echo site_url("products/agregarstock")?>">Agregar Stock de Productos</a>
					
					<hr/>
					
					
					<hr/>
					
            </div>
             
            <div>
                      <?php echo form_open(current_url());	?>
                                <table class="table table-striped table-hover table-bordered">
                                            <thead>
                                                    <tr>
                                                            <th>ID Operación</th>
                                                            <th>Nombre Producto</th>
                                                            <th>Fecha de Operación</th>
                                                            <th>Cantidad</th>
                                                            <th 
                                                                    title="Despliega información detallada.">
                                                                    Operador
                                                            </th>
                                                                                                       
                                                    </tr>
                                            </thead>
                                            <?php if (!empty($stock)) { ?>
                                            <tbody>
                                                    <?php foreach ($stock as $stk) { ?>
                                                    <tr>
                                                            <td>
                                                                    <?php echo $stk['id_stock'];?>
                                                            </td>
                                                            <td>
                                                                    <?php echo $stk['nombre'];?>
                                                            </td>
                                                             <td>
                                                                    <?php echo $stk['fecha'];?>
                                                            </td>
                                                            <td>
                                                                    <?php echo $stk['cantidad'];?>
                                                            </td>
                                                            <td class="align_ctr">
                                                                    <?php echo $stk['encargado'];?>
                                                            </td>
                                                                                                                 
                                                            <td class="align_ctr">
                                                                <a href="<?php echo base_url().'products/stkedit/'.$stk['id_stock'];?>">
                                                                   <i class="fa fa-pencil"></i>
                                                                </a>
                                                            </td>
                                                    </tr> 
                                            <?php } ?>
                                            </tbody>
                                            
                                    <?php } else { ?>
                                            <tbody>
                                                    <tr>
                                                            <td colspan="7" class="highlight_red">
                                                                    No hay stock de productos disponibles al momento.
                                                            </td>
                                                    </tr>
                                            </tbody>
                                    <?php } ?>
                                    </table>

                          
                <?php echo form_close();?>
                  
                  
            </div>
              
        </div>
    </div>
</div>	
