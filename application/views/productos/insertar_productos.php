<div class="login_page">
    <div class="registration">
        <div class="panel-heading border login_heading">Ingreso de Productos
            <?php if (! empty($message)) { ?>
                    <div id="message">
                            <?php echo $message; ?>
                    </div>
            <?php } ?>
        </div>
        <form id="details" role="form" class="form-horizontal" action="<?=current_url()?>" method="POST">
            <div class="portlet portlet-default">
                <?php echo form_open(current_url());?>  	
                <div class="portlet-heading">
                    <div class="portlet-title">
                        <h4>Detalles</h4>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="portlet-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <h4>Nombre</h4>
                            <input class="form-control" type="text" id="product_name" placeholder="<Nombre Producto>" name="product_name" id="product_name" value=""/>
                          
                            <h4>Tipo</h4>
                            <input type="text" placeholder="<Tipo Producto>" class="form-control" name="product_type" id="product_type">
                           
                            <h4>Detalle</h4>
                            <input class="form-control" type="text" placeholder="<Detalles>" id="address" name="product_detail" id="product_detail" value=""/>
                            
                            <h4>Disponibilidad Inicial</h4>
                            <div class="col-lg-6">
                                <input class="form-control" type="number" placeholder="0" id="address" name="product_available" id="product_available" value=""/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
          
            <fieldset class="form-group">
                <legend>Registrar Nuevo Producto</legend>
                 <input class="btn btn-default" type="submit"  name="add_product" id="submit" value="Registrar"></input>
            </fieldset>
            <?php echo form_close();?>
        </form>
    </div>	
	
</div>
<script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>
<script>
    $(function() {
  
    // Setup form validation on the #register-form element
    $("#details").validate({
    
        // Specify the validation rules
        rules: {
            product_name: "required",
            product_type: "required",
            product_detail: "required",
            product_available: "required"
            
           
        },
        
        // Specify the validation error messages
        messages: {
            product_name: "Por favor ingrese un nombre de producto",
            product_type: "Por favor ingrese el tipo de producto",
            product_detail: "Por favor ingrese detalles básicos del producto",
            product_available: "Por favor ingrese stock inicial de productos disponibles"
        },
        
        submitHandler: function(form) {
            form.submit();
        }
    });

  });
  
</script>