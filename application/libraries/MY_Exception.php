<?php

class MY_Exceptions extends CI_Exceptions {

    private $CI;
    private $dist;

    function MY_Exceptions() {
        parent::CI_Exceptions();
        $this->CI = & get_instance();
        $this->dist = $this->CI->config->item('dist');
    }

    function show_404() {
        $this->CI->load->view('errors/error_404_v2');
        echo $this->CI->output->get_output();
        exit;
    }

}
