<?php  defined('BASEPATH') OR exit('No direct script access allowed');


class Voucher{
    
    public function __construct()
	{
		parent::__construct();
                $this->load->model("voucher_model");
        }
        
        
        public function index($option=false)
        {
            //title,description,meta
            $this->output->set_common_meta('Vouchers', 'List', '');
            $this->output->set_canonical(site_url());
            if ($this->flexi_auth->is_privileged('View vouchers'))
            {
                $this->load->library('grocery_CRUD');
                $crud = new grocery_CRUD();
                $crud->set_table('vouchers');
                #$crud->columns('id_voucher','text','status','created','points','expiration','created','status');
                $crud->set_relation('status', 'status', 'status_name');
                $crud->set_relation('category', 'categories', 'name');
                $crud->set_relation('id_location', 'locations', 'name');
                $crud->set_relation('id_user', 'user_profiles', '{upro_first_name} {upro_last_name}');
                $crud->required_fields("id_location","text","total_points");
                $crud->unique_fields("text");
                $crud->display_as("points","Points (aditional to category)");
                $state_info = $crud->getStateInfo();
                $text = "text";
                if($option == 'edit')
                {
                    $primary_key = $state_info->primary_key;
                    if($this->qr_model->claimeds($primary_key))
                       $text="";
                    else
                        $text = "text";
                }
                
                if (!$this->flexi_auth->is_privileged('Add vouchers'))
                    $crud->unset_add();
                if (!$this->flexi_auth->is_privileged('Edit vouchers'))
                    $crud->unset_edit();
                if (!$this->flexi_auth->is_privileged('Delete vouchers'))
                    $crud->unset_delete();
                
                
                $output = $crud->render();
                
                $this->load->view('crud',$output); 
            }else
                $this->error_messages ("You don't have permissions");
	    
        }
    
        
        public function getVoucher($id_qr=0)
        {
            
        return $this->db->where("id_qr",$id_qr)->get("vouchers")->row_array();
        }
}