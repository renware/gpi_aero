<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
if ( session_status() == PHP_SESSION_NONE ) {
session_start();
}
// Autoload the required files
require_once( APPPATH . 'libraries/fbapi/autoload.php' );
use Facebook\FacebookRedirectLoginHelper;
use Facebook\FacebookSession;
use Facebook\FacebookRequest;
class Fb {
        var $ci;
        var $helper;
        var $session;
        var $permissions;
        var $facebook;
        public $user_id = null;
        
        public function __construct() {    
        $this->ci =& get_instance();
        $this->ci->load->config('facebook');
        $this->facebook=$this->ci->config->item('facebook');
        $this->permissions = $this->facebook["permissions"] ;
        // Initialize the SDK
        FacebookSession::setDefaultApplication( $this->facebook["app_id"], $this->facebook["app_secret"] );
        // Create the login helper and replace REDIRECT_URI with your URL
        // Use the same domain you set for the apps 'App Domains'
        // e.g. $helper = new FacebookRedirectLoginHelper( 'http://mydomain.com/redirect' );
        $this->helper = new FacebookRedirectLoginHelper( site_url("social/facebook") );
        if ( $this->ci->session->userdata('fb_token') ) {
        $this->session = new FacebookSession( $this->ci->session->userdata('fb_token') );
        // Validate the access_token to make sure it's still valid
        try {
        if ( ! $this->session->validate() ) {
        $this->session = null;
        }
        } catch ( Exception $e ) {
        // Catch any exceptions
        $this->session = null;
        }
        } else {
        // No session exists
        try {
        $this->session = $this->helper->getSessionFromRedirect();
        } catch( FacebookRequestException $ex ) {
        // When Facebook returns an error
        } catch( Exception $ex ) {
        // When validation fails or other local issues
        }
        }
        if ( $this->session ) {
        $this->ci->session->set_userdata( 'fb_token', $this->session->getToken() );
        $this->session = new FacebookSession( $this->session->getToken() );
        }
        }
        /**
        * Returns the login URL.
        */
        public function login_url() {
        return $this->helper->getLoginUrl( $this->permissions );
        }
        /**
        * Returns the current user's info as an array.
        */
        public function get_user() {
        if ( $this->session ) {
        /**
        * Retrieve User’s Profile Information
        */
        // Graph API to request user data
        $request = ( new FacebookRequest( $this->session, 'GET', '/me' ) )->execute();
        // Get response as an array
        $user = $request->getGraphObject()->asArray();
        if(empty($this->user_id))
            $this->user_id=$user["id"];
        return $user;
        }
        return false;
    }
    
    function image_url($uid = null, $large = true){
	if ($uid == null) $uid = $this->user_id;
	return 'https://graph.facebook.com/'.$uid.'/picture' . ($large == true ? '?type=large' : '' );
    }
} 