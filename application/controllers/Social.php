<?php
/******
* Codeigniter Social Register/Auth Sub system
* @author Fotis Alexandrou - fotis@redmark.gr
* @version 0.1
* @license Free/Open source
* http://www.redmark.gr
* Please read README.txt first
* It requires additional work IT IS NOT plug-n play
******/
class Social extends MY_Controller {

    /**
     * Constructor - Access Codeigniter's controller object
     * 
     */
    function __construct() {
	parent::__construct(true);
	//Load the session library - If session lib is autoloaded remove this from here
	$this->load->driver('session');
	//Load the user helper - If the helper is autoloaded remove this from here
	$this->load->helper('user');
	$this->load->helper('url');
	//Load the user model
	$this->load->model('social_model');
        
    }
    
    private function __init($template='blank',$jQuery=true)
	{
            $this->output->set_template($template);
            $this->load->css(site_url('assets/themes/aero/css/plugins/pace/pace.css'));
            $this->load->css(site_url('assets/themes/aero/css/plugins/bootstrap/css/bootstrap.min.css'));
            $this->load->css(site_url('assets/themes/aero/icons/font-awesome/css/font-awesome.min.css'));
            $this->load->css(site_url('assets/themes/aero/css/plugins/messenger/messenger.css'));
            $this->load->css(site_url('assets/themes/aero/css/plugins/messenger/messenger-theme-flat.css'));
            $this->load->css(site_url('assets/themes/aero/css/plugins/daterangepicker/daterangepicker-bs3.css'));
            $this->load->css(site_url('assets/themes/aero/css/plugins/morris/morris.css'));
            $this->load->css(site_url('assets/themes/aero/css/plugins/jvectormap/jquery-jvectormap-1.2.2.css'));
            $this->load->css(site_url('assets/themes/aero/css/plugins/datatables/datatables.css'));
            $this->load->css(site_url('assets/themes/aero/css/style.css'));
            $this->load->css(site_url('assets/themes/aero/css/demo.css'));
            $this->load->css(site_url("assets/themes/aero/css/register.css"));
            $this->load->js_inside(site_url("assets/themes/aero/js/plugins/bootstrap-datepicker/bootstrap-datepicker.js"));
        
            if($jQuery)
                $this->load->js('assets/themes/aero/js/jquery.min.js');
            
            $this->load->js(site_url('assets/themes/aero/js/plugins/pace/pace.js'));
            $this->load->js_inside(site_url("assets/themes/aero/js/register.js"));
            $this->load->js('assets/themes/aero/js/global.js');
            $this->load->js(site_url('assets/themes/aero/js/plugins/bootstrap/bootstrap.min.js'));
            $this->load->js(site_url('assets/themes/aero/js/plugins/slimscroll/jquery.slimscroll.min.js'));
            $this->load->js(site_url('assets/themes/aero/js/plugins/popupoverlay/jquery.popupoverlay.js'));
            $this->load->js(site_url('assets/themes/aero/js/plugins/popupoverlay/defaults.js'));
           
            $this->load->js(site_url('assets/themes/aero/js/plugins/hisrc/hisrc.js'));
            $this->load->js(site_url('assets/themes/aero/js/plugins/messenger/messenger.min.js'));
            $this->load->js(site_url('assets/themes/aero/js/plugins/messenger/messenger-theme-flat.js'));
            $this->load->js(site_url('assets/themes/aero/js/plugins/daterangepicker/moment.js'));
            $this->load->js(site_url('assets/themes/aero/js/plugins/daterangepicker/daterangepicker.js'));
            $this->load->js(site_url('assets/themes/aero/js/plugins/daterangepicker/daterangepicker.js'));
            
            
            $this->load->js(site_url('assets/themes/aero/js/flex.js'));      
            $this->load->js(site_url('assets/themes/aero/js/demo/dashboard-demo.js')); 
	}
    /**
     * Placeholder for default functions to be executed
     */
    public function index() {
	if (!is_logged()) {
	    redirect('social/login');
	}
	//Get the user id
	//Load a home view
	$user = get_user();
	
	$this->load->view('social/home', array('user'=>$user));
    }

    /**
     * Displays the login screen
     */
    public function login() {
	$this->load->library('fb');
	$this->load->library('tweet');
	
	//Look for errors from previous steps
	$error = $this->session->flashdata('register_error');
	$this->load->view('login/social/login', array('fb' => $this->fb, 'twitter' => $this->tweet, 'error' => $error));
    }

    /**
     * Shows user's register screen
     */
    public function register() {
        $this->__init();
	$method = $this->uri->rsegment(3);
	
	//Gets the error from previous steps
	$error = $this->session->flashdata('register_error');

	if ($this->input->post('register_user') != null) {

                $email = $this->input->post('register_email_address');
                $username = $this->input->post('register_username');
                if(!$username)
                    $username=$email;
                $profile_data = array(
                        'upro_first_name' => $this->input->post('register_first_name'),
                        'upro_last_name' => $this->input->post('register_last_name'),
                        'upro_address' => $this->input->post('register_address'),
                        'upro_gender' => $this->input->post('gender'),
                        'upro_date_bird' => $this->input->post('date_bird'),
                        'upro_public' => $this->input->post('pubic'),
                        'upro_phone' => $this->input->post('register_phone_number'),
                        'upro_newsletter' => $this->input->post('register_newsletter')
                );

                // Set whether to instantly activate account.
                // This var will be used twice, once for registration, then to check if to log the user in after registration.
                $instant_activate = TRUE;
		if ($method == 'twitter') {
                    $perfil["uacc_coming_from"]=2;
		    $perfil["external_id"]=$this->input->post('tw_id');
                    $password=md5($perfil["external_id"]);
		} else if ($method == 'facebook') {
                    $perfil["uacc_coming_from"]=3;
                    $perfil["external_id"]=$this->input->post('fb_id');
                    $password=md5($perfil["external_id"]);
		} else {
		    $error = 'You must select a valid login method';
		}
                $response = $this->flexi_auth->insert_user($email, $username, $password, $profile_data, 1, $instant_activate);
                if($response)
                {
                    $this->social_model->update_account($response,$perfil);
                    $this->flexi_auth->login($email, $password, true);
                    redirect(site_url());
                }
                else
                    $error = 'Registration failed please try again';
	}

	if ($method == 'twitter') {
	    //Form'submitted - TODO: Insert form validation
	    $this->load->library('twitter');
	    if (!$this->twitter->logged_in()) {
                    redirect(site_url());
	    }

	    $user = $this->twitter->get_user_data();
	    $this->load->view('login/social/register_twitter', array('user' => $user, 'error' => $error));
	    return;
	} else if ($method == 'facebook') {
	    $this->load->library('fb');
            $user=$this->fb->get_user();
            if (!$user){
		redirect ( $this->fb->login_url(array('next'=> current_url())));
	    }
	    //Image on Graph API is loaded separately
	    $image = $this->fb->image_url($user["id"]);
            $this->load->js(site_url("assets/themes/aero/js/plugins/dropzone/dropzone.js"));
            $this->load->css(site_url("assets/themes/aero/css/plugins/dropzone/css/dropzone.css"));
	    $this->load->view('login/social/register_facebook', array('user' => $user, 'image'=>$image, 'error' => $error));
	    return;
	}
	redirect('login/social/login');
    }

    /**
     * Logs user in with facebook
     */
    public function facebook($soure="w") {
	$this->load->library('fb');
        $fb_user=$this->fb->get_user();
	if (!$fb_user){
	    redirect ( $this->fb->login_url( current_url() ));
	}
	if (empty($fb_user)){
	    $error = "FACEBOOK LOGIN FAILED - USER US EMPTY. FILE: " . __FILE__ . " LINE: " . __LINE__;
	    $this->session->set_flashdata('register_error', $error);
	}else{
	    $this->social_model->set_facebook_id($fb_user['id']);
	    $user = $this->social_model->get_by_facebook();
	    if (!empty($user) && !empty($user->id) && is_numeric($user->id)){
	    	//TODO: Make things a bit more secure here
		//Login & Redirect home
                $this->flexi_auth->login($user->uacc_email, md5($user->id), true);
		$this->load->view('login/social/redirect_home');
		return;
	    }
	}
	//Go to the registeration page
	$this->load->view('login/social/redirect', array('method' => 'facebook'));
    }

    /**
     * Logs user in with twitter
     */
    
    public function twitter($soure="w") {
        $this->load->library('twitter');
        if(!$this->session->userdata('access_token') && !$this->session->userdata('access_token_secret'))
        {
            $this->twitter->auth();
        }
	//Go to the registeration page
	$this->load->view('login/social/redirect', array('method' => 'twitter'));
    }
    
    public function twitter_callback()
    {
        $this->load->library('twitter');
        if($this->twitter->callback())
        {
            $tw_user = $this->twitter->get_user_data();
            if (empty($tw_user)) {
                #TODO: Localize error messages etc
                $error = 'TWITTER LOGIN FAILED - USER IS EMPTY FILE: ' . __FILE__ . ' LINE: ' . __LINE__;
                $this->session->set_flashdata('register_error', $error);
            }else{
                $this->social_model->set_twitter_id($tw_user->id_str);
                $user = $this->social_model->get_by_twitter();
                if (!empty($user) && !empty($user->id) && is_numeric($user->id)){
                    //TODO: Make things a bit more secure here
                   # $this->_login($user->id, 'twitter');
                    $this->load->view('login/social/redirect_home');
                }
            }
        }
        else 
            redirect (site_url());
        $this->load->view('login/social/redirect', array('method' => 'twitter'));
    }

    /**
     * Stores a session variable
     */
    private function _login($user_id, $login_type) {
	//NOTE: Maybe a little workaround here, in order to make things more secure etc.
	$this->session->set_userdata('user_id', $user_id);
	$this->session->set_userdata('login_type', $login_type);
    }

    /**
     * Removes from session
     */
    public function logout() {
	$login_type = $this->session->userdata('login_type');
	$param = (int)$this->input->get('ret');
	if ($param != 1){
	    if ($login_type == 'facebook'){
		$this->_fb_logout();
	    }else if ($login_type == 'twitter'){
		$this->_tw_logout();
	    }
	}else{
	    $this->session->unset_userdata('user_id');
	    $this->session->unset_userdata('login_type');
	}
	redirect('/');
    }
    
    
    /**
     * Facebook has an issue with cookies unsetting, so...
     */
    private function _fb_logout() {

	$base_url = $this->config->item('base_url');

	if (array_key_exists('HTTP_REFERER', $_SERVER)) {
	    $refer = $_SERVER['HTTP_REFERER'];
	} else {
	    $refer = null;
	}

	$this->load->library('fb');
	$param = (int) $this->input->get('ret');
	//Local call
	if ($param != 1) {
	    //Store referer into a session variable
	    $url = $this->fb->logout_url(current_url() . '?ret=1');
	    redirect($url);
	} else {
	    $this->load->config('facebook');

	    $app_id = $this->config->item('facebook_app_id');
	    $api_key = $this->config->item('facebook_api_key');

	    $cookie_name = 'fbs_' . $app_id;

	    $dom = '.' . $_SERVER['HTTP_HOST'];

	    if (array_key_exists($cookie_name, $_COOKIE)) {
		setcookie($cookie_name, null, time() - 4200, '/', $dom);
		unset($_COOKIE[$cookie_name]);
	    }

	    if (array_key_exists($cookie_name, $_REQUEST)) {
		unset($_REQUEST[$cookie_name]);
	    }

	    $cookies = array($api_key . '_expires', $api_key . '_session_key', $api_key . '_ss', $api_key . '_user', $api_key, 'base_domain_' . $api_key);

	    foreach ($cookies as $var) {
		if (array_key_exists($var, $_COOKIE)) {
		    setcookie($var, null, time() - 4200, '/', $dom);
		    unset($_COOKIE[$var]);
		}

		if (array_key_exists($var, $_REQUEST)) {
		    unset($_REQUEST[$var]);
		}
	    }

	    if ($goto == null || $goto == '') {
		$goto = site_url('/');
	    }

	    $session = $this->fb->client->getSession();

	    redirect($goto);
	}
    }
    
    /**
     * Logout from twitter
     */
    private function _tw_logout(){
	$this->load->library('tweet');
	$this->tweet->set_callback(current_url().'?ret=1');
	$this->tweet->logout();
    }

}