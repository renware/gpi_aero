<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/* CONTROLADOR DEL MODULO DE REPARTO */
/* NO ALTERAR VARIABLES DEL CONSTRUCTOR, SE PUEDEN AGREGAR CONDICIONES, MÁS NO QUITAR LAS QUE YA ESTÁN */

class Repartos extends MY_Controller  {
 
        function __construct() 
        {
            parent::__construct();



            // Carga de variables generales
            $this->load->helper('url');
            $this->load->vars('base_url', site_url());
            $this->load->vars('includes_dir', site_url().'/includes');
            $this->load->vars('current_url', $this->uri->uri_to_assoc(1));

            // VARIABLE GLOBAL (LOS DATOS SON ENVIADOS INCLUSO A LAS VISTAS FINALES DEL USUARIO) 
            $this->data = null;
            

            //CARGA DEL TEMA GRAFICO (TODOS LOS SCRIPTS ESTAN ACA)
            $this->load->js(site_url('assets/themes/aero/js/jquery.js'));
            $this->_init();

            //CARGA DE MODELOS RELACIONADOS (para acceso de datos)

            //$this->load->model("product_model");

            //CARGA DE LIBRERÍAS, NO QUITAR LA DE GROCERY CRUD
            $this->load->library('grocery_CRUD');
        }


    /* COMENZAR A DESARROLLAR EL MODULO EN METODOS APARTE DESDE ACA */
    function index()
    {
        //proceso inicial que se realiza al entrar a sitio/clientes
        
        $variable = "algo";
        if(1==2)
            echo 'no pasa nada';
        
        $this->metodo();
    }
    
    function metodo()
    {
        //proceso que se realiza tras una llamada especifica o entrando a sitio/clientes/metodo
        $data["output"] = "llame un metodo desde repartos";
        
        //una llamada a vista busca un archivo en la carpeta views, y además puede enviarse la variable global como parametro
        //con información para el usuario
        $this->load->view("vuelo/vuelo_view",$data);
    }
    
}