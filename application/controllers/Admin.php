<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends MY_Controller {
 
    function __construct() 
    {
        parent::__construct();

        // Check user is logged in via either password or 'Remember me'.
        // Note: Allow access to logged out users that are attempting to validate a change of their email address via the 'update_email' page/method.
        if (! $this->flexi_auth->is_logged_in() && $this->uri->segment(2) != 'update_email')
        {
                // Set a custom error message.
                $this->flexi_auth->set_error_message('You must login to enter this area.', TRUE);
                $this->session->set_flashdata('message', $this->flexi_auth->get_messages());
                redirect('auth');
        }

        $this->load->model("admin_model");
    }
        
    public function usuarios($option=false)
    {
        $this->_init();
        //title,description,meta
        $this->output->set_common_meta('Usuarios', 'List', '');
        $this->output->set_canonical(site_url());
        if ($this->flexi_auth->is_privileged('View locations'))
        {
            $this->load->library('grocery_CRUD');
            $crud = new grocery_CRUD();
            $crud->set_theme('aero');
            $crud->set_table('usuarios');
            $crud->set_relation('status_location', 'status', 'status_name');
            
            if (!$this->flexi_auth->is_privileged('Add locations'))
                $crud->unset_add();
            if (!$this->flexi_auth->is_privileged('Edit locations'))
                $crud->unset_edit();
            if (!$this->flexi_auth->is_privileged('Delete locations'))
                $crud->unset_delete();

            $output = $crud->render();

            $this->load->view('crud',$output); 
        }else
            redirect (site_url());

    }
        
        public function categories($option=false)
        {
            $this->_init();
            //title,description,meta
            $this->output->set_common_meta('Qr-codes', 'Categories', '');
            $this->output->set_canonical(site_url());
            if ($this->flexi_auth->is_privileged('View qr-codes'))
            {
                $this->load->library('grocery_CRUD');
                $crud = new grocery_CRUD();
                $crud->set_theme('aero');
                $crud->set_table('categories');
                $crud->set_relation('status', 'status', 'status_name');
                $crud->set_relation('coupon_quantity_in_time_format', 'times_format', 'name_format');
                $crud->set_relation('coupon_expiration_format', 'times_format', 'name_format');
                $crud->display_as("age_limit","Age Limit (years)");
                
                if (!$this->flexi_auth->is_privileged('Add qr-codes'))
                    $crud->unset_add();
                if (!$this->flexi_auth->is_privileged('Edit qr-codes'))
                    $crud->unset_edit();
                if (!$this->flexi_auth->is_privileged('Delete qr-codes'))
                    $crud->unset_delete();
                
                $output = $crud->render();
                
                $this->load->view('crud',$output); 
            }else
                $this->error_messages ("You don't have permissions");
	    
        }
        
        public function schedule()
        {
            $this->_init("aero",true);
            
            if ($this->flexi_auth->is_privileged('Schedule'))
            {
                //title,description,meta
                $this->output->set_common_meta('Schedule', 'Events', '');
                $this->output->set_canonical(site_url());
                $this->load->js(site_url("assets/themes/aero/js/loader.js"));
                $this->load->css(site_url("assets/themes/aero/css/plugins/fullcalendar/fullcalendar.css"));
                $this->load->css(site_url("assets/themes/aero/css/plugins.css"));
                
                $this->load->js(site_url("assets/themes/aero/js/plugins/moment/moment.min.js"));
                $this->load->js(site_url("assets/themes/aero/js/plugins/fullcalendar/jquery-ui.custom.min.js"));
                $this->load->js(site_url("assets/themes/aero/js/plugins/fullcalendar/fullcalendar.min.js"));
                $this->load->js_inside(site_url("assets/themes/aero/js/schedule/schedule.js"));
                $data["events"]=$this->admin_model->getEvents();
                $this->load->view("admin/schedule/schedule",$data);
            }else
            {
                $this->output->unset_template();
                $this->error_messages ("You don't have permissions");
            }
            
        }
        
        public function new_event()
        {
            if($this->input->get("modal"))
                $this->_init("blank",false);
            else
                $this->_init("aero",true);
            
            if ($this->flexi_auth->is_privileged('Schedule'))
            {
                $this->load->js(site_url("assets/themes/aero/js/loader.js"));
                $this->load->js_inside(site_url("assets/themes/aero/js/plugins/bootstrap-timepicker/bootstrap-timepicker.min.js"));
                $this->load->js_inside(site_url("assets/themes/aero/js/plugins/bootstrap-datepicker/bootstrap-datepicker.js"));
                
                $this->load->js_inside(site_url("assets/themes/aero/js/schedule/new_event.js"));
                
                $this->load->js(site_url("assets/themes/aero/js/plugins/fullcalendar/fullcalendar.min.js"));
                
                $data["locations"]=$this->admin_model->getLocations();
                $data["categories"]=$this->admin_model->getCategories();
                $this->load->view("admin/schedule/new_event",$data);
            }else
            {
                $this->output->unset_template();
                $this->error_messages ("You don't have permissions");
            }
        }
        
        public function add_event()
        {
            if ($this->flexi_auth->is_privileged('Schedule'))
            {
                $data["name_event"]=$this->input->post("event_name");
                $data["category"]=$this->input->post("eventCategory");
                $data["description"]=$this->input->post("event_description");
                $data["all_day"]=$this->input->post("alldayEvent");
                $data["name_event"]=$this->input->post("event_name");
                $hour_start=$this->input->post("hour_begin");
                $data["date_start"]=$this->input->post("date_begin")." ".$hour_start;
                $hour_end=$this->input->post("hour_end");
                $data["date_end"]=$this->input->post("date_end")." ".$hour_end;
                $inserted = $this->admin_model->saveEvent($data);
                $locations=$this->input->post("locations");
                $locs["id_event"]=$inserted;
                if(!empty($locations))
                {
                    foreach($locations as $loc)
                    {
                        $locs["id_location"]=$loc;
                        $this->admin_model->saveEventLocations($locs);
                    }
                }
                $this->load->model("qr_model");
                $qr["event"]=$inserted;
                $qr["created"]=$data["date_start"];
                $qr["expiration"]=$data["date_end"];
                $qr["category"]=$data["category"];
                $qr["points"]=$this->input->post("points");
                
                if($locations)
                    foreach($locations as $loc)
                    {
                        $qr["id_location"]=$loc;
                        
                    }
                echo $inserted;
            }else
            {
                $this->output->unset_template();
                $this->error_messages ("You don't have permissions");
            }
        }
        
        
        public function event_detail($id_event=false)
        {
            if($this->input->get("modal"))
                $this->_init("blank",false);
            else
                $this->_init("aero",true);
            if ($this->flexi_auth->is_privileged('Schedule'))
            {
                $this->load->js(site_url("assets/themes/aero/js/loader.js"));
                $this->load->js_inside(site_url("assets/themes/aero/js/plugins/bootstrap-timepicker/bootstrap-timepicker.min.js"));
                $this->load->js_inside(site_url("assets/themes/aero/js/plugins/bootstrap-datepicker/bootstrap-datepicker.js"));
                
                $this->load->js_inside(site_url("assets/themes/aero/js/schedule/event_details.js"));
                
                $this->load->js(site_url("assets/themes/aero/js/plugins/fullcalendar/fullcalendar.min.js"));
                $data["event"]=$this->admin_model->selectEvent($id_event);
                $data["locations"]=$this->admin_model->getLocations();
                $data["categories"]=$this->admin_model->getCategories();
                if($data["event"])
                    $data["eventLocations"]=json_decode($data["event"]["locations"]);
                $this->load->view("admin/schedule/event_details",$data);
            }else
            {
                $this->output->unset_template();
                $this->error_messages ("You don't have permissions");
            }
        }
        
        public function site_config()
        {
            $this->_init();
            //title,description,meta
            $this->output->set_common_meta('Site config', 'general', '');
            $this->output->set_canonical(site_url());
            if ($this->flexi_auth->is_privileged('View locations'))
            {
                $this->load->library('grocery_CRUD');
                $crud = new grocery_CRUD();
                $crud->set_theme('aero');
                $crud->set_table('configs');
                $crud->set_subject("sistem config");
                
                $crud->unset_add();
                if (!$this->flexi_auth->is_privileged('Edit locations'))
                    $crud->unset_edit();
                $crud->unset_delete();
                
                $output = $crud->render();
                
                $this->load->view('crud',$output); 
            }else
                $this->error_messages ("You don't have permissions");
        }  
        
        
        public function payments_report()
        {
            if ($this->flexi_auth->is_privileged('View payments'))
            {
                $this->_init();
                //title,description,meta
                $this->output->set_common_meta('Payments', 'all payments', '');
                $this->output->set_canonical(site_url());
                $this->load->library('grocery_CRUD');
                $crud = new grocery_CRUD();
                $crud->set_theme('aero');
                $crud->set_table('payments');
                $crud->order_by("id_payment","DESC");
                $crud->set_subject("Payment");
                $crud->columns("id_payment","id_voucher","value","method","external_id","External_date","platform","id_client","id_user","date","status");
                $crud->set_relation('id_client', 'user_accounts', 'uacc_username');
                $crud->set_relation('id_user', 'user_accounts', 'uacc_username');
                $crud->set_relation('status', 'payment_status', 'name_status');
                $crud->set_relation('method', 'payment_method', 'name_method');
                $crud->set_relation('id_voucher', 'vouchers', 'ID N°: {id_voucher}, {total_points} Points');
                $crud->display_as("id_voucher","Voucher")->display_as("value","Value (Payed)");
                if (!$this->flexi_auth->is_privileged('Add payment'))
                    $crud->unset_add();
                if (!$this->flexi_auth->is_privileged('Edit payment'))
                    $crud->unset_edit();
                if (!$this->flexi_auth->is_privileged('Delete payment'))
                    $crud->unset_delete();
                
                $output = $crud->render();
                
                $this->load->view('crud',$output); 
            }else
                $this->error_messages ("You don't have permissions");
        }
        
        public function levels()
        {
            
            if ($this->flexi_auth->is_privileged('View payments'))
            {
                $this->_init();
                //title,description,meta
                $this->output->set_common_meta('Levels', 'all levels', '');
                $this->output->set_canonical(site_url());
                $this->load->library('grocery_CRUD');
                $crud = new grocery_CRUD();
                $crud->set_theme('aero');
                $crud->set_table('levels');
                $crud->set_subject("Levels");
                if (!$this->flexi_auth->is_privileged('Add payment'))
                    $crud->unset_add();
                if (!$this->flexi_auth->is_privileged('Edit payment'))
                    $crud->unset_edit();
                if (!$this->flexi_auth->is_privileged('Delete payment'))
                    $crud->unset_delete();
                
                $output = $crud->render();
                
                $this->load->view('crud',$output); 
            }else
                $this->error_messages ("You don't have permissions");
            
        }
        
}
	
/* End of file Admin.php */
/* Location: ./application/controllers/Admin.php */	
