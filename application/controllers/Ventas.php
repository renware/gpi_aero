<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/* CONTROLADOR DEL MODULO DE VENTAS */
/* NO ALTERAR VARIABLES DEL CONSTRUCTOR, SE PUEDEN AGREGAR CONDICIONES, MÁS NO QUITAR LAS QUE YA ESTÁN */

class Ventas extends MY_Controller {

    function __construct() {
        parent::__construct();



        // Carga de variables generales
        $this->load->helper('url');
        $this->load->vars('base_url', site_url());
        $this->load->vars('includes_dir', site_url() . '/includes');
        $this->load->vars('current_url', $this->uri->uri_to_assoc(1));

        // VARIABLE GLOBAL (LOS DATOS SON ENVIADOS INCLUSO A LAS VISTAS FINALES DEL USUARIO) 
        $this->data = null;


        //CARGA DEL TEMA GRAFICO (TODOS LOS SCRIPTS ESTAN ACA)
        $this->load->js(site_url('assets/themes/aero/js/jquery.js'));
        $this->_init();

        //CARGA DE MODELOS RELACIONADOS (para acceso de datos)
        //$this->load->model("product_model");
        //CARGA DE LIBRERÍAS, NO QUITAR LA DE GROCERY CRUD
        $this->load->library('grocery_CRUD');
    }

    /* COMENZAR A DESARROLLAR EL MODULO EN METODOS APARTE DESDE ACA */

    function index() {
        //proceso inicial que se realiza al entrar a sitio/ventas   
    }

    function pasajes() {
        $this->load->view("ventas/solicitud_transporte_cliente");
    }

    function encomiendas() {
        //Carga la vista para la gestion de encomiendas
        $this->load->model('Ventas_model');
        //retorna un array 
        $data['output'] = $this->Ventas_model->listarLocalizaciones();
        $this->load->view("ventas/solicitud_transporte_encomiendas", $data);
    }

    function buscar_vuelos_disponibles() {
        $post = $this->input->post();
        $this->load->model('ventas_model');
        $data = $this->ventas_model->buscar_vuelos_disponibles($post);
        if (!$data) {
            $data['output'] = $this->ventas_model->listarLocalizaciones();
            $data['error'] = "No se han encontrado vuelos disponibles";
            $this->load->view("ventas/solicitud_transporte_encomiendas", $data);
        } else {
            $datos['post'] = $post;
            $datos['output'] = $data;
            $this->load->view("ventas/buscar_vuelos_disponibles", $datos);
        }
    }

    function actualizarVuelo() {
        if ($this->input->is_ajax_request()) {
            $post = $this->input->post();
            $this->load->model('ventas_model');
            $data = $this->ventas_model->actualizar_vuelos($post);
            if ($data) {
                $notification = "true";
            } else {
                $notification = "false";
            }

            echo json_encode(array('notify' => $notification, 'post' => $post));
            exit;
        } else {
            show_404();
        }
    }

    function reservarEncomienda($vuelo='') {
            if (!empty($vuelo)) {
            $get['output'] = $vuelo;
            $this->load->view("ventas/reservar_encomienda", $get);
            }else{
                show_404();
            }
    }

    //Por defecto se le asigna el valor 0 por lo cual mostrara error 404 seguridad
    function boleta($numeroBoleta = '') {
        if (!empty($numeroBoleta)) {
            $this->load->model('Ventas_model');
            //retorna un array 
            $boleta = $this->Ventas_model->obtenerBoleta($numeroBoleta);
            $this->load->library('Fpdf');
            $this->pdf = new PDF('P', 'mm', 'A4');
            $this->pdf->Open();
            $this->pdf->AddPage();

            $this->pdf->SetMargins(15, 10, 15);

            $this->pdf->AddFont('consola', '', 'consola.php');
            $this->pdf->Image(base_url() . 'assets/themes/aero/img/Logo.png', 0, 8, 35, 40);
            //Texto Sucursal 
            $this->pdf->setXY(30, 0);
            $this->pdf->SetFont('consola', '', 20);
            $this->pdf->Cell(0, 23, "EMPRESA D&E AIRLINESS S.A", 0, 1);

            $this->pdf->setXY(30, 6);
            $this->pdf->SetFont('consola', '', 12);
            $this->pdf->Cell(0, 23, "SERVICIOS DE AEROLINEAS PARA,", 0, 1);

            $this->pdf->setXY(30, 10);
            $this->pdf->Cell(0, 23, "EL TRANSPORTE DE PERSONAS Y ENCOMIENDAS", 0, 1);
            $this->pdf->setXY(30, 14);
            $this->pdf->Cell(0, 23, "OF. PRINCIPAL: EL BOSQUE #500 ", 0, 1);
            $this->pdf->setXY(30, 18);
            $this->pdf->Cell(0, 23, "COMUNA LAS CONDES,SANTIAGO DE CHILE", 0, 1);
            $this->pdf->setXY(30, 22);
            $this->pdf->Cell(0, 23, "ventas@dye.com - www.dye.com", 0, 1);
            // FIN del texto sucursal
            $this->pdf->SetTextColor(266, 10, 10);
            $this->pdf->SetFont('consola', '', 14);
            $this->pdf->setXY(142, 12);
            $this->pdf->Cell(0, 6, '' . "R.U.T.: " . '78833320-K', 0, 1);
            $this->pdf->SetFont('consola', '', 17);
            $this->pdf->setXY(137, 21);
            $this->pdf->Cell(0, 6, '' . "BOLETA ELECTRONICA", 0, 1);
            $this->pdf->SetFont('consola', '', 14);
            $this->pdf->setXY(145, 30);
            $this->pdf->Cell(0, 6, "Numero: " . $boleta['idBoleta'], 0, 1);


            $this->pdf->setXY(140, 35);
            $this->pdf->Cell(0, 23, "S.I.I.SANTIAGO CHILE  ", 0, 1);

            //DIBUJAR RECTANGULO
            $this->pdf->SetDrawColor(255, 10, 10); //color del borde en este caso rojo rgb
            $this->pdf->Rect(130, 9, 72, 31, 'D');
            //Detalle
            //Que comienze depues de X pixeles abajo
            $this->pdf->Ln(4);

            //SECTOR CLIENTE
            $this->pdf->SetTextColor(0, 0, 0);
            $this->pdf->SetDrawColor(0, 0, 0); //color del borde en este caso rojo rgb
            $this->pdf->Rect(14, 54, 180, 23, 'D');
            $this->pdf->SetFont('consola', '', 10);
            $this->pdf->setXY(15, 55);
            $this->pdf->Cell(0, 6, '' . "Estimado:", 0, 1);
            $this->pdf->setXY(35, 55);
            $this->pdf->Cell(0, 6, '' . ucwords($boleta['nombre'] . " " . $boleta['apellido']), 0, 1);
            $this->pdf->setXY(140, 55);
            $this->pdf->Cell(0, 6, '' . "RUT.:", 0, 1);
            $this->pdf->setXY(155, 55);
            $this->pdf->Cell(0, 6, '' . substr_replace($boleta['idCliente'], "-", strlen($boleta['idCliente']) - 1, 0), 0, 1);
            //Segunda linea
            $this->pdf->setXY(15, 60);
            $this->pdf->Cell(0, 6, '' . "Direccion:", 0, 1);
            $this->pdf->setXY(35, 60);
            $this->pdf->Cell(0, 6, '' . "Sin informacion", 0, 1);
            $this->pdf->setXY(140, 60);
            $this->pdf->Cell(0, 6, '' . "Fecha.:", 0, 1);
            $this->pdf->setXY(155, 60);
            $this->pdf->Cell(0, 6, '' . $boleta['fecha'], 0, 1);
            //Tercera linea
            $this->pdf->setXY(15, 65);
            $this->pdf->Cell(0, 6, '' . "Comuna:", 0, 1);
            $this->pdf->setXY(35, 65);
            $this->pdf->Cell(0, 6, '' . "Sin informacion", 0, 1);
            $this->pdf->setXY(80, 65);
            $this->pdf->Cell(0, 6, '' . "Ciudad:", 0, 1);
            $this->pdf->setXY(95, 65);
            $this->pdf->Cell(0, 6, '' . "Sin informacion", 0, 1);
            $this->pdf->setXY(140, 65);
            $this->pdf->Cell(0, 6, '' . "Telefono.:", 0, 1);
            $this->pdf->setXY(160, 65);
            $this->pdf->Cell(0, 6, '' . $boleta['telefono'], 0, 1);
            //Cuarta linea
            $this->pdf->setXY(15, 70);
            $this->pdf->Cell(0, 6, '' . "Giro:", 0, 1);
            $this->pdf->setXY(35, 70);
            $this->pdf->Cell(0, 6, '' . "Sin informacion", 0, 1);
            $this->pdf->setXY(140, 70);
            $this->pdf->Cell(0, 6, '' . "Forma de Pago.:", 0, 1);
            $this->pdf->setXY(170, 70);
            $this->pdf->Cell(0, 6, '' . ucwords($boleta['tipo_pago']), 0, 1);
            //Fin del sector cliente
            //SECTOR DETALLE
            $this->pdf->SetWidths(array(20, 27, 80, 30, 24));
            $this->pdf->SetFont('consola', '', 11);
            $this->pdf->SetFillColor(255, 255, 255);
            $this->pdf->SetTextColor(0);
            $this->pdf->setXY(14, 90);
            $this->pdf->Row(array(" CODIGO", "   CANT", "               Detalle", " P.Unitario", "  Total"));

            $unitario = number_format('12312', 0, ',', '.');
            $detalle = number_format('12312', 0, ',', '.');
            $this->pdf->SetFillColor(255, 255, 255);

            $this->pdf->SetTextColor(1);
            $this->pdf->setX(14);
            if ($boleta['tipo_boleta'] == 'E') {
                $tipoVenta = "Envio de Encomienda";
            } else {
                $tipoVenta = "Venta de Pasajes";
            }
            $y = $this->pdf->Row(array('-------', $boleta['cantidad'], $tipoVenta . " desde " . ucwords($boleta['origen']) . " hacia " . ucwords($boleta['destino']), "$" . number_format($boleta['neto'] / $boleta['cantidad'], 0, ",", "."), "$" . number_format($boleta['neto'], 0, ",", ".")));

            //Footer
            //Firma electronica
            $this->pdf->Line(14, 186, 195, 186);
            $this->pdf->Image(base_url() . 'assets/themes/aero/img/codigo_barra.png', 25, 190, 78, 40);
            $this->pdf->setXY(50, 224);
            $this->pdf->Cell(0, 23, "RES. 64 de 2015", 0, 1);
            $this->pdf->setXY(30, 228);
            $this->pdf->Cell(0, 23, "VERIFIQUE DOCUMENTO: www.sii.cl", 0, 1);
            //Termino de firma electronica
            //
        //Totales
            $this->pdf->SetFont('consola', '', 14);
            $this->pdf->setXY(130, 186);
            $this->pdf->Cell(0, 23, "Neto", 0, 1);
            $this->pdf->setXY(130, 192);
            $this->pdf->Cell(0, 23, "IVA", 0, 1);
            $this->pdf->setXY(130, 198);
            $this->pdf->Cell(0, 23, "Total", 0, 1);
            //Valores de los totales
            $this->pdf->setXY(160, 186);
            $this->pdf->Cell(0, 23, "$" . number_format($boleta['neto'], 0, ",", "."), 0, 1);
            $this->pdf->setXY(160, 192);
            $this->pdf->Cell(0, 23, "$" . number_format($boleta['iva'], 0, ",", "."), 0, 1);
            $this->pdf->setXY(160, 198);
            $this->pdf->Cell(0, 23, "$" . number_format($boleta['total'], 0, ",", "."), 0, 1);
            $this->pdf->Rect(123, 190, 72, 31, 'D');

            //Acuse de recibo
            $this->pdf->Rect(105, 227, 91, 31, 'D');
            $this->pdf->SetFont('consola', '', 10);
            $this->pdf->setXY(105, 220);
            $this->pdf->Cell(0, 23, "Nombre:", 0, 1);
            $this->pdf->setXY(105, 225);
            $this->pdf->Cell(0, 23, "RUT.:", 0, 1);
            $this->pdf->setXY(105, 230);
            $this->pdf->Cell(0, 23, "Recinto:", 0, 1);
            $this->pdf->setXY(155, 225);
            $this->pdf->Cell(0, 23, "Firma:", 0, 1);
            $this->pdf->setXY(155, 230);
            $this->pdf->Cell(0, 23, "Fecha.:", 0, 1);
            $this->pdf->SetFont('consola', '', 6);
            $this->pdf->setXY(105, 235);
            $this->pdf->Cell(0, 23, "El acuse de recibo que se declara en este acto, de acuerdo a lo dispuesto en ", 0, 1);
            $this->pdf->setXY(105, 237);
            $this->pdf->Cell(0, 23, "la: letra b) del Art. 4°, y la letra c) del Art. 5° de la Ley 19.983,  ", 0, 1);
            $this->pdf->setXY(105, 239);
            $this->pdf->Cell(0, 23, "acredita que la entrega de mercaderías o servicio (s) prestado (s) ha (n)  ", 0, 1);
            $this->pdf->setXY(105, 241);
            $this->pdf->Cell(0, 23, "sido recibido (s).", 0, 1);

            //Fin del Footer
            $this->pdf->Output();
        } else {
            show_404();
        }
    }

    function factura($numeroFactura) {
        if (!empty($numeroFactura)) {
            $this->load->model('Ventas_model');
            //retorna un array 
            $dte = $this->Ventas_model->obtenerFactura($numeroFactura);
            $this->load->library('Fpdf');
            $this->pdf = new PDF('P', 'mm', 'A4');
            $this->pdf->Open();
            $this->pdf->AddPage();

            $this->pdf->SetMargins(15, 10, 15);

            $this->pdf->AddFont('consola', '', 'consola.php');
            $this->pdf->Image(base_url() . 'assets/themes/aero/img/Logo.png', 0, 8, 35, 40);
            //Texto Sucursal 
            $this->pdf->setXY(30, 0);
            $this->pdf->SetFont('consola', '', 20);
            $this->pdf->Cell(0, 23, "EMPRESA D&E AIRLINESS S.A", 0, 1);

            $this->pdf->setXY(30, 6);
            $this->pdf->SetFont('consola', '', 12);
            $this->pdf->Cell(0, 23, "SERVICIOS DE AEROLINEAS PARA,", 0, 1);

            $this->pdf->setXY(30, 10);
            $this->pdf->Cell(0, 23, "EL TRANSPORTE DE PERSONAS Y ENCOMIENDAS", 0, 1);
            $this->pdf->setXY(30, 14);
            $this->pdf->Cell(0, 23, "OF. PRINCIPAL: EL BOSQUE #500 ", 0, 1);
            $this->pdf->setXY(30, 18);
            $this->pdf->Cell(0, 23, "COMUNA LAS CONDES,SANTIAGO DE CHILE", 0, 1);
            $this->pdf->setXY(30, 22);
            $this->pdf->Cell(0, 23, "ventas@dye.com - www.dye.com", 0, 1);
            // FIN del texto sucursal
            $this->pdf->SetTextColor(266, 10, 10);
            $this->pdf->SetFont('consola', '', 14);
            $this->pdf->setXY(142, 12);
            $this->pdf->Cell(0, 6, '' . "R.U.T.: " . '78833320-K', 0, 1);
            $this->pdf->SetFont('consola', '', 17);
            $this->pdf->setXY(134, 21);
            $this->pdf->Cell(0, 6, '' . "FACTURA ELECTRONICA", 0, 1);
            $this->pdf->SetFont('consola', '', 14);
            $this->pdf->setXY(145, 30);
            $this->pdf->Cell(0, 6, "Numero: " . $dte['idFactura'], 0, 1);


            $this->pdf->setXY(140, 35);
            $this->pdf->Cell(0, 23, "S.I.I.SANTIAGO CHILE  ", 0, 1);

            //DIBUJAR RECTANGULO
            $this->pdf->SetDrawColor(255, 10, 10); //color del borde en este caso rojo rgb
            $this->pdf->Rect(130, 9, 72, 31, 'D');
            //Detalle
            //Que comienze depues de X pixeles abajo
            $this->pdf->Ln(4);

            //SECTOR CLIENTE
            $this->pdf->SetTextColor(0, 0, 0);
            $this->pdf->SetDrawColor(0, 0, 0); //color del borde en este caso rojo rgb
            $this->pdf->Rect(14, 54, 180, 23, 'D');
            $this->pdf->SetFont('consola', '', 10);
            $this->pdf->setXY(15, 55);
            $this->pdf->Cell(0, 6, '' . "Estimado:", 0, 1);
            $this->pdf->setXY(35, 55);
            $this->pdf->Cell(0, 6, '' . ucwords($dte['nombre'] . " " . $dte['apellido']), 0, 1);
            $this->pdf->setXY(140, 55);
            $this->pdf->Cell(0, 6, '' . "RUT.:", 0, 1);
            $this->pdf->setXY(155, 55);
            $this->pdf->Cell(0, 6, '' . substr_replace($dte['idCliente'], "-", strlen($dte['idCliente']) - 1, 0), 0, 1);
            //Segunda linea
            $this->pdf->setXY(15, 60);
            $this->pdf->Cell(0, 6, '' . "Direccion:", 0, 1);
            $this->pdf->setXY(35, 60);
            $this->pdf->Cell(0, 6, '' . "Sin informacion", 0, 1);
            $this->pdf->setXY(140, 60);
            $this->pdf->Cell(0, 6, '' . "Fecha.:", 0, 1);
            $this->pdf->setXY(155, 60);
            $this->pdf->Cell(0, 6, '' . $dte['fecha'], 0, 1);
            //Tercera linea
            $this->pdf->setXY(15, 65);
            $this->pdf->Cell(0, 6, '' . "Comuna:", 0, 1);
            $this->pdf->setXY(35, 65);
            $this->pdf->Cell(0, 6, '' . "Sin informacion", 0, 1);
            $this->pdf->setXY(80, 65);
            $this->pdf->Cell(0, 6, '' . "Ciudad:", 0, 1);
            $this->pdf->setXY(95, 65);
            $this->pdf->Cell(0, 6, '' . "Sin informacion", 0, 1);
            $this->pdf->setXY(140, 65);
            $this->pdf->Cell(0, 6, '' . "Telefono.:", 0, 1);
            $this->pdf->setXY(160, 65);
            $this->pdf->Cell(0, 6, '' . $dte['telefono'], 0, 1);
            //Cuarta linea
            $this->pdf->setXY(15, 70);
            $this->pdf->Cell(0, 6, '' . "Giro:", 0, 1);
            $this->pdf->setXY(35, 70);
            $this->pdf->Cell(0, 6, '' . "Sin informacion", 0, 1);
            $this->pdf->setXY(140, 70);
            $this->pdf->Cell(0, 6, '' . "Forma de Pago.:", 0, 1);
            $this->pdf->setXY(170, 70);
            $this->pdf->Cell(0, 6, '' . ucwords($dte['tipo_pago']), 0, 1);
            //Fin del sector cliente
            //SECTOR DETALLE
            $this->pdf->SetWidths(array(20, 27, 80, 30, 24));
            $this->pdf->SetFont('consola', '', 11);
            $this->pdf->SetFillColor(255, 255, 255);
            $this->pdf->SetTextColor(0);
            $this->pdf->setXY(14, 90);
            $this->pdf->Row(array(" CODIGO", "   CANT", "               Detalle", " P.Unitario", "  Total"));

            $unitario = number_format('12312', 0, ',', '.');
            $detalle = number_format('12312', 0, ',', '.');
            $this->pdf->SetFillColor(255, 255, 255);

            $this->pdf->SetTextColor(1);
            $this->pdf->setX(14);
            if ($dte['tipo_factura'] == 'E') {
                $tipoVenta = "Envio de Encomienda";
            } else {
                $tipoVenta = "Venta de Pasajes";
            }
            $y = $this->pdf->Row(array('-------', $dte['cantidad'], $tipoVenta . " desde " . ucwords($dte['origen']) . " hacia " . ucwords($dte['destino']), "$" . number_format($dte['neto'] / $dte['cantidad'], 0, ",", "."), "$" . number_format($dte['neto'], 0, ",", ".")));

            //Footer
            //Firma electronica
            $this->pdf->Line(14, 186, 195, 186);
            $this->pdf->Image(base_url() . 'assets/themes/aero/img/codigo_barra.png', 25, 190, 78, 40);
            $this->pdf->setXY(50, 224);
            $this->pdf->Cell(0, 23, "RES. 64 de 2015", 0, 1);
            $this->pdf->setXY(30, 228);
            $this->pdf->Cell(0, 23, "VERIFIQUE DOCUMENTO: www.sii.cl", 0, 1);
            //Termino de firma electronica
            //
        //Totales
            $this->pdf->SetFont('consola', '', 14);
            $this->pdf->setXY(130, 186);
            $this->pdf->Cell(0, 23, "Neto", 0, 1);
            $this->pdf->setXY(130, 192);
            $this->pdf->Cell(0, 23, "IVA", 0, 1);
            $this->pdf->setXY(130, 198);
            $this->pdf->Cell(0, 23, "Total", 0, 1);
            //Valores de los totales
            $this->pdf->setXY(160, 186);
            $this->pdf->Cell(0, 23, "$" . number_format($dte['neto'], 0, ",", "."), 0, 1);
            $this->pdf->setXY(160, 192);
            $this->pdf->Cell(0, 23, "$" . number_format($dte['iva'], 0, ",", "."), 0, 1);
            $this->pdf->setXY(160, 198);
            $this->pdf->Cell(0, 23, "$" . number_format($dte['total'], 0, ",", "."), 0, 1);
            $this->pdf->Rect(123, 190, 72, 31, 'D');

            //Acuse de recibo
            $this->pdf->Rect(105, 227, 91, 31, 'D');
            $this->pdf->SetFont('consola', '', 10);
            $this->pdf->setXY(105, 220);
            $this->pdf->Cell(0, 23, "Nombre:", 0, 1);
            $this->pdf->setXY(105, 225);
            $this->pdf->Cell(0, 23, "RUT.:", 0, 1);
            $this->pdf->setXY(105, 230);
            $this->pdf->Cell(0, 23, "Recinto:", 0, 1);
            $this->pdf->setXY(155, 225);
            $this->pdf->Cell(0, 23, "Firma:", 0, 1);
            $this->pdf->setXY(155, 230);
            $this->pdf->Cell(0, 23, "Fecha.:", 0, 1);
            $this->pdf->SetFont('consola', '', 6);
            $this->pdf->setXY(105, 235);
            $this->pdf->Cell(0, 23, "El acuse de recibo que se declara en este acto, de acuerdo a lo dispuesto en ", 0, 1);
            $this->pdf->setXY(105, 237);
            $this->pdf->Cell(0, 23, "la: letra b) del Art. 4°, y la letra c) del Art. 5° de la Ley 19.983,  ", 0, 1);
            $this->pdf->setXY(105, 239);
            $this->pdf->Cell(0, 23, "acredita que la entrega de mercaderías o servicio (s) prestado (s) ha (n)  ", 0, 1);
            $this->pdf->setXY(105, 241);
            $this->pdf->Cell(0, 23, "sido recibido (s).", 0, 1);

            //Fin del Footer
            $this->pdf->Output();
        } else {
            show_404();
        }
    }

    function ticket($ticket = '') {
        if (!empty($ticket)) {
            $this->load->model('Ventas_model');
            $this->load->library('fpdf2');

            $ticket = $this->Ventas_model->obtenerTicketEncomienda($ticket);
            $this->pdf = new Fpdf2();
            $this->pdf->Open();
            $this->pdf->AddPage();

            $this->pdf->SetMargins(10, 10, 10);

            $this->pdf->AddFont('consola', '', 'consola.php');
            //Ticket 
            //rectangulo principal
            $this->pdf->SetDrawColor(148, 148, 148); //color del borde en este caso rojo rgb
            $this->pdf->RoundedRect(5, 5, 200, 120, 8, '1234', 'D');

            //Escribir el vuelo
            $this->pdf->setXY(34, 10);
            $this->pdf->SetFont('consola', '', 17);
            $this->pdf->Cell(0, 23, "Vuelo", 0, 1);

            //Fin del vuelo
            //Hora de Presentacion Puerta de Embarque
            $this->pdf->Line(70, 15, 70, 44);
            //Salida
            $this->pdf->setXY(90, 10);
            $this->pdf->SetFont('consola', '', 17);
            $this->pdf->Cell(0, 23, "Volumen", 0, 1);
            $this->pdf->Line(134, 15, 134, 44);
            //Puerta
            //Fila Asiento
            $this->pdf->setXY(155, 10);
            $this->pdf->SetFont('consola', '', 17);
            $this->pdf->Cell(0, 23, "Hora", 0, 1);
            $this->pdf->setXY(150, 15);
            $this->pdf->Cell(0, 23, "Reserva", 0, 1);


            //Variables dinamicas
            //Vuelo
            $this->pdf->SetFont('consola', '', 18);
            $this->pdf->setXY(30, 25);
            $this->pdf->Cell(0, 23, substr($ticket['desde'], 0, 1) . substr($ticket['hacia'], 0, 1) . " " . $ticket['numero'] . "*", 0, 1);
            //Volumen
            $this->pdf->setXY(95, 25);
            $this->pdf->Cell(0, 23, $ticket['volumen'] . "m2", 0, 1);
            //Hora Reserva
            $this->pdf->setXY(148, 25);
            $this->pdf->Cell(0, 23, $ticket['hora_salida'], 0, 1);

            $this->pdf->SetDrawColor(174, 174, 174); //color del borde en este caso gris mas claro rgb
            $this->pdf->SetAlpha(0.5);
            $this->pdf->SetFillColor(203, 203, 203); //color del borde en este caso azul rgb
            $this->pdf->RoundedRect(15, 10, 180, 40, 8, '1234', 'DF');
            $this->pdf->SetAlpha(1);
            $this->pdf->SetFillColor(0, 8, 121); //color del borde en este caso azul rgb
            $this->pdf->RoundedRect(120, 55, 75, 30, 8, '1234', 'F');

            $this->pdf->Image(base_url() . 'assets/themes/aero/img/codigo_barra.png', 15, 55, 50, 18);

            //Numero ticket de encomienda
            $this->pdf->SetFont('consola', '', 15);
            $this->pdf->SetTextColor(255, 255, 255);
            $this->pdf->setXY(125, 55);
            $this->pdf->Cell(0, 23, "TICKET DE ENCOMIENDAS", 0, 1);
            $this->pdf->setXY(125, 60);
            $this->pdf->Cell(0, 23, "Numero #" . $ticket['numero'], 0, 1);
            //===============
            //Informacion ticket
            $this->pdf->SetFont('consola', '', 8);
            $this->pdf->SetTextColor(101, 101, 101);
            $this->pdf->setXY(66, 48);
            $this->pdf->Cell(0, 23, "VUELO OPERADOR POR", 0, 1);
            $this->pdf->setXY(66, 52);
            $this->pdf->Cell(0, 23, "PASAJERO FRECUENTE", 0, 1);
            $this->pdf->setXY(66, 56);
            $this->pdf->Cell(0, 23, "N# TICKET", 0, 1);
            //Datos
            $this->pdf->SetTextColor(0, 0, 0);
            $this->pdf->setXY(97, 48);
            $this->pdf->Cell(0, 23, "Empresa D&E", 0, 1);
            $this->pdf->setXY(97, 52);
            $this->pdf->Cell(0, 23, "LA", 0, 1);
            $this->pdf->setXY(97, 56);
            $this->pdf->Cell(0, 23, $ticket['numero'], 0, 1);
            //Fin de la informacion ticket
            //Detalle ticket
            $this->pdf->SetFont('consola', '', 12);
            $this->pdf->SetTextColor(101, 101, 101);
            $this->pdf->setXY(18, 70);
            $this->pdf->Cell(0, 23, "NOMBRE EMISOR", 0, 1);

            $this->pdf->setXY(75, 70);
            $this->pdf->Cell(0, 23, "NOMBRE RECEPTOR", 0, 1);

            $this->pdf->setXY(18, 87);
            $this->pdf->Cell(0, 23, "DESDE", 0, 1);

            $this->pdf->setXY(105, 87);
            $this->pdf->Cell(0, 23, "HACIA", 0, 1);

            //Datos Encabezado
            $this->pdf->SetFont('consola', '', 14);
            $this->pdf->SetTextColor(0, 0, 0);
            $this->pdf->setXY(18, 78);
            $this->pdf->Cell(0, 23, $ticket['nombre_emisor'], 0, 1);
            $this->pdf->setXY(75, 78);
            $this->pdf->Cell(0, 23, $ticket['nombre_destino'], 0, 1);

            $this->pdf->setXY(18, 95);
            $this->pdf->Cell(0, 23, $ticket['desde'], 0, 1);

            $this->pdf->setXY(105, 95);
            $this->pdf->Cell(0, 23, $ticket['hacia'], 0, 1);

            //Fin de datos Encabezado
            //DESDE
            $this->pdf->SetFont('consola', '', 9);
            $this->pdf->SetTextColor(101, 101, 101);
            $this->pdf->setXY(18, 102);
            $this->pdf->Cell(0, 23, "AEROPUERTO", 0, 1);
            $this->pdf->SetTextColor(0, 0, 0);
            $this->pdf->setXY(40, 102);
            $this->pdf->Cell(0, 23, $ticket['aeropuerto_origen'], 0, 1);
            $this->pdf->SetTextColor(101, 101, 101);
            $this->pdf->setXY(65, 102);
            $this->pdf->Cell(0, 23, "TERMINAL", 0, 1);
            $this->pdf->SetTextColor(0, 0, 0);
            $this->pdf->setXY(85, 102);
            $this->pdf->Cell(0, 23, "UNICO", 0, 1);
            //HACIA
            $this->pdf->SetTextColor(101, 101, 101);
            $this->pdf->setXY(105, 102);
            $this->pdf->Cell(0, 23, "AEROPUERTO", 0, 1);
            $this->pdf->SetTextColor(0, 0, 0);
            $this->pdf->setXY(130, 102);
            $this->pdf->Cell(0, 23, $ticket['aeropuerto_destino'], 0, 1);
            $this->pdf->SetTextColor(101, 101, 101);
            $this->pdf->setXY(160, 102);
            $this->pdf->Cell(0, 23, "TERMINAL", 0, 1);
            $this->pdf->SetTextColor(0, 0, 0);
            $this->pdf->setXY(180, 102);
            $this->pdf->Cell(0, 23, "UNICO", 0, 1);

            //Datos Ticket detalle
            //Fin de los datos ticket detalle
            //Fin del detalle del ticket
            //Indicaciones
            $this->pdf->SetFont('consola', '', 12);
            $this->pdf->setXY(30, 117);
            $this->pdf->Cell(0, 23, "LA HORA DE LLEGADA VARIA ENTRE 24 HASTA 72 HORAS DESPUES DE LA", 0, 1);
            $this->pdf->setXY(30, 121);
            $this->pdf->Cell(0, 23, "HORA DE RESERVA.EN CASO DE CUALQUIER INCONVENIENTE ACERQUESE AL", 0, 1);
            $this->pdf->setXY(75, 125);
            $this->pdf->Cell(0, 23, "MOSTRADOR DE ENCOMIENDAS.", 0, 1);

            //Fin del ticket



            $this->pdf->Output();
        } else {
            show_404();
        }
    }

    function ticketPasajero() {
        //$this->load->model('Ventas_model');
        $this->load->library('fpdf2');

        //$boleta = $this->facturas_model->obtenerBoleta('1');
        $this->pdf = new FPDF2('P', 'mm', 'A4');
        $this->pdf->Open();
        $this->pdf->AddPage();

        $this->pdf->SetMargins(10, 10, 10);

        $this->pdf->AddFont('consola', '', 'consola.php');
        //Ticket 
        //rectangulo principal
        $this->pdf->SetDrawColor(148, 148, 148); //color del borde en este caso rojo rgb
        $this->pdf->RoundedRect(5, 5, 200, 120, 8, '1234', 'D');

        //Escribir el vuelo
        $this->pdf->Line(44, 15, 44, 44);
        $this->pdf->setXY(25, 10);
        $this->pdf->SetFont('consola', '', 12);
        $this->pdf->Cell(0, 23, "Vuelo", 0, 1);
        //Fin del vuelo
        //Hora  Presentacion Aeropouerto
        $this->pdf->Line(75, 15, 75, 44);
        $this->pdf->setXY(45, 10);
        $this->pdf->SetFont('consola', '', 8);
        $this->pdf->Cell(0, 23, "Hora Presentacion", 0, 1);
        $this->pdf->setXY(50, 13);
        $this->pdf->Cell(0, 23, "Aeropuerto", 0, 1);
        //Hora de Presentacion Puerta de Embarque
        $this->pdf->Line(105, 15, 105, 44);
        $this->pdf->setXY(76, 10);
        $this->pdf->Cell(0, 23, "Hora Presentacion", 0, 1);
        $this->pdf->setXY(75, 13);
        $this->pdf->Cell(0, 23, "Puerta de Embarque", 0, 1);
        //Salida
        $this->pdf->Line(135, 15, 135, 44);
        $this->pdf->setXY(112, 10);
        $this->pdf->SetFont('consola', '', 12);
        $this->pdf->Cell(0, 23, "Salida", 0, 1);
        //Puerta
        $this->pdf->Line(165, 15, 165, 44);
        $this->pdf->setXY(142, 10);
        $this->pdf->Cell(0, 23, "Puerta", 0, 1);
        //Fila Asiento
        $this->pdf->setXY(167, 10);
        $this->pdf->SetFont('consola', '', 10);
        $this->pdf->Cell(0, 23, "Fila/Asiento", 0, 1);
        $this->pdf->SetDrawColor(174, 174, 174); //color del borde en este caso gris mas claro rgb
        $this->pdf->SetAlpha(0.5);
        $this->pdf->SetFillColor(203, 203, 203); //color del borde en este caso azul rgb
        $this->pdf->RoundedRect(15, 10, 180, 40, 8, '1234', 'DF');
        $this->pdf->SetAlpha(1);
        $this->pdf->SetFillColor(0, 8, 121); //color del borde en este caso azul rgb
        $this->pdf->RoundedRect(120, 55, 75, 30, 8, '1234', 'F');

        $this->pdf->Image(base_url() . 'assets/themes/aero/img/codigo_barra.png', 15, 55, 50, 18);
        //Fin del ticket
        //Recomendaciones

        $this->pdf->SetDrawColor(174, 174, 174); //color del borde en este caso gris mas claro rgb
        $this->pdf->RoundedRect(20, 130, 170, 130, 8, '1234', 'D');

        //Rectangulo interior Informacion importante
        $this->pdf->RoundedRect(25, 150, 160, 25, 7, '1234', 'D');
        //Fin del rectangulo interior de informacion importante
        //
        //Rectangulo interior Servicios Especiales
        $this->pdf->RoundedRect(25, 180, 55, 35, 7, '1234', 'D');
        //Fin del rectangulo interior Servicios Especiales
        //
         //Rectangulo interior Llaves Maletas
        $this->pdf->RoundedRect(25, 220, 55, 35, 7, '1234', 'D');
        //Fin del rectangulo interior Llaves Maletas
        //Rectangulo interior Solo Equipaje de Mano
        $this->pdf->RoundedRect(85, 180, 45, 75, 7, '1234', 'D');
        //Fin del rectangulo interior Solo Equipaje de Mano
        //Rectangulo interior Solo Equipaje de Mano PESO 
        $this->pdf->RoundedRect(135, 180, 50, 75, 7, '1234', 'D');
        //Fin del rectangulo interior Solo Equipaje de Mano PESO
        //Fin de las recomendaciones
        //Fin del Footer

        $this->pdf->Output();
    }

}
