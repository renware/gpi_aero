<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Messages extends MY_Controller {
 
    function __construct() 
    {
        parent::__construct();

			

		// Check user is logged in via either password or 'Remember me'.
		// Note: Allow access to logged out users that are attempting to validate a change of their email address via the 'update_email' page/method.
		if (! $this->flexi_auth->is_logged_in() && $this->uri->segment(2) != 'update_email')
		{
			// Set a custom error message.
			$this->flexi_auth->set_error_message('You must login to acces this area.', TRUE);
			$this->session->set_flashdata('message', $this->flexi_auth->get_messages());
			redirect('auth');
		}
                
                
	}

	public function index($option=false)
        {
            $this->_init();//se cargar el tema para todos los views de este controller
            //title,description,meta
            $this->output->set_common_meta('Messages', 'List', '');
            $this->output->set_canonical(site_url());
            if ($this->flexi_auth->is_privileged('View messages'))
            {
                $this->load->library('grocery_CRUD');
                $crud = new grocery_CRUD();
                $crud->set_theme('aero');
                $crud->set_table('messages');
                $crud->set_relation("id_client","user_profiles","{upro_first_name} {upro_last_name}");
                $crud->set_relation('status', 'status_messages', 'name_status');
                if($option=="add")
                    $crud->field_type("id_user","hidden",$this->flexi_auth->get_user_id());
                else
                    $crud->set_relation("id_user","user_accounts","uacc_username");
                
                if (!$this->flexi_auth->is_privileged('Add messages'))
                    $crud->unset_add();
                if (!$this->flexi_auth->is_privileged('Edit messages'))
                    $crud->unset_edit();
                if (!$this->flexi_auth->is_privileged('Delete mesaages'))
                    $crud->unset_delete();
                
                $output = $crud->render();
                
                $this->load->view('crud',$output); 
            }else
                $this->error_messages ("You don't have permissions");
	    
        }
        
        public function qr($option=false)
        {
            $this->_init();//se cargar el tema para todos los views de este controller
            //title,description,meta
            $this->output->set_common_meta('Messages', 'For qr-codes status', '');
            $this->output->set_canonical(site_url());
            if ($this->flexi_auth->is_privileged('View messages'))
            {
                $this->load->library('grocery_CRUD');
                $crud = new grocery_CRUD();
                $crud->set_theme('aero');
                $crud->set_subject("Message");
                $crud->set_table('qr_codes_messages');
                $crud->set_relation("id_qr_code", "qr_codes", "Text:{text}, Expiration: {expiration}");
                $crud->set_relation("id_event", "events", "name_event");
                $crud->set_relation('status', 'qr_codes_status_messages', 'name');
                $crud->display_as("id_event","Event (optional)")->display_as("id_qr_code","Qr code");
                
                if (!$this->flexi_auth->is_privileged('Add messages'))
                    $crud->unset_add();
                if (!$this->flexi_auth->is_privileged('Edit messages'))
                    $crud->unset_edit();
                if (!$this->flexi_auth->is_privileged('Delete mesaages'))
                    $crud->unset_delete();
                
                
                $output = $crud->render();
                
                $this->load->view('crud',$output); 
            }else
                $this->error_messages ("You don't have permissions");
	    
        }
}
	
/* End of file Messages.php */
/* Location: ./application/controllers/Messages.php */	
