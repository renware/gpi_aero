<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Referer extends MY_Controller {
 
    function __construct() 
    {
        parent::__construct();

			

		// Check user is logged in via either password or 'Remember me'.
		// Note: Allow access to logged out users that are attempting to validate a change of their email address via the 'update_email' page/method.
		if (! $this->flexi_auth->is_logged_in() && $this->uri->segment(2) != 'update_email')
		{
			// Set a custom error message.
			$this->flexi_auth->set_error_message('You must login to acces this area.', TRUE);
			$this->session->set_flashdata('message', $this->flexi_auth->get_messages());
			redirect('auth');
		}
		$this->load->js(site_url('assets/themes/aero/js/jquery.js'));
                $this->_init();//se cargar el tema para todos los views de este controller
                $this->load->model("referer_model");
                
	}

	###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###	
	// flexi auth demo
	###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###	

	/**
	 * Many of the functions within this controller load a custom model called 'auth_model' that has been created for the purposes of this demo.
	 * The 'auth_model' file is not part of the flexi auth library, it is included to demonstrate how some of the functions of flexi auth can be used.
	 *
	 * These demos show working examples of how to implement some (most) of the functions available from the flexi auth library.
	 * This particular controller 'start', is used by users who have logged in and now wish to manage their account settings
	 * 
	 * All demos are to be used as exactly that, a demonstation of what the library can do.
	 * In a few cases, some of the examples may not be considered as 'Best practice' at implementing some features in a live environment.
	*/
	
	###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###	
	// Dashboard
	###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###	

        
        
        
        
	/**
	 * index
	 * Forwards to the public dashboard.
	 */ 
	function index()
	{
            $id_referer = $this->input->get("ref");
            //este método obtendrá el código de referido y hará el referido
            if($id_referer && is_numeric($id_referer))
            {
                
                $this->set_temporaly_guest_relation($id_referer);
		redirect('auth/register_account');
            }else
                $this->relations();
            
	}
        
        private function set_temporaly_guest_relation($id_referer)
        {
            //validate user is allowed
            // set cookie
            
            return true;
        }
        
        public function relations(){
            
            //title,description,meta
            $this->output->set_common_meta('Referer', 'relations', '');
            $this->output->set_canonical(site_url());
            if ($this->flexi_auth->is_privileged('View referers'))
            {
                $this->load->library('grocery_CRUD');
                $crud = new grocery_CRUD();
                $crud->set_theme('aero');
                $crud->set_table('user_referer');
                $crud->set_relation('id_user_child', 'user_accounts', 'uacc_username');
                $crud->set_relation('id_user_father', 'user_accounts', 'uacc_username');
                
                $crud->callback_field("text", array($this,"qr_generator"));
                if (!$this->flexi_auth->is_privileged('Add referers'))
                    $crud->unset_add();
                if (!$this->flexi_auth->is_privileged('Edit referers'))
                    $crud->unset_edit();
                if (!$this->flexi_auth->is_privileged('Delete referers'))
                    $crud->unset_delete();
                
                $output = $crud->render();
                
                $this->load->view('crud',$output); 
            }else
                redirect (site_url());
        }
        
        
 
}
	
/* End of file start.php */
/* Location: ./application/controllers/start.php */	
