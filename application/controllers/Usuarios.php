<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends MY_Controller {
 
    function __construct() 
    {
        parent::__construct();

        // Check user is logged in via either password or 'Remember me'.
        // Note: Allow access to logged out users that are attempting to validate a change of their email address via the 'update_email' page/method.
        if (! $this->flexi_auth->is_logged_in() && $this->uri->segment(2) != 'update_email')
        {
                // Set a custom error message.
                $this->flexi_auth->set_error_message('You must login to enter this area.', TRUE);
                $this->session->set_flashdata('message', $this->flexi_auth->get_messages());
                redirect('auth');
        }

        $this->load->model("admin_model");
    }
    
    public function index($option=false)
    {
        $this->_init();
        //title,description,meta
        $this->output->set_common_meta('Usuarios', 'List', '');
        $this->output->set_canonical(site_url());
        if ($this->flexi_auth->is_privileged('View locations'))
        {
            $this->load->library('grocery_CRUD');
            $crud = new grocery_CRUD();
            $crud->set_theme('aero');
            $crud->set_table('usuarios');
            $crud->set_relation('status_location', 'status', 'status_name');
            
            if (!$this->flexi_auth->is_privileged('Add locations'))
                $crud->unset_add();
            if (!$this->flexi_auth->is_privileged('Edit locations'))
                $crud->unset_edit();
            if (!$this->flexi_auth->is_privileged('Delete locations'))
                $crud->unset_delete();

            $output = $crud->render();

            $this->load->view('crud',$output); 
        }else
            redirect (site_url());

    }
    
    public function accesos($option=false)
    {
        $this->_init();
        //title,description,meta
        $this->output->set_common_meta('Accesos', 'List', '');
        $this->output->set_canonical(site_url());
        if ($this->flexi_auth->is_privileged('View locations'))
        {
            $this->load->library('grocery_CRUD');
            $crud = new grocery_CRUD();
            $crud->set_theme('aero');
            $crud->set_table('accesos');
            
            if (!$this->flexi_auth->is_privileged('Add locations'))
                $crud->unset_add();
            if (!$this->flexi_auth->is_privileged('Edit locations'))
                $crud->unset_edit();
            if (!$this->flexi_auth->is_privileged('Delete locations'))
                $crud->unset_delete();

            $output = $crud->render();

            $this->load->view('crud',$output); 
        }else
            redirect (site_url());

    }
}