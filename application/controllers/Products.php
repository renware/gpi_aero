<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Products extends MY_Controller  {
 
    function __construct() 
    {
        parent::__construct();

      
        
        // Note: This is only included to create base urls for purposes of this demo only and are not necessarily considered as 'Best practice'.
        $this->load->vars('base_url', site_url());
        $this->load->vars('includes_dir', site_url().'/includes');
        $this->load->vars('current_url', $this->uri->uri_to_assoc(1));
            
        // Define a global variable to store data that is then used by the end view page.
        $this->data = null;
                
        //$this->load->js(site_url('assets/themes/aero/js/jquery.js'));        
        $this->_init();//se cargar el tema para todos los views de este controller

        $this->load->model("product_model");
        $this->load->library('grocery_CRUD');
    }



        function manager()
        {
            $this->output->set_common_meta('Dashboard', 'Administrar Productos', '');
            $this->output->set_canonical(site_url("dashboard"));
            // If 'Update Account' form has been submitted, update the user account details.
            if ($this->input->post('operation')) 
            {
                $this->load->model('product_model');
                echo 'Stand by';
            }
            
            // Get users current data.
            // This example does so via 'get_user_by_identity()', however, 'get_users()' using any other unqiue identifying column and value could also be used.
             $this->data['message'] = (! isset($this->data['message'])) ? $this->session->flashdata('message') : $this->data['message'];     

            
            $this->grocery_crud->set_table('productos');
            $data['output'] = $this->grocery_crud->render();

            $this->load->view('productos/ver_productos',$data['output']);

           
            
            
         }
         
         
         function manager2()
        {
            $this->output->set_common_meta('Dashboard', 'Administrar Productos', '');
            $this->output->set_canonical(site_url("dashboard"));
            // If 'Update Account' form has been submitted, update the user account details.
            if ($this->input->post('operation')) 
            {
                $this->load->model('product_model');
                echo 'Stand by';
            }
            
            // Get users current data.
            // This example does so via 'get_user_by_identity()', however, 'get_users()' using any other unqiue identifying column and value could also be used.
             $this->data['message'] = (! isset($this->data['message'])) ? $this->session->flashdata('message') : $this->data['message'];     

            
            $this->grocery_crud->set_table('times_format');
            $this->grocery_crud->unset_add();
            $data['output'] = $this->grocery_crud->render();

            $this->load->view('productos/ver_productos',$data['output']);

           
            
            
         }
         
         
        /* 
         * To change this license header, choose License Headers in Project Properties.
         * To change this template file, choose Tools | Templates
         * and open the template in the editor.
        */
    }
?>