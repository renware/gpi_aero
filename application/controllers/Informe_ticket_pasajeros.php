<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Informe_Ticket_Pasajeros extends CI_Controller {

    public function index() {
        $this->load->model('facturas_model');
        $this->load->library('fpdf');

        $boleta = $this->facturas_model->obtenerDetalleBoleta('1');
        $this->pdf = new PDF('P', 'mm', 'A4');
        $this->pdf->Open();
        $this->pdf->AddPage();

        $this->pdf->SetMargins(10, 10, 10);

        $this->pdf->AddFont('consola', '', 'consola.php');
        //Ticket 
        //rectangulo principal
        $this->pdf->SetDrawColor(148, 148, 148); //color del borde en este caso rojo rgb
        $this->pdf->RoundedRect(5,5,200,120, 8,'1234', 'D');
        
        //Escribir el vuelo
        $this->pdf->Line(44,15, 44, 44);
        $this->pdf->setXY(25,10);
        $this->pdf->SetFont('consola', '',12);
        $this->pdf->Cell(0, 23, "Vuelo", 0, 1);
        //Fin del vuelo
        
        //Hora  Presentacion Aeropouerto
        $this->pdf->Line(75,15, 75, 44);
        $this->pdf->setXY(45,10);
        $this->pdf->SetFont('consola', '', 8);
        $this->pdf->Cell(0, 23, "Hora Presentacion", 0, 1);
        $this->pdf->setXY(50,13);
        $this->pdf->Cell(0, 23, "Aeropuerto", 0, 1);
        //Hora de Presentacion Puerta de Embarque
        $this->pdf->Line(105,15, 105, 44);
        $this->pdf->setXY(76,10);
        $this->pdf->Cell(0, 23, "Hora Presentacion", 0, 1);
        $this->pdf->setXY(75,13);
        $this->pdf->Cell(0, 23, "Puerta de Embarque", 0, 1);
        //Salida
        $this->pdf->Line(135,15, 135, 44);
        $this->pdf->setXY(112,10);
        $this->pdf->SetFont('consola', '',12);
        $this->pdf->Cell(0, 23, "Salida", 0, 1);
        //Puerta
        $this->pdf->Line(165,15, 165, 44);
        $this->pdf->setXY(142,10);
        $this->pdf->Cell(0, 23, "Puerta", 0, 1);
        //Fila Asiento
        $this->pdf->setXY(167,10);
        $this->pdf->SetFont('consola', '',10);
        $this->pdf->Cell(0, 23, "Fila/Asiento", 0, 1);
        $this->pdf->SetDrawColor(174, 174, 174); //color del borde en este caso gris mas claro rgb
        $this->pdf->SetAlpha(0.5);
        $this->pdf->SetFillColor(203, 203, 203); //color del borde en este caso azul rgb
        $this->pdf->RoundedRect(15,10,180,40, 8,'1234', 'DF');
        $this->pdf->SetAlpha(1);
        $this->pdf->SetFillColor(0, 8, 121); //color del borde en este caso azul rgb
        $this->pdf->RoundedRect(120,55,75,30, 8,'1234', 'F');
        
        $this->pdf->Image(base_url() . 'assets/themes/aero/img/codigo_barra.png',15,55,50,18);
        //Fin del ticket
        
        //Recomendaciones
        
        $this->pdf->SetDrawColor(174, 174, 174); //color del borde en este caso gris mas claro rgb
        $this->pdf->RoundedRect(20,130,170,130, 8,'1234', 'D');
        
        //Rectangulo interior Informacion importante
        $this->pdf->RoundedRect(25,150,160,25, 7,'1234', 'D');
        //Fin del rectangulo interior de informacion importante
        //
        //Rectangulo interior Servicios Especiales
        $this->pdf->RoundedRect(25,180,55,35, 7,'1234', 'D');
        //Fin del rectangulo interior Servicios Especiales
        //
         //Rectangulo interior Llaves Maletas
        $this->pdf->RoundedRect(25,220,55,35, 7,'1234', 'D');
        //Fin del rectangulo interior Llaves Maletas
        
         //Rectangulo interior Solo Equipaje de Mano
        $this->pdf->RoundedRect(85,180,45,75, 7,'1234', 'D');
        //Fin del rectangulo interior Solo Equipaje de Mano
        
        //Rectangulo interior Solo Equipaje de Mano PESO 
        $this->pdf->RoundedRect(135,180,50,75, 7,'1234', 'D');
        //Fin del rectangulo interior Solo Equipaje de Mano PESO
        
        
        
        //Fin de las recomendaciones
        
        //Fin del Footer
       
        $this->pdf->Output();
    }


}
