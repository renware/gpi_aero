<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class Messages_model extends CI_Model {
    
    public function checkUserMessage($id_client)
    {
        $sql="SELECT m.*,u.uacc_username as user_name,c.uacc_username as client_name from messages as m "
                ." left join user_accounts as u on u.uacc_id = m.id_user"
                ." left join user_accounts as c on c.uacc_id = m.id_client"
                . " where m.id_client = ".$id_client." and status = 1 order by m.date desc limit 1";
        return $this->db->query($sql)->row_array();
    }
    
    public function markAsReadUserMessage($id_client){
        $message=$this->checkUserMessage($id_client);
        $data["status"]=2;
        $this->db->where("id_message",$message["id_message"])->update("messages",$data);
        
        return true;
    }
    
}
