<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class Admin_model extends CI_Model {
    
    public function getEvents()
    {
        $events= $this->db->get("events")->result_array();
        $av_ev=false;
        foreach ($events as $ev)
        {
            $ev["locations"]=$this->geteventLocations($ev["id_event"]);
            $av_ev[]=$ev;
        }
        return $av_ev;
    }
    
    public function getLocations()
    {
        return $this->db->where("status_location",1)->get("locations")->result_array();
    }
    
    public function getCategories()
    {
        return $this->db->where("status",1)->get("categories")->result_array();
    }
    
    public function saveEvent($data)
    {
        $this->db->insert("events",$data);
        return $this->db->insert_id();
    }
    
    public function saveEventLocations($data)
    {
        $this->db->insert("event_locations",$data);
        return true;
    }
    
    public function geteventLocations($id_event)
    {
        return $this->db->where("id_event",$id_event)->get("event_locations")->result_array();
    }
    
    public function selectEvent($id_event)
    {
        $event= $this->db->where("id_event",$id_event)->get("events")->row_array();
        
        $event["locations"]=$this->getEventLocations($ev["id_event"]);
        return $event;
    }
    
    public function recordSale($client_id,$id_voucher,$value,$method,$status,$external_id="None",$external_date="0000-00-00 00:00:00",$plataform="Manual",$id_user=false)
    {    
        $payment["id_voucher"]=$id_voucher;
        $payment["value"]=$value;
        $payment["method"]=$method;
        $payment["id_client"]=$client_id;
        $payment["external_id"]=$external_id;
        $payment["external_date"]=$external_date;
        $payment["platform"]=$plataform;
        if($id_user)
            $payment["id_user"]=$id_user;
        $payment["status"]=$status;
        $this->db->insert("payments",$payment);
        return $this->db->insert_id();
    }
    
}
