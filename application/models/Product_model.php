<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class Product_model extends CI_Model {
    
   
    public function obtenerListaProductos()
    {
        
        return $this->db->get("productos")->result_array();
        
    }
    
    public function obtenerListaStock()
    {
        $sql = "SELECT id_stock, p.nombre, cantidad, uacc_username AS 'encargado', fecha
                FROM stock s
                JOIN user_accounts u ON s.encargado = u.uacc_id
                JOIN productos p ON s.id_producto = p.id_producto";
        
        $data = $this->db->query($sql)->result_array();
        return $data;
        
    }

    
    public function agregarProducto($arr)
    {
        $sql = "INSERT INTO productos(nombre,tipo,detalle,disponible) VALUES ('".$arr['name']."','".$arr['type']."','".$arr['detail']."',".$arr['available'].")";
        
        $this->db->query($sql);
        
        $sql = "SELECT * FROM productos WHERE nombre = '".$arr['name']."' and detalle = '".$arr['detail']."'";
        $data = $this->db->query($sql)->row();
        
        $this->agregarStock($data->id_producto,$arr['available'],$arr['user']);
        
        return 1;
    }
    
    public function agregarStock($id,$av,$user)
    {
        if((int)$av > 0)
        {
            $sql = "INSERT INTO stock(id_producto,cantidad,encargado) VALUES ('".$id."',".$av.",".$user.")";
            $this->db->query($sql);
            
            return 1;
        }
    }
    
    
}

?>