<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require('./application/config/database.php');//se requiere el archivo database.php

class MOS_function_model extends CI_Model{
    //Constructor
   public function __construct() {
        parent::__construct();
   }
    //metodo guardar datos de la tabla mos_historico
   public function guardar($idHistorico, $idPasajero, $idVuelo, $idEncomienda, $idAvion){
    $data= array(
        'idHistorico' => $idHistorico,
        'idPasajero' => $idPasajero,
        'idVuelo' => $idVuelo,
        'idEncomienda' => $idEncomienda,
        'idAvion' => $idAvion
    );
    if($idVuelo){
    $this->db->where('idVuelo',$idVuelo);
    $this->db->update('mos_historicos',$data);
    }else{
        $this->db->insert('mos_historicos',$data);
    } 
   }
   //metodo para buscar idVuelo de los datos historicos
   public function buscarDatos($idvuelo){
   $this->db->select('idHistorico, idPasajero, idVuelo, idEncomienda, idAvion');
   $this->db->from('mos_historicos');
   $this->db->where('idvuelo', $idvuelo);
   $consulta = $this->db->get();
   $resultado = $consulta->row();
   return $resultado;
   }
    //metodo para listar todos lo datos de historico
    public function listarDatos(){
    $this->db->select('idHistorico, idPasajero, idVuelo, idEncomienda, idAvion');
    $this->db->from('mos_historicos');
    $consulta = $this->db->get();
    $resultado = $consulta->result();
    return $resultado;
    }

}

?>