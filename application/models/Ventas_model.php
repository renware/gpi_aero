<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Ventas_model extends CI_Model {

    public function buscar_vuelos_disponibles($data) {
        if(!empty($data)){
            $this->load->database();

        $origen = $data['origen'];
        $destino = $data['destino'];
        $date = str_replace('/', '-', $data['fecha']);
        $fecha = date('Y-m-d', strtotime($date));
        $volumen = $data['volumen'];

        $sql = "SELECT * FROM PLV_VUELOS V  "
                . "                  JOIN REP_LOCALIZACIONES L ON(V.IDORIGEN=L.IDLOCALIZACION)"
                . "                  WHERE V.IDORIGEN='$origen' "
                . "                  AND V.IDDESTINO='$destino'"
                . "                  AND V.FECHA>='$fecha'"
                . "                  AND V.VOLUMEN_BODEGA>='$volumen'";
        
        $vuelos = $this->db->query($sql);
        if ($vuelos->num_rows() > 0) {
            return $vuelos->result_array();
        } else {
            return false;
        }
        }else{
            show_404();
        }
        
    }
    
    public function actualizar_vuelos($post){
        if(!empty($post)){
            $this->load->database();

        $volumen = $post['volumen'];
        $idVuelo = $post['vuelo'];

        $sql = "update plv_vuelos set volumen_bodega=volumen_bodega-'$volumen' where idVuelo='$idVuelo'";
        
        $vuelos = $this->db->query($sql);
        if ($vuelos) {
            return true;
        } else {
            return false;
        }
        }else{
            show_404();
        }
    }

    public function obtenerBoleta($numeroBoleta) {
        $this->load->database();
        $boletas = $this->db->query("SELECT v.idBoleta,v.idCliente,v.total,v.neto,v.iva,v.cantidad,cl.nombre,cl.telefono,v.tipo_boleta,pv.precio,"
                . "                         l.ciudad as origen,q.ciudad as destino,cl.apellido,DATE_FORMAT(v.fecha,'%d-%m-%Y') as fecha,"
                . "                         t.nombre as tipo_pago"
                . "                  FROM VEN_BOLETAS V  "
                . "                  JOIN CLI_CLIENTES CL ON(V.IDCLIENTE=CL.IDCLIENTE)"
                . "                  JOIN VEN_TIPO_PAGO T ON(V.TIPO_PAGO=T.ID_TIPO_PAGO)"
                . "                  JOIN PLV_VUELOS PV ON(V.IDVUELO=PV.IDVUELO)"
                . "                  JOIN REP_LOCALIZACIONES L ON(PV.IDORIGEN=L.IDLOCALIZACION)"
                . "                  JOIN REP_LOCALIZACIONES Q ON(PV.IDDESTINO=L.IDLOCALIZACION)"
                . "                  WHERE V.IDBOLETA='$numeroBoleta' limit 1");
        if ($boletas->num_rows() > 0) {
            return $boletas->row_array();
        } else {
            show_404();
        }
    }

    public function obtenerFactura($numeroFactura) {
        $dte = $this->db->query("SELECT v.idFactura,v.idCliente,v.total,v.neto,v.iva,v.cantidad,cl.nombre,cl.telefono,v.tipo_factura,pv.precio,"
                . "                         l.ciudad as origen,q.ciudad as destino,cl.apellido,DATE_FORMAT(v.fecha,'%d-%m-%Y') as fecha,"
                . "                         t.nombre as tipo_pago"
                . "                  FROM VEN_FACTURAS V  "
                . "                  JOIN CLI_CLIENTES CL ON(V.IDCLIENTE=CL.IDCLIENTE)"
                . "                  JOIN VEN_TIPO_PAGO T ON(V.TIPO_PAGO=T.ID_TIPO_PAGO)"
                . "                  JOIN PLV_VUELOS PV ON(V.IDVUELO=PV.IDVUELO)"
                . "                  JOIN REP_LOCALIZACIONES L ON(PV.IDORIGEN=L.IDLOCALIZACION)"
                . "                  JOIN REP_LOCALIZACIONES Q ON(PV.IDDESTINO=L.IDLOCALIZACION)"
                . "                  WHERE V.IDFACTURA='$numeroFactura' limit 1");
        if ($dte->num_rows() > 0) {
            return $dte->row_array();
        } else {
            show_404();
        }
    }

    function obtenerTicketEncomienda($ticket) {
        $ticket = $this->db->query("SELECT concat(c.nombre,' ',c.apellido) as nombre_emisor,
                                concat(vd.nombre,' ',vd.apellido) as nombre_destino,
                                rl.ciudad as desde,rq.ciudad as hacia,
                                vt.idTicket_de_encomienda as numero,TIME_FORMAT(pv.hora_salida, '%H:%i %p') as hora_salida,e.volumen,
                                pa.nombre as aeropuerto_origen,pa2.nombre as aeropuerto_destino
                                from ven_ticket_encomiendas vt 
                                JOIN plv_vuelos pv on(vt.idVuelo=pv.idVuelo)
                                JOIN rep_localizaciones rl on(pv.idOrigen=rl.idLocalizacion)
                                JOIN rep_localizaciones rq on(pv.idDestino=rq.idLocalizacion)
                                JOIN plv_aeropuertos pa on(rl.idLocalizacion=pa.idLocalización)
                                JOIN plv_aeropuertos pa2 on(rq.idLocalizacion=pa2.idLocalización)
                                JOIN emb_encomiendas e on(vt.idEncomienda=e.idEncomienda)
                                JOIN ven_destinatarios vd on(vt.idDestinatario=vd.idDestinatario)
                                JOIN cli_clientes c on(vt.idCliente=c.idCliente)
                                WHERE vt.idTicket_de_encomienda='$ticket'
                                LIMIT 1");
        if ($ticket->num_rows() > 0) {
            return $ticket->row_array();
        } else {
            show_404();
        }
    }

    function listarLocalizaciones() {
        $this->load->database();
        $localizacion = $this->db->query("SELECT rl.ciudad,rl.idLocalizacion,rr.nombre "
                . "                 from rep_localizaciones rl "
                . "                 join rep_regiones rr on(rl.idRegion=rr.idRegion);");
        if ($localizacion->num_rows() > 0) {
            return $localizacion->result_array();
        }
    }

}

?>