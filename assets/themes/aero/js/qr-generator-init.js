var keylist="abcdefghijklmnopqrstuvwxyzñABCDEFGHIJKLMNOPQRSTUVWXYZÑ123456789"
var temp=""

function generateCode(plength){
    temp=""
    for (i=0;i<plength;i++)
    temp+=keylist.charAt(Math.floor(Math.random()*keylist.length))
    return temp
 }
function codeCreator(){
    var pass=generateCode(6);
    $.getJSON(base_url+"qr/validate_code?code="+pass,function(result){
        if(result.validation===0)
            $("#field-text").val(pass);
        else if ($("#field-text").val() ==="example")
            codeCreator();
    });
}

$("#generate_code").on("click",function(){
        codeCreator();
});

var qrcode = new QRCode("qrcode");

function makeCode () {		
	var elText = document.getElementById("field-text");
	
	if (!elText.value) {
		alert("Input a text");
		elText.focus();
		return;
	}
	
	qrcode.makeCode(elText.value);
}

function exportImage(){
    var form =document.createElement("form");
    form.action=base_url+"qr/export_image";
    form.method="POST";
    form.style.display="none";
    var input=document.createElement("input");
    input.name="imgBase64";
    input.value=$("#qrcode img").attr("src");
    form.appendChild(input);
    document.body.appendChild(form);
    form.submit();
    document.body.removeChild(form)
    
}

makeCode();

$("#field-text").
	on("input", function () {
		makeCode();
	}).
	on("keydown", function (e) {
		if (e.keyCode == 13) {
			makeCode();
		}
	});

$(document).ready(function(){
    if ($("#field-text").val() ==="example")
        codeCreator();
    
})