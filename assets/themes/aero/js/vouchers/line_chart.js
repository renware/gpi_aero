
Morris.Line({
// ID of the element in which to draw the chart.
element: 'morris-chart-line',
// Chart data records -- each entry in this array corresponds to a point on
// the chart.
data: data,
// The name of the data record attribute that contains x-visitss.
xkey: 'd',
// A list of names of data record attributes that contain y-visitss.
ykeys: id_locations,
// Labels for the ykeys -- will be displayed when you hover over the
// chart.
lineColors: colors,
labels: locations,
// Disables line smoothing
smooth: false,
resize: true
});