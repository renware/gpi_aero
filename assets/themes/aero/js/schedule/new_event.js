$('#sandbox-container input').datepicker({
    autoclose: true,
    todayHighlight: true,
    format: 'yyyy-mm-dd'
});

$(document).ready(function() {
    $('#hour_begin').timepicker({showSeconds: true,showMeridian: false});
    $('#hour_end').timepicker({showSeconds: true,showMeridian: false});

    setTimeout(function() {
        $('#timeDisplay').text($('#hour_begin').val());
    }, 100);

    setTimeout(function() {
        $('#timeDisplay').text($('#hour_end').val());
    }, 100);

    $('#hour_begin').on('changeTime.timepicker', function(e) {
        $('#timeDisplay').text(e.time.value);
    });

    $('#hour_end').on('changeTime.timepicker', function(e) {
        $('#timeDisplay').text(e.time.value);
    });
    
    $("#save_data").unbind("click").bind("click",function(){
       post_data($("#add_new_event").attr("action"),$("#add_new_event").serialize());
        location.reload();
    })
});