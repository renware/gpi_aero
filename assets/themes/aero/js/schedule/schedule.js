//Calendar Demo
function launchCalendar() {


    ;

    /* initialize the calendar
        -----------------------------------------------------------------*/

    $('#calendar').fullCalendar({
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,agendaWeek,agendaDay'
        },
        editable: true,
        droppable: true, // this allows things to be dropped onto the calendar !!!
        drop: function(date, allDay) { // this function is called when something is dropped

            // retrieve the dropped element's stored Event Object
            var originalEventObject = $(this).data('eventObject');

            // we need to copy it, so that multiple events don't have a reference to the same object
            var copiedEventObject = $.extend({}, originalEventObject);

            // assign it the date that was reported
            copiedEventObject.start = date;
            copiedEventObject.allDay = allDay;

            // render the event on the calendar
            // the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
            $('#calendar').fullCalendar('renderEvent', copiedEventObject, true);

            // is the "remove after drop" checkbox checked?
            if ($('#drop-remove').is(':checked')) {
                // if so, remove the element from the "Draggable Events" list
                $(this).remove();
            }
        },

        events: events,
    eventClick: function(calEvent, jsEvent, view) {
        $("#status").attr("style","display:block");
        $("#flexModalLabel").html("Event Details");
        $("#insert").html();
        cargar("insert", base_url+"admin/event_detail/"+calEvent.id+"?modal=true");
       $('#flexModal').modal('show'); 
        

    }
    });

    /* initialize the external events
        -----------------------------------------------------------------*/

    $('#external-events div.external-event').each(function() {

        // create an Event Object (http://arshaw.com/fullcalendar/docs/event_data/Event_Object/)
        // it doesn't need to have a start or end
        var eventObject = {
            title: $.trim($(this).text()) // use the element's text as the event title
        };

        // store the Event Object in the DOM element so we can get to it later
        $(this).data('eventObject', eventObject);

        // make the event draggable using jQuery UI
        $(this).draggable({
            zIndex: 999,
            revert: true, // will cause the event to go back to its
            revertDuration: 0 //  original position after the drag
        });

    });
    
    $("#external-events div.external-event").on("click",function(){
        $("#status").attr("style","display:block");
        $("#flexModalLabel").html("Event Details");
        $("#insert").html();
        cargar("insert", base_url+"admin/event_detail/"+this.id+"?modal=true");
       $('#flexModal').modal('show'); 
   });


}
launchCalendar();
///button new event

$("#new_event").on("click",function(){
    $("#status").attr("style","display:block");
    $("#flexModalLabel").html("New event");
    $("#insert").html();
    cargar("insert", base_url+"admin/new_event?modal=true");
   $('#flexModal').modal('show'); 
})
